with Ekko_Lexical_Analyzer;     use Ekko_Lexical_Analyzer;
with Ekko_Lexical_Analyzer_dfa; use Ekko_Lexical_Analyzer_dfa;
with Ekko_Lexical_Analyzer_io;  use Ekko_Lexical_Analyzer_io;

with Ekko_Syntax_Analyzer_tokens; use Ekko_Syntax_Analyzer_tokens;
with Ekko_Syntax_Analyzer_goto;   use Ekko_Syntax_Analyzer_goto;
with Ekko_Syntax_Analyzer_shift_reduce; use Ekko_Syntax_Analyzer_shift_reduce;
with Ekko_Syntax_Analyzer_error_report; use Ekko_Syntax_Analyzer_error_report;

with semantics.Syntax_Routines; use semantics.Syntax_Routines;
with semantics.Code_Generation_Routines; use semantics.Code_Generation_Routines;

with Text_IO;

package Ekko_Syntax_Analyzer is
    procedure YYParse;
end Ekko_Syntax_Analyzer;
