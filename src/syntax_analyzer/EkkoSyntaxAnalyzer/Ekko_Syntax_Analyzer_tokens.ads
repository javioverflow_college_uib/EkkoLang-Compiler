with  Declarations.d_Attribute;
package Ekko_Syntax_Analyzer_Tokens is

 subtype YYSType is declarations.d_Attribute.Attribute; 
    YYLVal, YYVal : YYSType; 
    type Token is
        (End_Of_Input, Error, Action_Kw, Is_Kw,
         Begin_Kw, End_Kw, Opening_Brackets,
         In_Kw, Out_Kw, Semicolon,
         Closing_Brackets, Type_Kw, New_Kw,
         Range_Kw, Point, Record_Kw,
         Array_Kw, Of_Kw, Constant_Kw,
         Colon, If_Kw, Then_Kw,
         Else_Kw, While_Kw, Loop_Kw,
         Colonequal, Null_Kw, Not_Op,
         And_Op, Or_Op, Addition_Op,
         Substraction_Op, Product_Op, Division_Op,
         Modulus_Op, Comma, Id,
         Literal, Rel_Op, Un_Subs_Op );

    Syntax_Error : exception;

end Ekko_Syntax_Analyzer_Tokens;
