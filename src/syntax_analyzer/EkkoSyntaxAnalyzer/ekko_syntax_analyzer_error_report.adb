

with Text_IO;

package body Ekko_Syntax_Analyzer_Error_Report is

    The_File : Text_io.File_Type;

procedure Report_Continuable_Error 
    (Line_Number : in Natural;
    Offset      : in Natural;
    Finish      : in Natural;
    Message     : in String;
    Error       : in Boolean)  is
begin
    Text_IO.Put_Line("Syntax error at line" & Natural'Image(Line_Number)
        & " between columns" & Natural'Image(Offset) & " and" & Natural'Image(Finish) 
        & ". Hint: " & Message);
end;

procedure Initialize_User_Error_Report is
begin
  null;
end;

procedure Terminate_User_Error_Report is
begin
  null;
end;


    procedure Initialize_Output is
      begin
        Text_io.Create(The_File, Text_io.Out_File, "Ekko_Syntax_Analyzer.lis");
        initialize_user_error_report;
      end Initialize_Output;

    procedure Finish_Output is
      begin
        Text_io.Close(The_File);
        terminate_user_error_report;
      end Finish_Output;

    procedure Put(S: in String) is
    begin
      Text_io.put(The_File, S);
    end Put;

    procedure Put(C: in Character) is
    begin
      Text_io.put(The_File, C);
    end Put;

    procedure Put_Line(S: in String) is
    begin
      Text_io.put_Line(The_File, S);
    end Put_Line;


end Ekko_Syntax_Analyzer_Error_Report;
