package Ekko_Syntax_Analyzer_Goto is

    type Small_Integer is range -32_000 .. 32_000;

    type Goto_Entry is record
        Nonterm  : Small_Integer;
        Newstate : Small_Integer;
    end record;

  --pragma suppress(index_check);

    subtype Row is Integer range -1 .. Integer'Last;

    type Goto_Parse_Table is array (Row range <>) of Goto_Entry;

    Goto_Matrix : constant Goto_Parse_Table :=
       ((-1,-1)  -- Dummy Entry.
-- State  0
,(-3, 1),(-2, 3)
-- State  1

-- State  2
,(-4, 4)
-- State  3

-- State  4
,(-9, 7)
,(-5, 8)
-- State  5

-- State  6

-- State  7

-- State  8

-- State  9
,(-10, 14)
-- State  10

-- State  11
,(-10, 15)
-- State  12
,(-6, 16)

-- State  13

-- State  14

-- State  15

-- State  16
,(-23, 27),(-22, 28),(-21, 25),(-20, 24)
,(-19, 23),(-15, 20),(-14, 19),(-13, 18)
,(-12, 30),(-3, 21)
-- State  17
,(-11, 33)
-- State  18

-- State  19

-- State  20

-- State  21

-- State  22
,(-18, 36)

-- State  23

-- State  24

-- State  25

-- State  26

-- State  27

-- State  28

-- State  29
,(-7, 42)
-- State  30

-- State  31

-- State  32

-- State  33

-- State  34

-- State  35

-- State  36

-- State  37

-- State  38

-- State  39

-- State  40

-- State  41

-- State  42
,(-8, 53)
-- State  43

-- State  44

-- State  45
,(-18, 56)
-- State  46

-- State  47

-- State  48

-- State  49

-- State  50

-- State  51

-- State  52

-- State  53
,(-33, 73)
,(-32, 71),(-28, 68),(-27, 67),(-26, 66)
,(-25, 65),(-24, 75)
-- State  54

-- State  55

-- State  56

-- State  57

-- State  58

-- State  59

-- State  60

-- State  61

-- State  62

-- State  63

-- State  64

-- State  65

-- State  66

-- State  67

-- State  68

-- State  69
,(-33, 73),(-32, 85)
,(-29, 82)
-- State  70
,(-30, 88)
-- State  71

-- State  72

-- State  73

-- State  74

-- State  75

-- State  76

-- State  77
,(-17, 96),(-16, 100)

-- State  78

-- State  79

-- State  80

-- State  81

-- State  82
,(-30, 103)
-- State  83
,(-33, 73),(-32, 85),(-29, 112)

-- State  84
,(-33, 73),(-32, 85),(-29, 113)
-- State  85

-- State  86

-- State  87
,(-33, 73)
,(-32, 85),(-29, 114)
-- State  88
,(-33, 73),(-32, 85)
,(-29, 115)
-- State  89
,(-33, 73),(-32, 85),(-29, 116)

-- State  90

-- State  91

-- State  92
,(-33, 73),(-32, 85),(-29, 118)
-- State  93

-- State  94
,(-33, 73)
,(-32, 85),(-29, 119)
-- State  95

-- State  96

-- State  97
,(-17, 121)
-- State  98

-- State  99

-- State  100

-- State  101
,(-17, 96)
,(-16, 123)
-- State  102

-- State  103

-- State  104
,(-30, 125)
-- State  105
,(-30, 126)
-- State  106
,(-33, 73)
,(-32, 85),(-29, 127)
-- State  107
,(-33, 73),(-32, 85)
,(-29, 128)
-- State  108
,(-33, 73),(-32, 85),(-29, 129)

-- State  109
,(-33, 73),(-32, 85),(-29, 130)
-- State  110
,(-33, 73)
,(-32, 85),(-29, 131)
-- State  111
,(-33, 73),(-32, 85)
,(-29, 132)
-- State  112

-- State  113

-- State  114

-- State  115

-- State  116

-- State  117

-- State  118

-- State  119

-- State  120

-- State  121

-- State  122

-- State  123

-- State  124
,(-8, 137)
-- State  125
,(-33, 73),(-32, 85)
,(-29, 138)
-- State  126
,(-33, 73),(-32, 85),(-29, 139)

-- State  127

-- State  128

-- State  129

-- State  130

-- State  131

-- State  132

-- State  133

-- State  134
,(-30, 140)
-- State  135

-- State  136

-- State  137
,(-33, 73),(-32, 71),(-28, 68)
,(-27, 67),(-26, 66),(-25, 65),(-24, 75)

-- State  138

-- State  139

-- State  140
,(-8, 144)
-- State  141
,(-17, 96),(-16, 145)
-- State  142

-- State  143
,(-31, 147)

-- State  144
,(-33, 73),(-32, 71),(-28, 68),(-27, 67)
,(-26, 66),(-25, 65),(-24, 75)
-- State  145

-- State  146

-- State  147
,(-8, 151)

-- State  148

-- State  149

-- State  150

-- State  151
,(-33, 73),(-32, 71),(-28, 68),(-27, 67)
,(-26, 66),(-25, 65),(-24, 75)
-- State  152

-- State  153

-- State  154

-- State  155

-- State  156

);
--  The offset vector
GOTO_OFFSET : array (0.. 156) of Integer :=
( 0,
 2, 2, 3, 3, 5, 5, 5, 5, 5, 6,
 6, 7, 8, 8, 8, 8, 18, 19, 19, 19,
 19, 19, 20, 20, 20, 20, 20, 20, 20, 21,
 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
 21, 21, 22, 22, 22, 23, 23, 23, 23, 23,
 23, 23, 23, 30, 30, 30, 30, 30, 30, 30,
 30, 30, 30, 30, 30, 30, 30, 30, 30, 33,
 34, 34, 34, 34, 34, 34, 34, 36, 36, 36,
 36, 36, 37, 40, 43, 43, 43, 46, 49, 52,
 52, 52, 55, 55, 58, 58, 58, 59, 59, 59,
 59, 61, 61, 61, 62, 63, 66, 69, 72, 75,
 78, 81, 81, 81, 81, 81, 81, 81, 81, 81,
 81, 81, 81, 81, 82, 85, 88, 88, 88, 88,
 88, 88, 88, 88, 89, 89, 89, 96, 96, 96,
 97, 99, 99, 100, 107, 107, 107, 108, 108, 108,
 108, 115, 115, 115, 115, 115);

subtype Rule        is Natural;
subtype Nonterminal is Integer;

   Rule_Length : array (Rule range  0 ..  68) of Natural := ( 2,
 1, 11, 0, 0, 1, 2, 3, 3,
 4, 1, 1, 2, 2, 0, 1, 1,
 1, 1, 7, 1, 2, 1, 1, 2,
 3, 3, 1, 1, 1, 11, 4, 5,
 4, 5, 3, 6, 0, 2, 2, 1,
 1, 1, 1, 8, 11, 0, 0, 9,
 4, 2, 1, 3, 2, 3, 3, 4,
 4, 2, 3, 3, 3, 3, 3, 3,
 2, 1, 1, 3);
   Get_LHS_Rule: array (Rule range  0 ..  68) of Nonterminal := (-1,
-2,-3,-4,-7,-5,-5,-9,-9,
-10,-11,-11,-11,-6,-6,-12,-12,
-12,-12,-13,-16,-16,-17,-17,-14,
-18,-18,-15,-15,-15,-19,-21,-22,
-22,-20,-23,-23,-8,-8,-8,-24,
-24,-24,-24,-25,-25,-30,-31,-26,
-27,-28,-32,-32,-32,-33,-33,-29,
-29,-29,-29,-29,-29,-29,-29,-29,
-29,-29,-29,-29);
end Ekko_Syntax_Analyzer_Goto;
