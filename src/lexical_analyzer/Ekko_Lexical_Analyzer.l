DIG                             [0-9]
NZD                             [1-9]

NON_UNDERSCORED_INTEGER         ({NZD}{DIG}*|0)

UNDERSCORED_INTEGER_HEAD        {NZD}|{NZD}{DIG}|{NZD}{DIG}{DIG}
UNDERSCORED_INTEGER_APPEND      (_{DIG}{DIG}{DIG})*
UNDERSCORED_INTEGER             {UNDERSCORED_INTEGER_HEAD}{UNDERSCORED_INTEGER_APPEND}

INT_LIT                         {NON_UNDERSCORED_INTEGER}|{UNDERSCORED_INTEGER}

ALPHA_CHAR                      [a-zA-Z]

IDENTIFIER                      ({ALPHA_CHAR}(_?|({ALPHA_CHAR}|{DIG}))*)

PRINTABLE_CHAR                  [\040-\176]
PRINTABLE_CHAR_WITHOUT_QUOTES   [\040-\041\043-\176]

CHAR_LIT                        \'{PRINTABLE_CHAR}\'

TWO_DOUBLE_QUOTES               \"\"
STRING_BODY                     ({TWO_DOUBLE_QUOTES}|{PRINTABLE_CHAR_WITHOUT_QUOTES})*
STR_LIT                         \"{STRING_BODY}\"

COMMENT                         "--"{PRINTABLE_CHAR}*\n

SEPARATOR                       [ \t\r\n]

%%
-- Procedures
"action"            { return action_kw; }
"is"                { return is_kw; }
"begin"             { return begin_kw; }
"end"               { return end_kw; }
"("                 { return opening_brackets; }
"in"                { return in_kw; }
"out"               { return out_kw; }
";"                 { return semicolon; }
")"                 { return closing_brackets; }

-- Declarations
"type"              { return type_kw; }
"new"               { return new_kw; }
"range"             { return range_kw; }
"."                 { return point; }
"record"            { return record_kw; }
"array"             { return array_kw; }
"of"                { return of_kw; }
"constant"          { return constant_kw; }
":"                 { return colon; }


-- Statements
"if"                { return if_kw; }
"then"              { return then_kw; }
"else"              { return else_kw; }

"while"             { return while_kw; }
"loop"              { return loop_kw; }

":="                { return colonequal; }
"null"              { return null_kw; }


-- Relational operators
"<"                 { LR_Relational_Less_Op         (YYLVal, YYPos); return rel_op; }
">"                 { LR_Relational_Greater_Op      (YYLVal, YYPos); return rel_op; }
"<="                { LR_Relational_Less_Equal_Op   (YYLVal, YYPos); return rel_op; }
">="                { LR_Relational_Greater_Equal_Op(YYLVal, YYPos); return rel_op; }
"="                 { LR_Relational_Equal_Op        (YYLVal, YYPos); return rel_op; }
"/="                { LR_Relational_Unequal_Op      (YYLVal, YYPos); return rel_op; }

-- Logical operators
"and"               { LR_Logical_And_Op  (YYLVal, YYPos); return and_op; }
"or"                { LR_Logical_Or_Op   (YYLVal, YYPos); return or_op; }
"not"               { return not_op; }

-- Arithmetic operators
"+"                 { LR_Arithmetic_Add_Op  (YYLVal, YYPos); return addition_op;    }
"-"                 { LR_Arithmetic_Sub_Op  (YYLVal, YYPos); return substraction_op;}
"*"                 { LR_Arithmetic_Mult_Op (YYLVal, YYPos); return product_op;     }
"/"                 { LR_Arithmetic_Div_Op  (YYLVal, YYPos); return division_op;    }
"mod"               { LR_Arithmetic_Mod_Op  (YYLVal, YYPos); return modulus_op;     }

-- Indexes
","                 { return comma; }

-- User input content
{IDENTIFIER}        { LR_Identifier       (YYLVal, YYPos, YYText); return id; }
{INT_LIT}           { LR_Literal_Integer  (YYLVal, YYPos, YYText); return literal; }
{CHAR_LIT}          { LR_Literal_Char     (YYLVal, YYPos, YYText); return literal; }
{STR_LIT}           { LR_Literal_String   (YYLVal, YYPos, YYText); return literal; }
{COMMENT}           { null; }
{SEPARATOR}         { null; }

-- Other
.                   { return Error; }

%%

with Ekko_Syntax_Analyzer_tokens; use Ekko_Syntax_Analyzer_tokens;
with declarations.d_Attribute;    use declarations.d_Attribute;

package Ekko_Lexical_Analyzer is

    procedure Set_Input_File(file_path: in String);

    function YYLex return Token;
    function YYPos return Position;
    procedure YYError(s: in String);

end Ekko_Lexical_Analyzer;

-------------------------------------------------------------------------------

with Ekko_Lexical_Analyzer_io;  use Ekko_Lexical_Analyzer_io;
with Ekko_Lexical_Analyzer_dfa; use Ekko_Lexical_Analyzer_dfa;

with semantics.Lexic_Routines; use semantics.Lexic_Routines;

package body Ekko_Lexical_Analyzer is

    procedure Set_Input_File(file_path: in String) is
    begin
        Ekko_Lexical_Analyzer_io.Open_Input(file_path);
    end Set_Input_File;

    function YYText return String is
    begin
        return Ekko_Lexical_Analyzer_dfa.YYText;
    end YYText;

    function YYPos return Position is
    begin
        return (Tok_Begin_Line, Tok_Begin_Col);
    end YYPos;

    procedure YYError(s: in String) is
    begin
        put(s);
    end YYError;

##

end Ekko_Lexical_Analyzer;
