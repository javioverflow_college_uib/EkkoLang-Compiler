
-------------------------------------------------------------------------------

with Ekko_Lexical_Analyzer_io;  use Ekko_Lexical_Analyzer_io;
with Ekko_Lexical_Analyzer_dfa; use Ekko_Lexical_Analyzer_dfa;

with semantics.Lexic_Routines; use semantics.Lexic_Routines;

package body Ekko_Lexical_Analyzer is

    procedure Set_Input_File(file_path: in String) is
    begin
        Ekko_Lexical_Analyzer_io.Open_Input(file_path);
    end Set_Input_File;

    function YYText return String is
    begin
        return Ekko_Lexical_Analyzer_dfa.YYText;
    end YYText;

    function YYPos return Position is
    begin
        return (Tok_Begin_Line, Tok_Begin_Col);
    end YYPos;

    procedure YYError(s: in String) is
    begin
        put(s);
    end YYError;

function YYLex return Token is
subtype short is integer range -32768..32767;
    yy_act : integer;
    yy_c : short;

-- returned upon end-of-file
YY_END_TOK : constant integer := 0;
YY_END_OF_BUFFER : constant := 49;
subtype yy_state_type is integer;
yy_current_state : yy_state_type;
INITIAL : constant := 0;
yy_accept : constant array(0..122) of short :=
    (   0,
        0,    0,   49,   47,   46,   46,   47,   47,    5,    9,
       37,   35,   40,   36,   13,   38,   42,   42,   18,    8,
       26,   30,   27,   41,   41,   41,   41,   41,   41,   41,
       41,   41,   41,   41,   41,   41,    0,   44,    0,    0,
       31,   42,    0,   24,   28,   29,   41,   41,   41,   41,
       41,   41,   41,   41,   41,   19,    6,    2,   41,   41,
       41,   41,   41,   16,   33,   41,   41,   41,   41,   41,
       41,   43,   45,    0,   42,    0,   41,   32,   41,   41,
       41,   41,    4,   41,   39,   11,   34,   41,    7,   41,
       41,   41,   41,   41,   42,    0,   41,   41,   41,   41,

       21,   23,   25,   41,   41,   20,   10,   41,   42,   41,
       15,    3,   41,   12,   41,   22,    1,   41,   14,   41,
       17,    0
    ) ;

yy_ec : constant array(ASCII.NUL..ASCII.DEL) of short :=
    (   0,
        1,    1,    1,    1,    1,    1,    1,    1,    2,    3,
        1,    1,    2,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    4,    5,    6,    5,    5,    5,    5,    7,    8,
        9,   10,   11,   12,   13,   14,   15,   16,   17,   17,
       17,   17,   17,   17,   17,   17,   17,   18,   19,   20,
       21,   22,    5,    5,   23,   23,   23,   23,   23,   23,
       23,   23,   23,   23,   23,   23,   23,   23,   23,   23,
       23,   23,   23,   23,   23,   23,   23,   23,   23,   23,
        5,    5,    5,    5,   24,    5,   25,   26,   27,   28,

       29,   30,   31,   32,   33,   23,   23,   34,   35,   36,
       37,   38,   23,   39,   40,   41,   42,   23,   43,   23,
       44,   23,    5,    5,    5,    5,    1
    ) ;

yy_meta : constant array(0..44) of short :=
    (   0,
        1,    1,    2,    3,    3,    3,    3,    3,    3,    3,
        3,    3,    3,    3,    3,    4,    4,    3,    3,    3,
        3,    3,    4,    4,    4,    4,    4,    4,    4,    4,
        4,    4,    4,    4,    4,    4,    4,    4,    4,    4,
        4,    4,    4,    4
    ) ;

yy_base : constant array(0..126) of short :=
    (   0,
        0,    0,  227,  228,  228,  228,  220,    0,  228,  228,
      228,  228,  228,  212,  228,  203,  228,   29,  202,  228,
      201,  228,  200,  196,   23,   25,   24,   31,   28,   32,
       33,   42,   48,   51,   49,   50,  213,  212,  210,  213,
      228,   72,   69,  228,  228,  228,  191,  190,   36,   67,
       59,   68,   27,   70,   73,  189,  188,  187,   76,   78,
       79,   80,   81,  186,  185,   83,   84,   85,   87,   90,
       93,  228,  228,  205,  113,  102,   99,  183,  109,  103,
      101,  111,  182,  107,  181,  180,  179,  114,  178,  115,
      118,  120,  123,  119,  133,  141,  127,  130,  135,  136,

      177,  176,  175,  137,  139,  174,  173,  138,  172,  144,
      171,  170,  145,  169,  148,  160,  159,  149,  157,  141,
      155,  228,  183,  185,  171,  188
    ) ;

yy_def : constant array(0..126) of short :=
    (   0,
      122,    1,  122,  122,  122,  122,  123,  124,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  125,  125,  125,  125,  125,  125,  125,
      125,  125,  125,  125,  125,  125,  123,  122,  122,  126,
      122,  122,  122,  122,  122,  122,  125,  125,  125,  125,
      125,  125,  125,  125,  125,  125,  125,  125,  125,  125,
      125,  125,  125,  125,  125,  125,  125,  125,  125,  125,
      125,  122,  122,  126,  122,  122,  125,  125,  125,  125,
      125,  125,  125,  125,  125,  125,  125,  125,  125,  125,
      125,  125,  125,  125,  122,  122,  125,  125,  125,  125,

      125,  125,  125,  125,  125,  125,  125,  125,  122,  125,
      125,  125,  125,  125,  125,  125,  125,  125,  125,  125,
      125,    0,  122,  122,  122,  122
    ) ;

yy_nxt : constant array(0..272) of short :=
    (   0,
        4,    5,    6,    5,    4,    7,    8,    9,   10,   11,
       12,   13,   14,   15,   16,   17,   18,   19,   20,   21,
       22,   23,   24,    4,   25,   26,   27,   24,   28,   24,
       24,   24,   29,   30,   31,   32,   33,   24,   34,   24,
       35,   24,   36,   24,   42,   42,   48,   48,   48,   49,
       48,   48,   43,   52,   48,   48,   48,   56,   50,   48,
       53,   51,   81,   57,   54,   48,   55,   58,   59,   60,
       61,   48,   48,   48,   48,   67,   77,   64,   62,   68,
       69,   71,   48,   63,   76,   76,   65,   75,   75,   66,
       48,   48,   70,   48,   78,   43,   48,   79,   80,   48,

       83,   48,   48,   48,   48,   85,   48,   48,   48,   82,
       48,   91,   84,   48,   88,   92,   48,   96,   96,   90,
       87,   86,   48,   89,   48,   94,   48,   93,   95,   95,
       48,   97,   48,   98,   48,   99,   43,   48,   48,  101,
      100,   48,   48,   48,  102,  104,   48,  103,   95,   95,
       48,  107,  108,   48,  105,  106,  109,  109,   48,   48,
       48,   48,   48,  110,   48,  114,  116,   48,   48,  118,
      112,   48,   48,  111,   47,  119,  113,  115,   48,  117,
       48,  121,   48,   48,  120,   37,   37,   39,   39,   74,
       74,   74,   48,   48,   48,   43,   48,   48,   48,   48,

       48,   48,   48,   48,   48,   48,   48,   73,   48,   48,
       48,   48,   48,   48,   48,   73,   72,   37,   38,   48,
       46,   45,   44,   41,   40,   38,  122,    3,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122
    ) ;

yy_chk : constant array(0..272) of short :=
    (   0,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
        1,    1,    1,    1,   18,   18,   25,   27,   26,   25,
       53,   29,   18,   26,   28,   30,   31,   29,   25,   49,
       27,   25,   53,   29,   28,   32,   28,   29,   30,   31,
       32,   33,   35,   36,   34,   34,   49,   33,   32,   34,
       35,   36,   51,   32,   43,   43,   33,   42,   42,   33,
       50,   52,   35,   54,   50,   42,   55,   51,   52,   59,

       55,   60,   61,   62,   63,   60,   66,   67,   68,   54,
       69,   68,   59,   70,   63,   69,   71,   76,   76,   67,
       62,   61,   77,   66,   81,   71,   80,   70,   75,   75,
       84,   77,   79,   79,   82,   80,   75,   88,   90,   82,
       81,   91,   94,   92,   84,   90,   93,   88,   95,   95,
       97,   93,   94,   98,   91,   92,   96,   96,   99,  100,
      104,  108,  105,   97,  120,  104,  108,  110,  113,  113,
       99,  115,  118,   98,  125,  115,  100,  105,  121,  110,
      119,  120,  117,  116,  118,  123,  123,  124,  124,  126,
      126,  126,  114,  112,  111,  109,  107,  106,  103,  102,

      101,   89,   87,   86,   85,   83,   78,   74,   65,   64,
       58,   57,   56,   48,   47,   40,   39,   38,   37,   24,
       23,   21,   19,   16,   14,    7,    3,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122,  122,  122,  122,  122,  122,  122,  122,  122,
      122,  122
    ) ;


-- copy whatever the last rule matched to the standard output

procedure ECHO is
begin
   if (text_io.is_open(user_output_file)) then
     text_io.put( user_output_file, yytext );
   else
     text_io.put( yytext );
   end if;
end ECHO;

-- enter a start condition.
-- Using procedure requires a () after the ENTER, but makes everything
-- much neater.

procedure ENTER( state : integer ) is
begin
     yy_start := 1 + 2 * state;
end ENTER;

-- action number for EOF rule of a given start state
function YY_STATE_EOF(state : integer) return integer is
begin
     return YY_END_OF_BUFFER + state + 1;
end YY_STATE_EOF;

-- return all but the first 'n' matched characters back to the input stream
procedure yyless(n : integer) is
begin
        yy_ch_buf(yy_cp) := yy_hold_char; -- undo effects of setting up yytext
        yy_cp := yy_bp + n;
        yy_c_buf_p := yy_cp;
        YY_DO_BEFORE_ACTION; -- set up yytext again
end yyless;

-- redefine this if you have something you want each time.
procedure YY_USER_ACTION is
begin
        null;
end;

-- yy_get_previous_state - get the state just before the EOB char was reached

function yy_get_previous_state return yy_state_type is
    yy_current_state : yy_state_type;
    yy_c : short;
begin
    yy_current_state := yy_start;

    for yy_cp in yytext_ptr..yy_c_buf_p - 1 loop
	yy_c := yy_ec(yy_ch_buf(yy_cp));
	if ( yy_accept(yy_current_state) /= 0 ) then
	    yy_last_accepting_state := yy_current_state;
	    yy_last_accepting_cpos := yy_cp;
	end if;
	while ( yy_chk(yy_base(yy_current_state) + yy_c) /= yy_current_state ) loop
	    yy_current_state := yy_def(yy_current_state);
	    if ( yy_current_state >= 123 ) then
		yy_c := yy_meta(yy_c);
	    end if;
	end loop;
	yy_current_state := yy_nxt(yy_base(yy_current_state) + yy_c);
    end loop;

    return yy_current_state;
end yy_get_previous_state;

procedure yyrestart( input_file : file_type ) is
begin
   open_input(text_io.name(input_file));
end yyrestart;

begin -- of YYLex
<<new_file>>
        -- this is where we enter upon encountering an end-of-file and
        -- yywrap() indicating that we should continue processing

    if ( yy_init ) then
        if ( yy_start = 0 ) then
            yy_start := 1;      -- first start state
        end if;

        -- we put in the '\n' and start reading from [1] so that an
        -- initial match-at-newline will be true.

        yy_ch_buf(0) := ASCII.LF;
        yy_n_chars := 1;

        -- we always need two end-of-buffer characters. The first causes
        -- a transition to the end-of-buffer state. The second causes
        -- a jam in that state.

        yy_ch_buf(yy_n_chars) := YY_END_OF_BUFFER_CHAR;
        yy_ch_buf(yy_n_chars + 1) := YY_END_OF_BUFFER_CHAR;

        yy_eof_has_been_seen := false;

        yytext_ptr := 1;
        yy_c_buf_p := yytext_ptr;
        yy_hold_char := yy_ch_buf(yy_c_buf_p);
        yy_init := false;
-- UMASS CODES :
--   Initialization
        tok_begin_line := 1;
        tok_end_line := 1;
        tok_begin_col := 0;
        tok_end_col := 0;
        token_at_end_of_line := false;
        line_number_of_saved_tok_line1 := 0;
        line_number_of_saved_tok_line2 := 0;
-- END OF UMASS CODES.
    end if; -- yy_init

    loop                -- loops until end-of-file is reached

-- UMASS CODES :
--    if last matched token is end_of_line, we must
--    update the token_end_line and reset tok_end_col.
    if Token_At_End_Of_Line then
      Tok_End_Line := Tok_End_Line + 1;
      Tok_End_Col := 0;
      Token_At_End_Of_Line := False;
    end if;
-- END OF UMASS CODES.

        yy_cp := yy_c_buf_p;

        -- support of yytext
        yy_ch_buf(yy_cp) := yy_hold_char;

        -- yy_bp points to the position in yy_ch_buf of the start of the
        -- current run.
	yy_bp := yy_cp;
	yy_current_state := yy_start;
	loop
		yy_c := yy_ec(yy_ch_buf(yy_cp));
		if ( yy_accept(yy_current_state) /= 0 ) then
		    yy_last_accepting_state := yy_current_state;
		    yy_last_accepting_cpos := yy_cp;
		end if;
		while ( yy_chk(yy_base(yy_current_state) + yy_c) /= yy_current_state ) loop
		    yy_current_state := yy_def(yy_current_state);
		    if ( yy_current_state >= 123 ) then
			yy_c := yy_meta(yy_c);
		    end if;
		end loop;
		yy_current_state := yy_nxt(yy_base(yy_current_state) + yy_c);
	    yy_cp := yy_cp + 1;
if ( yy_current_state = 122 ) then
    exit;
end if;
	end loop;
	yy_cp := yy_last_accepting_cpos;
	yy_current_state := yy_last_accepting_state;

<<next_action>>
	    yy_act := yy_accept(yy_current_state);
            YY_DO_BEFORE_ACTION;
            YY_USER_ACTION;

        if aflex_debug then  -- output acceptance info. for (-d) debug mode
            text_io.put( Standard_Error, "--accepting rule #" );
            text_io.put( Standard_Error, INTEGER'IMAGE(yy_act) );
            text_io.put_line( Standard_Error, "(""" & yytext & """)");
        end if;

-- UMASS CODES :
--   Update tok_begin_line, tok_end_line, tok_begin_col and tok_end_col
--   after matching the token.
        if yy_act /= YY_END_OF_BUFFER and then yy_act /= 0 then
-- Token are matched only when yy_act is not yy_end_of_buffer or 0.
          Tok_Begin_Line := Tok_End_Line;
          Tok_Begin_Col := Tok_End_Col + 1;
          Tok_End_Col := Tok_Begin_Col + yy_cp - yy_bp - 1;
          if yy_ch_buf ( yy_bp ) = ASCII.LF then
            Token_At_End_Of_Line := True;
          end if;
        end if;
-- END OF UMASS CODES.

<<do_action>>   -- this label is used only to access EOF actions
            case yy_act is
		when 0 => -- must backtrack
		-- undo the effects of YY_DO_BEFORE_ACTION
		yy_ch_buf(yy_cp) := yy_hold_char;
		yy_cp := yy_last_accepting_cpos;
		yy_current_state := yy_last_accepting_state;
		goto next_action;


-- Procedures
when 1 => 
--# line 31 "Ekko_Lexical_Analyzer.l"
 return action_kw; 

when 2 => 
--# line 32 "Ekko_Lexical_Analyzer.l"
 return is_kw; 

when 3 => 
--# line 33 "Ekko_Lexical_Analyzer.l"
 return begin_kw; 

when 4 => 
--# line 34 "Ekko_Lexical_Analyzer.l"
 return end_kw; 

when 5 => 
--# line 35 "Ekko_Lexical_Analyzer.l"
 return opening_brackets; 

when 6 => 
--# line 36 "Ekko_Lexical_Analyzer.l"
 return in_kw; 

when 7 => 
--# line 37 "Ekko_Lexical_Analyzer.l"
 return out_kw; 

when 8 => 
--# line 38 "Ekko_Lexical_Analyzer.l"
 return semicolon; 

when 9 => 
--# line 39 "Ekko_Lexical_Analyzer.l"
 return closing_brackets; 

-- Declarations
when 10 => 
--# line 42 "Ekko_Lexical_Analyzer.l"
 return type_kw; 

when 11 => 
--# line 43 "Ekko_Lexical_Analyzer.l"
 return new_kw; 

when 12 => 
--# line 44 "Ekko_Lexical_Analyzer.l"
 return range_kw; 

when 13 => 
--# line 45 "Ekko_Lexical_Analyzer.l"
 return point; 

when 14 => 
--# line 46 "Ekko_Lexical_Analyzer.l"
 return record_kw; 

when 15 => 
--# line 47 "Ekko_Lexical_Analyzer.l"
 return array_kw; 

when 16 => 
--# line 48 "Ekko_Lexical_Analyzer.l"
 return of_kw; 

when 17 => 
--# line 49 "Ekko_Lexical_Analyzer.l"
 return constant_kw; 

when 18 => 
--# line 50 "Ekko_Lexical_Analyzer.l"
 return colon; 

-- Statements
when 19 => 
--# line 54 "Ekko_Lexical_Analyzer.l"
 return if_kw; 

when 20 => 
--# line 55 "Ekko_Lexical_Analyzer.l"
 return then_kw; 

when 21 => 
--# line 56 "Ekko_Lexical_Analyzer.l"
 return else_kw; 

when 22 => 
--# line 58 "Ekko_Lexical_Analyzer.l"
 return while_kw; 

when 23 => 
--# line 59 "Ekko_Lexical_Analyzer.l"
 return loop_kw; 

when 24 => 
--# line 61 "Ekko_Lexical_Analyzer.l"
 return colonequal; 

when 25 => 
--# line 62 "Ekko_Lexical_Analyzer.l"
 return null_kw; 

-- Relational operators
when 26 => 
--# line 66 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Less_Op         (YYLVal, YYPos); return rel_op; 

when 27 => 
--# line 67 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Greater_Op      (YYLVal, YYPos); return rel_op; 

when 28 => 
--# line 68 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Less_Equal_Op   (YYLVal, YYPos); return rel_op; 

when 29 => 
--# line 69 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Greater_Equal_Op(YYLVal, YYPos); return rel_op; 

when 30 => 
--# line 70 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Equal_Op        (YYLVal, YYPos); return rel_op; 

when 31 => 
--# line 71 "Ekko_Lexical_Analyzer.l"
 LR_Relational_Unequal_Op      (YYLVal, YYPos); return rel_op; 

-- Logical operators
when 32 => 
--# line 74 "Ekko_Lexical_Analyzer.l"
 LR_Logical_And_Op  (YYLVal, YYPos); return and_op; 

when 33 => 
--# line 75 "Ekko_Lexical_Analyzer.l"
 LR_Logical_Or_Op   (YYLVal, YYPos); return or_op; 

when 34 => 
--# line 76 "Ekko_Lexical_Analyzer.l"
 return not_op; 

-- Arithmetic operators
when 35 => 
--# line 79 "Ekko_Lexical_Analyzer.l"
 LR_Arithmetic_Add_Op  (YYLVal, YYPos); return addition_op;    

when 36 => 
--# line 80 "Ekko_Lexical_Analyzer.l"
 LR_Arithmetic_Sub_Op  (YYLVal, YYPos); return substraction_op;

when 37 => 
--# line 81 "Ekko_Lexical_Analyzer.l"
 LR_Arithmetic_Mult_Op (YYLVal, YYPos); return product_op;     

when 38 => 
--# line 82 "Ekko_Lexical_Analyzer.l"
 LR_Arithmetic_Div_Op  (YYLVal, YYPos); return division_op;    

when 39 => 
--# line 83 "Ekko_Lexical_Analyzer.l"
 LR_Arithmetic_Mod_Op  (YYLVal, YYPos); return modulus_op;     

-- Indexes
when 40 => 
--# line 86 "Ekko_Lexical_Analyzer.l"
 return comma; 

-- User input content
when 41 => 
--# line 89 "Ekko_Lexical_Analyzer.l"
 LR_Identifier       (YYLVal, YYPos, YYText); return id; 

when 42 => 
--# line 90 "Ekko_Lexical_Analyzer.l"
 LR_Literal_Integer  (YYLVal, YYPos, YYText); return literal; 

when 43 => 
--# line 91 "Ekko_Lexical_Analyzer.l"
 LR_Literal_Char     (YYLVal, YYPos, YYText); return literal; 

when 44 => 
--# line 92 "Ekko_Lexical_Analyzer.l"
 LR_Literal_String   (YYLVal, YYPos, YYText); return literal; 

when 45 => 
--# line 93 "Ekko_Lexical_Analyzer.l"
 null; 

when 46 => 
--# line 94 "Ekko_Lexical_Analyzer.l"
 null; 

-- Other
when 47 => 
--# line 97 "Ekko_Lexical_Analyzer.l"
 return Error; 

when 48 => 
--# line 99 "Ekko_Lexical_Analyzer.l"
ECHO;
when YY_END_OF_BUFFER + INITIAL + 1 => 
    return End_Of_Input;
                when YY_END_OF_BUFFER =>
                    -- undo the effects of YY_DO_BEFORE_ACTION
                    yy_ch_buf(yy_cp) := yy_hold_char;

                    yytext_ptr := yy_bp;

                    case yy_get_next_buffer is
                        when EOB_ACT_END_OF_FILE =>
                            begin
                            if ( yywrap ) then
                                -- note: because we've taken care in
                                -- yy_get_next_buffer() to have set up yytext,
                                -- we can now set up yy_c_buf_p so that if some
                                -- total hoser (like aflex itself) wants
                                -- to call the scanner after we return the
                                -- End_Of_Input, it'll still work - another
                                -- End_Of_Input will get returned.

                                yy_c_buf_p := yytext_ptr;

                                yy_act := YY_STATE_EOF((yy_start - 1) / 2);

                                goto do_action;
                            else
                                --  start processing a new file
                                yy_init := true;
                                goto new_file;
                            end if;
                            end;
                        when EOB_ACT_RESTART_SCAN =>
                            yy_c_buf_p := yytext_ptr;
                            yy_hold_char := yy_ch_buf(yy_c_buf_p);
                        when EOB_ACT_LAST_MATCH =>
                            yy_c_buf_p := yy_n_chars;
                            yy_current_state := yy_get_previous_state;

                            yy_cp := yy_c_buf_p;
                            yy_bp := yytext_ptr;
                            goto next_action;
                        when others => null;
                        end case; -- case yy_get_next_buffer()
                when others =>
                    text_io.put( "action # " );
                    text_io.put( INTEGER'IMAGE(yy_act) );
                    text_io.new_line;
                    raise AFLEX_INTERNAL_ERROR;
            end case; -- case (yy_act)
        end loop; -- end of loop waiting for end of file
end YYLex;
--# line 99 "Ekko_Lexical_Analyzer.l"

end Ekko_Lexical_Analyzer;

