with Ada.Strings; use Ada.Strings;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Maps.Constants; use Ada.Strings.Maps.Constants;

package body semantics.Assembler is

   type Register is (eax, ebx, ecx, edx, esi, edi, esp, ebp);

   procedure Init;
   procedure Init_Files;
   procedure Write_Assembler_File_Headers;
   procedure Declare_Constants;
   procedure Close_Files;
   function To_Assembler (instr: in Instruction) return String;
   function To_String (lab: in Label) return String;
   function To_String (i: in Integer) return String;
   function To_String (r: in Register) return String;
   function To_String (o: in Offset) return String;
   function To_String (o: in Occupancy) return String;
   function To_String (v: in Value) return String;


   function LD  (a: in Var_Id; R: in Register) return String;
   function LDA (x: in Var_Id; R: in Register) return String;
   function ST  (a: in Var_Id; R: in Register) return String;

   package Procedure_Id_d_Stack is new d_Stack(Proc_Id); use Procedure_Id_d_Stack;
   ps: Procedure_Id_d_Stack.Stack;

   NL: constant String(1..1):= (1 => ASCII.LF);


   procedure Generate_Assembler is
      instr: Instruction;
   begin

      Init;

      while not End_Of_File(bin_3AC_file) loop
         Read(File => bin_3AC_file,
              Item => instr);
         Put_Line(File => assembler_file,
                  Item => To_Assembler(instr));
      end loop;

      Close_Files;

   end Generate_Assembler;


   procedure Init is
   begin
      Init_Files;
      Write_Assembler_File_Headers;
      Declare_Constants;
      Empty(ps);
   end Init;


   procedure Init_Files is
   begin
      Set_Index(File => bin_3AC_file,
                To   => IO_3AC.Positive_Count'First);
      Create(File => assembler_file,
             Mode => Out_File,
             Name => To_String(filepath_no_extension) & ".s");
   end Init_Files;


   procedure Write_Assembler_File_Headers is
   begin
      Put_Line(assembler_file, ""
               & ".section .bss"   & NL
               & ".comm DISP, 100" & NL
               & ".section .text"  & NL
               & ".global _e1"     & NL
              );
   end Write_Assembler_File_Headers;

   procedure Declare_Constants is
      it: Constants_Vector_Package.Iterator;
      cin: Constant_Index;
      cid: Var_Id;
      c: Var;
   begin
      Put_Line(assembler_file, ".section .data");

      First(ct, it);
      while Is_Valid(ct, it) loop
         Get(ct, it, cin, cid);
         Next(ct, it);

         c:= vt(cid);
         case c.ut is
             when int_ut | bool_ut =>
               Put_Line(assembler_file, "  ec" & trim(c.lab'Img, Left)
                        & ": .long  " & c.v'Img);
            when char_ut =>
               Put_Line(assembler_file, "  ec" & trim(c.lab'Img, Left)
                        & ": .ascii  " & '"' & Character'Val(Integer(c.v)) & '"');
            when null_ut =>
               Put_Line(assembler_file, "  ec" & trim(c.lab'Img, Left)
                        &": .asciz  " & Get(nt, String_Id(c.v)));
            when others =>
               null;
         end case;
      end loop;

      Put_Line(assembler_file, NL & ".section .text");

   end Declare_Constants;


   function To_Assembler (instr: in Instruction) return String is
      jmp: Unbounded_String;
      reg: Register;
      lab: Label;
      param4, depth4: Natural;
      p: Proc;
   begin
      case instr.it is
         when IT_Copy =>
            return ""
              & LD(instr.y2, eax)     & NL
              & ST(instr.x2, eax)     & NL
            ;
         when IT_Query_Index =>
            return ""
              & LD(instr.z3, eax)     & NL
              & LDA(instr.y3, esi)    & NL
              & "  addl %eax, %esi"   & NL
              & "  movl (%esi), %eax" & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Assign_Index =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & LDA(instr.x3, edi)    & NL
              & "  addl %eax, %edi"   & NL
              & "  movl %ebx, (%edi)" & NL
            ;

         when IT_Add =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & "  addl  %ebx, %eax"  & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Sub =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & "  subl %ebx, %eax"   & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Mult =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & "  imull %ebx, %eax"  & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Div | IT_Mod =>
            case instr.it is
               when IT_Div => reg:= eax;
               when IT_Mod => reg:= edx;
               when others => null;
            end case;
            return ""
              & LD(instr.y3, eax)     & NL
              & "  movl %eax, %edx"   & NL
              & "  sarl $31,  %edx"   & NL
              & LD(instr.z3, ebx)     & NL
              & "  idivl %ebx"        & NL
              & ST(instr.x3, reg)     & NL
            ;
         when IT_Neg =>
            return ""
              & LD(instr.y2, eax)     & NL
              & "  negl %eax"         & NL
              & ST(instr.x2, eax)     & NL
            ;

         when IT_And =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & "  andl %ebx, %eax"   & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Or =>
            return ""
              & LD(instr.y3, eax)     & NL
              & LD(instr.z3, ebx)     & NL
              & "  orl %ebx, %eax"    & NL
              & ST(instr.x3, eax)     & NL
            ;
         when IT_Not =>
            return ""
              & LD(instr.y2, eax)     & NL
              & "  notl %eax"         & NL
              & ST(instr.x2, eax)     & NL
            ;

         when IT_Skip =>
            return ""
              & To_String(instr.e1) & ":" & NL
            ;
         when IT_Goto =>
            return ""
              & "  jmp " & To_String(instr.e1) & NL
            ;

         when IT_Eq | IT_Uneq | IT_G | IT_GE | IT_L | IT_LE =>

            case instr.it is
               when IT_Eq   => jmp:= To_Unbounded_String("jne");
               when IT_Uneq => jmp:= To_Unbounded_String("je");
               when IT_G    => jmp:= To_Unbounded_String("jle");
               when IT_GE   => jmp:= To_Unbounded_String("jl");
               when IT_L    => jmp:= To_Unbounded_String("jge");
               when IT_LE   => jmp:= To_Unbounded_String("jg");
               when others  => null;
            end case;

            lab:= New_Label(la);

            return ""
              & LD(instr.xe, eax)                            & NL
              & LD(instr.ye, ebx)                            & NL
              & "  cmpl %ebx, %eax"                          & NL
              & "  " & To_String(jmp) & " " & To_String(lab) & NL
              & "  jmp " & To_String(instr.e3)               & NL
              & To_String(lab) & ":"                         & NL
            ;

         when IT_Call =>
            p:= pt(instr.pid);
            param4 := p.pa * 4;
            case p.pt is
               when PT_Internal =>
                  return ""
                    & "  call " & To_String(p.lab)              & NL
                    & "  addl $" & To_String(param4) & ", %esp" & NL
                  ;
               when PT_External =>
                  return ""
                    & "  call _" & Get(nt, p.nid)               & NL
                    & "  addl $" & To_String(param4) & ", %esp" & NL
                  ;
            end case;

         when IT_Pmb =>
            Push(ps, instr.pid);
            p:= pt(instr.pid);
            depth4 := 4 * Natural(p.depth);

            return ""
              & "  movl $DISP, %eax"                         & NL
              & "  addl $" & To_String(depth4) & ", %eax"    & NL
              & "  pushl (%eax)"                             & NL
              & "  pushl %ebp"                               & NL
              & "  movl %esp, %ebp"                          & NL
              & "  movl %ebp, (%eax)"                        & NL
              & "  subl $" & To_String(p.occupvl) & ", %esp" & NL
            ;
         when IT_Rtn =>
            Pop(ps);
            p:= pt(instr.pid);
            depth4 := 4 * Natural(p.depth);

            return ""
              & "  movl %ebp, %esp"                       & NL
              & "  popl %ebp"                             & NL
              & "  movl $DISP, %eax"                      & NL
              & "  addl $" & To_String(depth4) & ", %eax" & NL
              & "  popl %eax"                             & NL
              & "  ret"                                   & NL
               ;

         when IT_Params =>
            return ""
              & LDA(instr.x1, eax)  & NL
              & "  pushl %eax"      & NL
            ;
         when IT_Paramc =>
            return ""
              & LDA(instr.x2, eax)  & NL
              & LD(instr.y2, ebx)   & NL
              & "  addl %ebx, %eax" & NL
              & "  pushl %eax"      & NL
            ;

      end case;
   end To_Assembler;

   function LD  (a: in Var_Id; R: in Register) return String is
      v: Var;
      p: Proc;
      depthvar, depthcode, depth4: Natural;
   begin
      v:= vt(a);
      case v.vt is
         when VT_Const =>
            -- Constant
            return "  movl $" & To_String(v.v) & ", %" & To_String(R);

         when VT_Var =>
            depthvar := Natural( pt(v.pid).depth );
            depthcode:= Natural( pt(Top(ps)).depth );

            p     := pt(v.pid);
            depth4:= 4 * Natural(p.depth);

            -- Local Variable
            if depthvar = depthcode and v.shift < 0 then
               return "  movl " & To_String(v.shift) & "(%ebp), %" & To_String(R);

               -- Local Parameter
            elsif depthvar = depthcode and v.shift > 0 then
               return "  movl " & To_String(v.shift) &"(%ebp), %esi" & NL
                 & "  movl (%esi), %" & To_String(R);

               -- Global Variable
            elsif depthvar < depthcode and v.shift < 0 then
               return "  movl $DISP, %esi"                        & NL
                 & "  movl " & To_String(depth4) & "(%esi), %esi" & NL
                 & "  movl " & To_String(v.shift) & "(%esi), %" & To_String(R);

               -- Global Parameter
            elsif depthvar < depthcode and v.shift > 0 then
               return "  movl $DISP, %esi"                         & NL
                 & "  movl " & To_String(depth4)  & "(%esi), %esi" & NL
                 & "  movl " & To_String(v.shift) & "(%esi), %esi" & NL
                 & "  movl (%esi), %" & To_String(R);
            end if;
      end case;
      return "";
   end LD;

   function LDA (x: in Var_Id; R: in Register) return String is
      v: Var;
      p: Proc;
      depthvar, depthcode, depth4: Natural;
   begin
      v:= vt(x);

      case v.vt is
         when VT_Var =>

            depthvar := Natural( pt(v.pid).depth );
            depthcode:= Natural( pt(Top(ps)).depth );

            p     := pt(v.pid);
            depth4:= 4 * Natural(p.depth);

            -- Local Variable
            if depthvar = depthcode and v.shift < 0 then
               return "  leal " & To_String(v.shift) & "(%ebp), %" & To_String(R);

               -- Local Parameter
            elsif depthvar = depthcode and v.shift > 0 then
               return "  movl " & To_String(v.shift) & "(%ebp), %" & To_String(R);

               -- Global Variable
            elsif depthvar < depthcode and v.shift < 0 then
               return ""
                 & "  movl $DISP, %esi"                                  & NL
                 & "  movl " & To_String(depth4)  & "(%esi), %esi"       & NL
                 & "  leal " & To_String(v.shift) & "(%esi), %" & To_String(R);

               -- Global Parameter
            elsif depthvar < depthcode and v.shift > 0 then
               return ""
                 & "  movl $DISP, %esi"                                  & NL
                 & "  movl " & To_String(depth4)  & "(%esi), %esi"       & NL
                 & "  movl " & To_String(v.shift) & "(%esi), %" & To_String(R);

            end if;

         when VT_Const =>
            return "  movl $ec" & trim(v.lab'Img, Left) & ", %" & To_String(R);
      end case;
      return "";
   end LDA;

   function ST  (a: in Var_Id; R: in Register) return String is
      v: Var;
      p: Proc;
      depthvar, depthcode, depth4: Natural;
   begin
      v:= vt(a);
      depthvar := Natural( pt(v.pid).depth );
      depthcode:= Natural( pt(Top(ps)).depth );

      p     := pt(v.pid);
      depth4:= 4 * Natural(p.depth);

      -- Local Variable
      if depthvar = depthcode and v.shift < 0 then
         return "  movl %" & To_String(R) & ", " & To_String(v.shift) & "(%ebp)";

         -- Local Parameter
      elsif depthvar = depthcode and v.shift > 0 then
         return ""
           & "  movl "  & To_String(v.shift) & "(%ebp), %edi" & NL
           & "  movl %" & To_String(R) & ", (%edi)";

         -- Global Variable
      elsif depthvar < depthcode and v.shift < 0 then
         return ""
           & "  movl $DISP, %esi"                            & NL
           & "  movl "  & To_string(depth4) & "(%esi), %edi" & NL
           & "  movl %" & To_String(R) & ", " & To_String(v.shift) & "(%edi)";

         -- Global Parameter
      elsif depthvar < depthcode and v.shift > 0 then
         return ""
           & "  movl $DISP, %esi" & NL
           & "  movl "  & To_string(depth4)  & "(%esi), %esi"   & NL
           & "  movl $" & To_String(v.shift) & "(%esi), %edi"   & NL
           & "  movl %" & To_String(R)       &       ", (%edi)";
      end if;
      return "";
   end ST;

   function To_String (lab: in Label) return String is
   begin
      return "_e" & Trim(lab'Img, Left);
   end To_String;

   function To_String (i: in Integer) return String is
   begin
      return Trim(i'Img, Left);
   end To_String;

   function To_String (r: in Register) return String is
   begin
      return Translate( Trim(r'Img, Left), Lower_Case_Map);
   end To_String;

   function To_String (o: in Offset) return String is
   begin
      return To_String(Integer(o));
   end To_String;

   function To_String (o: in Occupancy) return String is
   begin
      return To_String(Integer(o));
   end To_String;

   function To_String (v: in Value) return String is
   begin
      return Trim( Integer(v)'Img, Left );
   end To_String;

   procedure Close_Files is
   begin
      Close(File => assembler_file);
   end Close_Files;


end semantics.Assembler;
