with declarations; use declarations;

with declarations.d_Description; use declarations.d_Description;
with declarations.d_Symbol_Table; use declarations.d_Symbol_Table;
with declarations.d_Names_Table; use declarations.d_Names_Table;
with declarations.d_3AC; use declarations.d_3AC;
use declarations.d_3AC.Constants_Vector_Package;
with d_Stack;

with Ada.Direct_IO;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package semantics is

   procedure Get_Analysis_Ready(filepath: in String);
   procedure Get_Analysis_Done;

   procedure Generate_Symbolic_3AC_File;

   package IO_3AC is new Ada.Direct_IO(Element_Type => Instruction); use IO_3AC;
   Null_PC: constant Positive:= Positive'Last;

   successful_type_checking: Boolean;

private

   filepath_no_extension: Unbounded_String;

   bin_3AC_file: IO_3AC.File_Type;
   sym_3AC_file: Ada.Text_IO.File_Type;
   assembler_file: Ada.Text_IO.File_Type;

   nt: Name_Table;
   st: Symbol_Table;
   ct: Constants_Vector_Package.Vector;

   va: Var_Id;
   pa: Proc_Id;
   la, lac: Label;

   vt: Var_Table;
   pt: Proc_Table;
   pds: Proc_Depth_Stack;
   pd: Proc_Depth;

   bool_true_value: constant Value:= -1;
   bool_false_value: constant Value:= 0;

end semantics;
