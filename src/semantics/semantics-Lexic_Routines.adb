with declarations; use declarations;
with declarations.d_Description; use declarations.d_Description;

package body semantics.Lexic_Routines is

    procedure LR_Identifier
        (id  : out Attribute;
         pos : in Position;
         text: in String)
    is
    begin
      id:= (att => id_at,
            pos => pos, id_id => Null_Name_Id);
        put(nt, text, id.id_id);
    end LR_Identifier;


    procedure LR_Literal_Integer
        (lit : out Attribute;
         pos : in Position;
         text: in String)
    is
    begin
      lit:= (att => lit_at,
             pos => pos, lit_ut => int_ut, lit_v => Value'VALUE(text));
    end LR_Literal_Integer;

    procedure LR_Literal_Char
        (lit : out Attribute;
         pos : in Position;
         text: in String)
    is
    begin
      lit:= (att => lit_at,
             pos => pos, lit_ut => char_ut, lit_v => Value(Character'Pos(text(text'First+1))));
    end LR_Literal_Char;

    procedure LR_Literal_String
        (lit : out Attribute;
         pos : in Position;
         text: in String)
    is
        id: String_Id;
    begin
        put(nt, text, id);
      lit:= (att => lit_at,
             pos => pos, lit_ut => null_ut, lit_v => Value(id));
    end LR_Literal_String;


    procedure LR_Relational_Less_Op (rel_op: out Attribute; pos: in Position) is
    begin
      rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Less_Op);
    end LR_Relational_Less_Op;

    procedure LR_Relational_Greater_Op (rel_op: out Attribute; pos: in Position) is
    begin
        rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Greater_Op);
    end LR_Relational_Greater_Op;

    procedure LR_Relational_Less_Equal_Op (rel_op: out Attribute; pos: in Position) is
    begin
        rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Less_Equal_Op);
    end LR_Relational_Less_Equal_Op;

    procedure LR_Relational_Greater_Equal_Op (rel_op: out Attribute; pos: in Position) is
    begin
        rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Greater_Equal_Op);
    end LR_Relational_Greater_Equal_Op;

    procedure LR_Relational_Equal_Op (rel_op: out Attribute; pos: in Position) is
    begin
        rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Equal_Op);
    end LR_Relational_Equal_Op;

    procedure LR_Relational_Unequal_Op (rel_op: out Attribute; pos: in Position) is
    begin
        rel_op:= (att => rel_op_at,
                pos => pos, rel_rot => Unequal_Op);
    end LR_Relational_Unequal_Op;

   procedure LR_Arithmetic_Add_Op (arith_op: out Attribute; pos: in Position) is
    begin
        arith_op:= (att => arith_op_at,
                pos => pos, arith_aot => Add_Op);
    end LR_Arithmetic_Add_Op;

   procedure LR_Arithmetic_Sub_Op (arith_op: out Attribute; pos: in Position) is
    begin
        arith_op:= (att => arith_op_at,
                pos => pos, arith_aot => Sub_Op);
   end LR_Arithmetic_Sub_Op;

   procedure LR_Arithmetic_Mult_Op (arith_op: out Attribute; pos: in Position) is
    begin
        arith_op:= (att => arith_op_at,
                pos => pos, arith_aot => Mult_Op);
   end LR_Arithmetic_Mult_Op;

   procedure LR_Arithmetic_Div_Op (arith_op: out Attribute; pos: in Position) is
    begin
        arith_op:= (att => arith_op_at,
                pos => pos, arith_aot => Div_Op);
   end LR_Arithmetic_Div_Op;

   procedure LR_Arithmetic_Mod_Op (arith_op: out Attribute; pos: in Position) is
    begin
        arith_op:= (att => arith_op_at,
                pos => pos, arith_aot => Mod_Op);
   end LR_Arithmetic_Mod_Op;

   procedure LR_Logical_And_Op (logical_op: out Attribute; pos: in Position) is
   begin
      logical_op:= (att => logical_op_at,
                    pos => pos, log_lot => And_Op);
   end LR_Logical_And_Op;

   procedure LR_Logical_Or_Op (logical_op: out Attribute; pos: in Position) is
   begin
      logical_op:= (att => logical_op_at,
                    pos => pos, log_lot => Or_Op);
   end LR_Logical_Or_Op;

end semantics.Lexic_Routines;
