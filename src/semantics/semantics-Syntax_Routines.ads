with declarations.d_Attribute; use declarations.d_Attribute;

package semantics.Syntax_Routines is

   -- S
   procedure SR_S(s: out Attribute; proc_decl: in Attribute);

   -- Procedure
   procedure SR_Proc_Decl(proc_decl                           : out Attribute;
                          proc_preamble, decls, statements, id: in Attribute);

   procedure SR_Proc_Preamble_Simple(proc_preamble: out Attribute;
                                     id           : in Attribute);

   procedure SR_Proc_Preamble_Complex(proc_preamble    : out Attribute;
                                      pre_proc_preamble: in Attribute);

   procedure SR_Pre_Proc_Preamble_Simple(pre_proc_preamble: out Attribute;
                                         id, arg          : in Attribute);

   procedure SR_Pre_Proc_Preamble_Recursive(pre_proc_preamble           : out Attribute;
                                            inner_pre_proc_preamble, arg: in Attribute);

   procedure SR_Arg(arg           : out Attribute;
                    ida, mode, idt: in Attribute);

   procedure SR_Mode_In    (mode: out Attribute);
   procedure SR_Mode_Out   (mode: out Attribute);
   procedure SR_Mode_In_Out(mode: out Attribute);

   -- Declarations
   procedure SR_Decls(decls            : out Attribute;
                      inner_decls, decl: in Attribute);

   procedure SR_Decls(decls: out Attribute);

   procedure SR_Decl(decl      : out Attribute;
                     inner_decl: in Attribute);

   -- Constant Declaration
   procedure SR_Const_Decl(const_decl     : out Attribute;
                           idc, idt, value: in Attribute);

   procedure SR_Value(value     : out Attribute;
                      elem_value: in Attribute);

   procedure SR_Value_Negative(value     : out Attribute;
                               elem_value: in Attribute);

   procedure SR_Elem_Value_Id(elem_value: out Attribute;
                              id        : in Attribute);

   procedure SR_Elem_Value_Literal(elem_value: out Attribute;
                                   literal   : in Attribute);

   -- Variable Declaration
   procedure SR_Var_Decl(var_decl         : out Attribute;
                         id, cont_var_decl: in Attribute);

   procedure SR_Cont_Var_Decl(cont_var_decl          : out Attribute;
                              id, inner_cont_var_decl: in Attribute);

   procedure SR_Cont_Var_Decl(cont_var_decl: out Attribute;
                              id           : in Attribute);

   -- Type declaration
   procedure SR_Type_Decl(type_decl: out Attribute;
                          decl     : in Attribute);

   -- Type::Subrange Declaration
   procedure SR_Subrange_Decl(subrange_decl                    : out Attribute;
                              id, idt, lower_value, upper_value: in Attribute);

   -- Type::Record Declaration
   procedure SR_Record_Decl(record_decl    : out Attribute;
                            pre_record_decl: in Attribute);

   procedure SR_Pre_Record_Decl(pre_record_decl               : out Attribute;
                                inner_pre_record_decl, id, idt: in Attribute);

   procedure SR_Pre_Record_Decl(pre_record_decl: out Attribute;
                                id             : in Attribute);

   -- Type::Array Declaration
   procedure SR_Array_Decl(array_decl         : out Attribute;
                           pre_array_decl, idt: in Attribute);

   procedure SR_Pre_Array_Decl_Recursive(pre_array_decl           : out Attribute;
                                         inner_pre_array_decl, idt: in Attribute);

   procedure SR_Pre_Array_Decl_Simple(pre_array_decl: out Attribute;
                                      id, idt       : in Attribute);

   -- Statements
   procedure SR_Statements(statements                 : out Attribute;
                           inner_statements, statement: in Attribute);

   procedure SR_Statements(statements: out Attribute);

   procedure SR_Statement(statement      : out Attribute;
                          inner_statement: in Attribute);

   procedure SR_Cond_Statement(cond_statement  : out Attribute;
                               expr, M, statements: in Attribute);

   procedure SR_Cond_Statement(cond_statement                          : out Attribute;
                               expr, M, statements, ME, else_statements: in Attribute);

   procedure SR_Loop_Statement(loop_statement          : out Attribute;
                               M1, expr, M2, statements: in Attribute);

   procedure SR_Assig_Statement(assig_statement: out Attribute;
                                ref, expr      : in Attribute);

   procedure SR_Call_Statement(call_statement: out Attribute;
                               ref           : in Attribute);

   -- References
   procedure SR_Ref_Simple(ref: out Attribute;
                           id : in Attribute);

   procedure SR_Ref(ref          : out Attribute;
                    inner_ref, id: in Attribute);

   procedure SR_Ref_Recursive(ref            : out Attribute;
                              inner_pre_arrayOrCall: in Attribute);

   procedure SR_Pre_ArrayOrCall_Simple(pre_arrayOrCall: in out Attribute;
                                       ref, expr: in Attribute);

   procedure SR_Pre_ArrayOrCall_Recursive(pre_arrayOrCall            : out Attribute;
                                          inner_pre_arrayOrCall, expr: in Attribute);

   -- Expressions
   procedure SR_Expr_Logical(expr                 : out Attribute;
                             left_expr, logical_op, M, right_expr: in Attribute);

   procedure SR_Expr_Logical(expr      : out Attribute;
                             inner_expr: in Attribute);

   procedure SR_Expr_Relational(expr                 : out Attribute;
                                left_expr, relational_op, right_expr: in Attribute);

   procedure SR_Expr_Arithmetic(expr                 : out Attribute;
                                left_expr, arith_op, right_expr: in Attribute);

   procedure SR_Expr_Neg(expr      : out Attribute;
                         inner_expr: in Attribute);

   procedure SR_Ref(expr: out Attribute;
                    ref : in Attribute);

   procedure SR_Literal(expr   : out Attribute;
                        literal: in Attribute);

   procedure SR_Enclosing(expr      : out Attribute;
                          inner_expr: in Attribute);

end semantics.Syntax_Routines;

