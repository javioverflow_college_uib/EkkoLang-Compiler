with declarations; use declarations;
with declarations.d_Attribute;    use declarations.d_Attribute;
with declarations.d_Description;  use declarations.d_Description;
with declarations.d_Symbol_Table; use declarations.d_Symbol_Table;
with d_Stack;

with semantics; use semantics;
with Ada.Direct_IO;
use semantics.IO_3AC;

package body semantics.Code_Generation_Routines is

   procedure CG_Pre_Array_Simple
     (pre_array: out Attribute;
      ref, expr: in Attribute);
   procedure CG_Pre_Array_Recursive
     (pre_array            : out Attribute;
      inner_pre_array, expr: in Attribute);
   procedure CG_Ref_Recursive_Array
     (ref            : out Attribute;
      inner_pre_array: in Attribute);

   procedure CG_Pre_Call_Simple
     (pre_call: in out Attribute;
      ref, expr: in Attribute);
   procedure CG_Pre_Call_Recursive
     (pre_call            : in out Attribute;
      inner_pre_call, expr: in Attribute);
   procedure CG_Ref_Recursive_Call
     (ref            : out Attribute;
      inner_pre_call: in Attribute);

   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
   --|||||                                                     |||||
   --|||||                AUXILIAR FUNCTIONS                   |||||
   --|||||                                                     |||||
   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

   procedure Generate(instr: Instruction) is
   begin
      Write(File => bin_3AC_file,
            Item => instr);
   end Generate;

   procedure Dereference(r, s: in Var_Id; t: out Var_Id) is
   begin
      if s = Null_Var_Id then
         t:= r;
      else
         t:= New_Local_Var(vt    => vt,
                           pt    => pt,
                           pid   => pds(pd),
                           va    => va,
                           occup => INT_OCCUPANCY);
         Generate(instr => (it => IT_Query_Index,
                            x3 => t,
                            y3 => r,
                            z3 => s));
      end if;
   end;


   procedure Put_Label(pc: in Positive; lab: in Label) is
      instr: Instruction;
      pci, pcii: Positive;
   begin
      pci:= pc;
      while pci /= Null_PC loop
         Read(File => bin_3AC_file,
              Item => instr,
              From => IO_3AC.Positive_Count(pci));

         case instr.it is
            when IT_Skip | IT_Goto =>
               pcii:= pci;
               pci:= instr.pc1;

               instr.e1:= lab;

            when IT_Eq | IT_Uneq | IT_G | IT_GE | IT_L | IT_LE =>
               pcii:= pci;
               pci:= instr.pc3;

               instr.e3:= lab;

            when others =>
               raise Standard.Constraint_Error;

         end case;

         Write(File => bin_3AC_file,
               Item => instr,
               To   => IO_3AC.Positive_Count(pcii));
      end loop;

      Set_Index(File => bin_3AC_file,
                To   => Size(File => bin_3AC_file) + 1);
   end Put_Label;

   function Concatenate(pc1, pc2: in Positive) return Positive is
      instr: Instruction;
      pci, pcii: Positive;
   begin
      if pc1 = Null_PC then return pc2; end if;

      pci:= pc1;
      while pci /= Null_PC loop
         Read(File => bin_3AC_file,
              Item => instr,
              From => IO_3AC.Positive_Count(pci));

         case instr.it is
            when IT_Skip | IT_Goto =>
               pcii:= pci;
               pci:= instr.pc1;

            when IT_Eq | IT_Uneq | IT_G | IT_GE | IT_L | IT_LE =>
               pcii:= pci;
               pci:= instr.pc3;

            when others =>
               raise Standard.Constraint_Error;

         end case;
      end loop;

      case instr.it is
         when IT_Skip | IT_Goto =>
            instr.pc1:= pc2;

         when IT_Eq | IT_Uneq | IT_G | IT_GE | IT_L | IT_LE =>
            instr.pc3:= pc2;

         when others =>
            null;

      end case;

      Write(File => bin_3AC_file,
            Item => instr,
            To   => IO_3AC.Positive_Count(pcii));

      Set_Index(File => bin_3AC_file,
                To   => Size(File => bin_3AC_file) + 1);

      return pc1;
   end Concatenate;


   procedure ResolveBooleanExpression(ptrue, pfalse: in Positive; te: out Var_Id) is
      tt, tf: Var_Id;
      labt, labf, labfi: Label;
   begin

      labt:= New_Label(la);
      Generate(instr => (it => IT_Skip,                  -- labt: skip
                         e1 => labt, pc1 => Null_PC));
      te:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                         occup => BOOL_OCCUPANCY);
      tt:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                        v => bool_true_value, ut => bool_ut);
      Generate(instr => (it => IT_Copy,                  --       te:= tt
                         x2 => te, y2 => tt));
      labfi:= New_Label(la);
      Generate(instr => (it => IT_Goto,                  --       goto labfi
                         e1 => labfi, pc1 => Null_PC));
      labf:= New_Label(la);
      Generate(instr => (it => IT_Skip,                  -- labf: skip
                         e1 => labf, pc1 => Null_PC));
      tf:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                        v => bool_false_value, ut => bool_ut);
      Generate(instr => (it => IT_Copy,                  --        te:= tf
                         x2 => te, y2 => tf));
      Generate(instr => (it => IT_Skip,                  -- labfi: skip
                         e1 => labfi, pc1 => Null_PC));
      Put_Label(ptrue, labt);
      Put_Label(pfalse, labf);

   end ResolveBooleanExpression;



   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
   --|||||                                                     |||||
   --|||||           CODE GENERATION ROUTINES                  |||||
   --|||||                                                     |||||
   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

   procedure CG_S (s: out Attribute; proc_decl: in Attribute)
   is
      -- S: PROC_DECL
   begin
      null;
   end CG_S;

   procedure CG_MProc_Preamble is
      pid: Proc_Id;
   begin
      pd:= pd+1;
      pid:= New_Int_Proc(pt => pt,
                         pa => pa,
                         la => la,
                         pd => pd);
      pds(pd):= pid;
   end CG_MProc_Preamble;

   procedure CG_MProc_Begin is
      pid: Proc_Id;
      lab: Label;
   begin
      pid:= pds(pd);
      lab:= pt(pid).lab;
      Generate(instr => (it => IT_Skip, e1 => lab, pc1 => Null_PC));
      Generate(instr => (it => IT_Pmb, pid => pid));
   end CG_MProc_Begin;

   procedure CG_Proc_Decl
     (proc_decl                           : out Attribute;
      proc_preamble, decls, statements, id: in Attribute)
   is
      pragma Unreferenced (proc_decl);
      --  PROC_DECL:
      --       action_kw MPROC_PREAMBLE PROC_PREAMBLE is_kw
      --           DECLS
      --       begin_kw MPROC_BEGIN
      --           STATEMENTS
      --       end_kw action_kw

      pid: Proc_Id;
      ad, atd: Description;
   begin
      pid:= pds(pd); pd:= pd-1;
      Generate(instr => (it => IT_Rtn, pid => pid));
      pt(pid).pa:= proc_preamble.pp_arga;
   end CG_Proc_Decl;


   procedure CG_Proc_Preamble_Simple
     (proc_preamble: out Attribute;
      id           : in Attribute)
   is
      pragma Unreferenced (proc_preamble);
      -- PROC_PREAMBLE: id
   begin
      null;
   end CG_Proc_Preamble_Simple;


   procedure CG_Proc_Preamble_Complex
     (proc_preamble    : out Attribute;
      pre_proc_preamble: in Attribute)
   is
      pragma Unreferenced (proc_preamble);
      -- PROC_PREAMBLE: PRE_PROC_PREAMBLE closing_brackets
      ait: Argument_Iterator;
      pnid: Name_Id:= pre_proc_preamble.pp_pid;
      anid: Name_Id;
      avid: Var_Id;
      ad: Description;
      d: Description;
      error: Boolean:= False;
   begin
      First(st  => st,
            pid => pnid,
            it  => ait);
      while Is_Valid(ait) loop
         Get(st  => st,
             it  => ait,
             aid => anid,
             ad  => ad);
         avid:= New_Arg_Var(vt  => vt,
                            pt  => pt,
                            pid => pds(pd),
                            va  => va);
         case ad.argmd is
            when in_md =>
               d:= (dt => argc_d,
                    argct => ad.argt, argcv => avid);
            when in_out_md | out_md =>
               d:= (dt => var_d,
                    vt => ad.argt, vn => avid);
         end case;

         Put(st    => st,
             id    => anid,
             d     => d,
             error => error);

         Next(st => st, it => ait);
      end loop;

   end CG_Proc_Preamble_Complex;

   procedure CG_Pre_Proc_Preamble_Simple
     (pre_proc_preamble: out Attribute;
      id, arg          : in Attribute)
   is
      -- PRE_PROC_PREAMBLE: id opening_brackets ARG
   begin
      null;
   end CG_Pre_Proc_Preamble_Simple;


   procedure CG_Pre_Proc_Preamble_Recursive
     (pre_proc_preamble           : out Attribute;
      inner_pre_proc_preamble, arg: in Attribute)
   is
      -- PRE_PROC_PREAMBLE: PRE_PROC_PREAMBLE semicolon ARG
   begin
      null;
   end CG_Pre_Proc_Preamble_Recursive;


   procedure CG_Arg (arg: out Attribute; ida, mode, idt: in Attribute)
   is
      pragma Unreferenced (arg);

      -- ARG: id colon MODE id
   begin
      null;
   end CG_Arg;


   procedure CG_Mode_In (mode: out Attribute)
   is
      -- MODE: in_kw
   begin
      null;
   end CG_Mode_In;


   procedure CG_Mode_Out (mode: out Attribute)
   is
      -- MODE: out_kw
   begin
      null;
   end CG_Mode_Out;


   procedure CG_Mode_In_Out (mode: out Attribute)
   is
      -- MODE: in_kw out_kw
   begin
      null;
   end CG_Mode_In_Out;


   procedure CG_Decls
     (decls            : out Attribute;
      inner_decls, decl: in Attribute)
   is
      -- DECLS: DECLS DECL
   begin
      null;
   end CG_Decls;


   procedure CG_Decls (decls: out Attribute)
   is
      -- DECLS: -- Lambda
   begin
      null;
   end CG_Decls;


   procedure CG_Decl (decl: out Attribute; inner_decl: in Attribute)
   is
      --  DECL:
      --       CONST_DECL
      --    |  VAR_DECL
      --    |  TYPE_DECL
      --    |  PROC_DECL
   begin
      null;
   end CG_Decl;


   procedure CG_Const_Decl
     (const_decl     : out Attribute;
      idc, idt, value: in Attribute)
   is
      -- CONST_DECL: id colon constant_kw id colonequal VALUE
   begin
      null;
   end CG_Const_Decl;


   procedure CG_Value (value: out Attribute; elem_value: in Attribute)
   is
      -- VALUE: ELEM_VALUE
   begin
      value.v_vid:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                                 v  => elem_value.ev_v,
                                 ut => elem_value.ev_ut);
   end CG_Value;


   procedure CG_Value_Negative
     (value     : out Attribute;
      elem_value: in Attribute)
   is
      -- VALUE: substraction_op ELEM_VALUE
   begin
      value.v_vid:= New_Constant(vt => vt, va => va,  ct => ct, la => lac,
                                 v  => -elem_value.ev_v,
                                 ut => elem_value.ev_ut);
   end CG_Value_Negative;


   procedure CG_Elem_Value_Id (elem_value: out Attribute; id: in Attribute)
   is
      -- ELEM_VALUE: id
   begin
      null;
   end CG_Elem_Value_Id;


   procedure CG_Elem_Value_Literal
     (elem_value: out Attribute;
      literal   : in Attribute)
   is
      -- ELEM_VALUE: literal
   begin
      null;
   end CG_Elem_Value_Literal;
   procedure CG_Var_Decl
     (var_decl         : out Attribute;
      id, cont_var_decl: in Attribute)
   is
      -- VAR_DECL: id CONT_VAR_DECL
   begin
      null;
   end CG_Var_Decl;


   procedure CG_Cont_Var_Decl
     (cont_var_decl          : out Attribute;
      id, inner_cont_var_decl: in Attribute)
   is
      -- CONT_VAR_DECL: comma id CONT_VAR_DECL
   begin
      null;
   end CG_Cont_Var_Decl;


   procedure CG_Cont_Var_Decl
     (cont_var_decl: out Attribute;
      id           : in Attribute)
   is
      -- CONT_VAR_DECL: colon id semicolon
   begin
      null;
   end CG_Cont_Var_Decl;


   procedure CG_Type_Decl (type_decl: out Attribute; decl: in Attribute)
   is
      --  TYPE_DECL:
      --       SUBRANGE_DECL
      --    |  ARRAY_DECL
      --    |  RECORD_DECL
   begin
      null;
   end CG_Type_Decl;


   procedure CG_Subrange_Decl
     (subrange_decl                    : out Attribute;
      id, idt, lower_value, upper_value: in Attribute)
   is
      --  SUBRANGE_DECL:
      --          type_kw id is_kw new_kw id range_kw VALUE point point VALUE
   begin
      null;
   end CG_Subrange_Decl;


   procedure CG_Record_Decl
     (record_decl    : out Attribute;
      pre_record_decl: in Attribute)
   is
      --  RECORD_DECL: PRE_RECORD_DECL end_kw record_kw semicolon
   begin
      null;
   end CG_Record_Decl;


   procedure CG_Pre_Record_Decl
     (pre_record_decl               : out Attribute;
      inner_pre_record_decl, id, idt: in Attribute)
   is
      -- PRE_RECORD_DECL: PRE_RECORD_DECL id colon id semicolon
   begin
      null;
   end CG_Pre_Record_Decl;


   procedure CG_Pre_Record_Decl
     (pre_record_decl: out Attribute;
      id             : in Attribute)
   is
      -- PRE_RECORD_DECL: type_kw id is_kw record_kw
   begin
      null;
   end CG_Pre_Record_Decl;


   procedure CG_Pre_Array_Decl_Simple
     (pre_array_decl: out Attribute;
      id, idt       : in Attribute)
   is
      -- PRE_ARRAY_DECL: type_kw id is_kw array_kw opening_brackets id
      dti: Description;
      lb: Value;
   begin
      dti:= Get(st => st, id => idt.id_id);
      lb:= dti.td.lb;
      pre_array_decl.pad_b:= Offset(lb);
   end CG_Pre_Array_Decl_Simple;


   procedure CG_Pre_Array_Decl_Recursive
     (pre_array_decl           : out Attribute;
      inner_pre_array_decl, idt: in Attribute)
   is
      -- PRE_ARRAY_DECL: PRE_ARRAY_DECL comma id
      dti: Description;
      n, lb: Value;
   begin
      dti:= Get(st => st, id => idt.id_id);
      n := dti.td.ub - dti.td.lb + 1;
      lb:= dti.td.lb;
      pre_array_decl.pad_b:= inner_pre_array_decl.pad_b * Offset(n) + Offset(lb);
   end CG_Pre_Array_Decl_Recursive;


   procedure CG_Array_Decl
     (array_decl         : out Attribute;
      pre_array_decl, idt: in Attribute)
   is
      pragma Unreferenced (array_decl);

      -- ARRAY_DECL: PRE_ARRAY_DECL closing_brackets of_kw id semicolon
      da: Description;
   begin
      da:= Get(st, pre_array_decl.pad_ida);
      da.td.b:= pre_array_decl.pad_b;
      Update(st, pre_array_decl.pad_ida, da);
   end CG_Array_Decl;


   procedure CG_Statements
     (statements                 : out Attribute;
      inner_statements, statement: in Attribute)
   is
      -- STATEMENTS: STATEMENTS STATEMENT
   begin
      null;
   end CG_Statements;


   procedure CG_Statements (statements: out Attribute)
   is
      -- STATEMENTS: null_kw semicolon
   begin
      null;
   end CG_Statements;


   procedure CG_Statement
     (statement      : out Attribute;
      inner_statement: in Attribute)
   is
      --  STATEMENT:
      --       COND_STATEMENT
      --    |  LOOP_STATEMENT
      --    |  ASSIG_STATEMENT
      --    |  CALL_STATEMENT
   begin
      null;
   end CG_Statement;

   procedure CG_M_Expr (M: out Attribute) is
      lab: Label;
   begin
      lab:= New_Label(la);
      Generate(instr => (it => IT_Skip,
                         e1 => lab, pc1 => Null_PC));
      M:= (att => mark_expr, pos => (0,0),
           m_lab => lab);
   end CG_M_Expr;

   procedure CG_M_Else (ME: out Attribute) is
      lab, labfi: Label;
   begin
      -- goto labfi
      labfi:= New_Label(la);
      Generate(instr => (it => IT_Goto,
                         e1 => labfi, pc1 => Null_PC));

      -- lab: skip
      lab:= New_Label(la);
      Generate(instr => (it => IT_Skip,
                         e1 => lab, pc1 => Null_PC));

      ME:= (att => mark_else, pos => (0,0),
            me_lab => lab, me_labfi => labfi);
   end CG_M_Else;

   procedure CG_Cond_Statement
     (cond_statement  : out Attribute;
      expr, M, statements: in Attribute)
   is
      pragma Unreferenced (cond_statement);
      --  COND_STATEMENT:
      --       if_kw EXPR M_EXPR then_kw
      --           STATEMENTS
      --       end_kw if_kw semicolon
      lab: Label;
   begin
      Put_Label(expr.e_ptrue, M.m_lab);

      -- lab: skip
      lab:= New_Label(la);
      Generate(instr => (it => IT_Skip,
                         e1 => lab, pc1 => Null_PC));
      Put_Label(expr.e_pfalse, lab);
   end CG_Cond_Statement;


   procedure CG_Cond_Statement
     (cond_statement                   : out Attribute;
      expr, M, statements, ME, else_statements: in Attribute)
   is
      pragma Unreferenced (cond_statement);
      --  COND_STATEMENT:
      --    if_kw EXPR M_EXPR then_kw
      --       STATEMENTS
      --    else_kw M_ELSE
      --       STATEMENTS
      --    end_kw if_kw semicolon
   begin
      Put_Label(expr.e_ptrue, M.m_lab);
      Put_Label(expr.e_pfalse, ME.me_lab);
      Generate(instr => (it => IT_Skip,
                         e1 => ME.me_labfi, pc1 => Null_PC));
   end CG_Cond_Statement;


   procedure CG_Loop_Statement
     (loop_statement          : out Attribute;
      M1, expr, M2, statements: in Attribute)
   is
      pragma Unreferenced (loop_statement);
      --  LOOP_STATEMENT:
      --       while_kw M_EXPR EXPR loop_kw M_EXPR
      --          STATEMENTS
      --       end_kw loop_kw semicolon
      labf: Label;
   begin
      Generate(instr => (it => IT_Goto,
                         e1 => M1.m_lab, pc1 => Null_PC));
      labf:= New_Label(la);
      Generate(instr => (it => IT_Skip,
                         e1 => labf, pc1 => Null_PC));
      Put_Label(expr.e_ptrue, M2.m_lab);
      Put_Label(expr.e_pfalse, labf);
   end CG_Loop_Statement;

   --////////

   procedure CG_Assig_Statement
     (assig_statement: out Attribute;
      ref, expr      : in Attribute)
   is
      pragma Unreferenced (assig_statement);

      --  ASSIG_STATEMENT:
      --       REF colonequal EXPR semicolon

      te, t1, t2: Var_Id;
   begin
      if expr.e_ut /= bool_ut then
         Dereference(r => expr.e_r,
                     s => expr.e_shift,
                     t => te);
      else
         ResolveBooleanExpression(ptrue => expr.e_ptrue,
                                  pfalse => expr.e_pfalse,
                                  te   => te);
      end if;

      if ref.r_const_shift=0 and ref.r_relat_shift=Null_Var_Id then
         Generate(instr => (it => IT_Copy,
                            x2 => ref.r_r, y2 => te));

      elsif ref.r_const_shift=0 and ref.r_relat_shift/=Null_Var_Id then
         Generate(instr => (it => IT_Assign_Index,
                            x3 => ref.r_r, y3 => ref.r_relat_shift, z3 => te));

      elsif ref.r_const_shift/=0 and ref.r_relat_shift=Null_Var_Id then
         t1:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                           v  => Value(ref.r_const_shift), ut => int_ut);
         Generate(instr => (it => IT_Assign_Index,
                            x3 => ref.r_r, y3 => t1, z3 => te));

      elsif ref.r_const_shift/=0 and ref.r_relat_shift/=Null_Var_Id then
         t1:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                           v  => Value(ref.r_const_shift), ut => int_ut);
         t2:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                            occup => INT_OCCUPANCY);
         Generate(instr => (it => IT_Add,
                            x3 => t2, y3 => ref.r_relat_shift, z3 => t1));
         Generate(instr => (it => IT_Assign_Index,
                            x3 => ref.r_r, y3 => t2, z3 => te));

      end if;

   end CG_Assig_Statement;


   procedure CG_Call_Statement
     (call_statement: out Attribute;
      ref           : in Attribute)
   is
      pragma Unreferenced (call_statement);
      --  CALL_STATEMENT:
      --       REF semicolon
      d: Description;
   begin
      d:= Get(st, ref.c_nid);
      Generate(instr => (it => IT_Call,
                         pid => d.pn));
   end CG_Call_Statement;


   procedure CG_Ref_Simple (ref: out Attribute; id: in Attribute)
   is
      -- REF: id
   begin
      null;
   end CG_Ref_Simple;


   procedure CG_Ref (ref: out Attribute; inner_ref, id: in Attribute)
   is
      -- REF: REF point id
      fd: Description;
   begin
      fd:= Get_Field(st  => st,
                     rid => inner_ref.r_idt,
                     fid => id.id_id);
      ref.r_r           := inner_ref.r_r;
      ref.r_const_shift := inner_ref.r_const_shift + fd.fo;
      ref.r_relat_shift := inner_ref.r_relat_shift;
   end CG_Ref;


   procedure CG_Pre_ArrayOrCall_Simple
     (pre_arrayOrCall: in out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR
   begin
      if ref.att = call_at then
         CG_Pre_Call_Simple(pre_call => pre_arrayOrCall,
                            ref      => ref,
                            expr     => expr);
      else
         CG_Pre_Array_Simple(pre_array => pre_arrayOrCall,
                             ref       => ref,
                             expr      => expr);
      end if;
   end CG_Pre_ArrayOrCall_Simple;

   procedure CG_Pre_ArrayOrCall_Recursive
     (pre_arrayOrCall            : out Attribute;
      inner_pre_arrayOrCall, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR
   begin
      if inner_pre_arrayOrCall.att = pre_array_at then
         CG_Pre_Array_Recursive(pre_array       => pre_arrayOrCall,
                                inner_pre_array => inner_pre_arrayOrCall,
                                expr            => expr);
      else
         CG_Pre_Call_Recursive(pre_call       => pre_arrayOrCall,
                               inner_pre_call => inner_pre_arrayOrCall,
                               expr           => expr);
      end if;
   end CG_Pre_ArrayOrCall_Recursive;

   procedure CG_Ref_Recursive
     (ref            : out Attribute;
      inner_pre_arrayOrCall: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets
   begin
      if inner_pre_arrayOrCall.att = pre_array_at then
         CG_Ref_Recursive_Array(ref             => ref,
                                inner_pre_array => inner_pre_arrayOrCall);
      else
         CG_Ref_Recursive_Call(ref            => ref,
                               inner_pre_call => inner_pre_arrayOrCall);
      end if;
   end CG_Ref_Recursive;


   --///// ARRAY
   procedure CG_Pre_Array_Simple
     (pre_array: out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR
      te: Var_Id;
      it: Index_Iterator;
   begin
      if expr.e_ut /= bool_ut then
         Dereference(r => expr.e_r,
                     s => expr.e_shift,
                     t => te);
      else
         ResolveBooleanExpression(ptrue => expr.e_ptrue,
                                  pfalse => expr.e_pfalse,
                                  te   => te);
      end if;

      pre_array.pa_r:= ref.r_r;
      pre_array.pa_const_shift:= ref.r_const_shift;
      pre_array.pa_relat_shift:= ref.r_relat_shift;
      pre_array.pa_shift:= te;

      First(st  => st,
            aid => ref.r_idt,
            it  => it);
      pre_array.pa_it_cg:= it;

   end CG_Pre_Array_Simple;

   procedure CG_Pre_Array_Recursive
     (pre_array            : out Attribute;
      inner_pre_array, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR
      te: Var_Id;
      it: Index_Iterator;
      aid: Description;
      aitd: Description;
      n: Value;
      tn: Var_Id;
      t1, t2: Var_Id;
   begin
      if expr.e_ut /= bool_ut then
         Dereference(r => expr.e_r,
                     s => expr.e_shift,
                     t => te);
      else
         ResolveBooleanExpression(ptrue => expr.e_ptrue,
                                  pfalse => expr.e_pfalse,
                                  te   => te);
      end if;

      -- Index occupation in array ('n')
      it:= inner_pre_array.pa_it_cg;
      Next(st, it);
      Get(st, it, aid);
      aitd:= Get(st, aid.it);
      n:= aitd.td.ub - aitd.td.lb + 1;
      tn:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                        v  => n, ut => int_ut);

      -- (i1 * n2 + i2)
      t1:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                         occup => INT_OCCUPANCY);
      Generate(instr => (it => IT_Mult,
                         x3 => t1, y3 => inner_pre_array.pa_shift, z3 => tn));
      t2:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                         occup => INT_OCCUPANCY);
      Generate(instr => (it => IT_Add,
                         x3 => t2, y3 => t1, z3 => te));
      pre_array.pa_shift:= t2;
      pre_array.pa_it_cg:= it;
   end CG_Pre_Array_Recursive;

   procedure CG_Ref_Recursive_Array
     (ref            : out Attribute;
      inner_pre_array: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets
      da: Description;
      tb, t1: Var_Id;
      idtc: Name_Id;
      dc: Description;
      tw, t2, t3: Var_Id;
   begin
      -- (dinamic_shift - static_shift) * w
      da:= Get(st, inner_pre_array.pa_adt);
      tb:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                        v  => Value(da.td.b), ut => int_ut);
      t1:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                         occup => INT_OCCUPANCY);
      Generate(instr => (it => IT_Sub,
                         x3 => t1, y3 => inner_pre_array.pa_shift, z3 => tb));
      idtc:= da.td.ct;
      dc:= Get(st, idtc);
      tw:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                        v  => Value(dc.td.occu), ut => int_ut);
      t2:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                         occup => INT_OCCUPANCY);
      Generate(instr => (it => IT_Mult,
                         x3 => t2, y3 => t1, z3 => tw));
      if inner_pre_array.pa_relat_shift /= Null_Var_Id then
         t3:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                            occup => INT_OCCUPANCY);
         Generate(instr => (it => IT_Add,
                            x3 => t3, y3 => t2, z3 => inner_pre_array.pa_relat_shift));

         -- Set output data
         ref.r_relat_shift:= t3;
      else
         ref.r_relat_shift:= t2;
      end if;

      ref.r_r:= inner_pre_array.pa_r;
      ref.r_const_shift:= inner_pre_array.pa_const_shift;

   end CG_Ref_Recursive_Array;


   --///// CALL

   procedure CG_Pre_Call_Simple
     (pre_call: in out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_CALL: REF opening_brackets EXPR

      it: Argument_Iterator:= pre_call.cp_it;
      pnid: Name_Id; -- Param Name_Id
      pd, ptd: Description; -- Param type description
      aexpr: Arg_Expression;
      lab: Label;
   begin
      Get(st, it, pnid, pd);
      aexpr:= (var  => expr.e_r,
               shift => expr.e_shift);

      if expr.e_ut = bool_ut then
            case pd.argmd is
               when in_md =>
                  ResolveBooleanExpression(ptrue  => expr.e_ptrue,
                                           pfalse => expr.e_pfalse,
                                           te     => aexpr.var);
                  aexpr.shift:= Null_Var_Id;
               when out_md | in_out_md =>
                  lab:= New_Label(la);
                  Generate(instr => (it => IT_Skip,
                                     e1 => lab, pc1 => Null_PC));
                  Put_Label(expr.e_ptrue, lab);
                  Put_Label(expr.e_pfalse, lab);
            end case;
         end if;

      Args_Stack.Push(pre_call.cp_args, aexpr);
   end CG_Pre_Call_Simple;

   procedure CG_Pre_Call_Recursive
     (pre_call            : in out Attribute;
      inner_pre_call, expr: in Attribute)
   is
      --  PRE_CALL: PRE_CALL comma EXPR

      it: Argument_Iterator:= pre_call.cp_it;
      pnid: Name_Id;
      pd, ptd: Description;
      aexpr: Arg_Expression;
      lab: Label;
   begin
      Get(st, it, pnid, pd);
      aexpr:= (var  => expr.e_r,
               shift => expr.e_shift);

      if expr.e_ut = bool_ut then
            case pd.argmd is
               when in_md =>
                  ResolveBooleanExpression(ptrue  => expr.e_ptrue,
                                           pfalse => expr.e_pfalse,
                                           te     => aexpr.var);
                  aexpr.shift:= Null_Var_Id;
               when out_md | in_out_md =>
                  lab:= New_Label(la);
                  Generate(instr => (it => IT_Skip,
                                     e1 => lab, pc1 => Null_PC));
                  Put_Label(expr.e_ptrue, lab);
                  Put_Label(expr.e_pfalse, lab);
            end case;
         end if;

      Args_Stack.Push(pre_call.cp_args, aexpr);
   end CG_Pre_Call_Recursive;

   procedure CG_Ref_Recursive_Call
     (ref            : out Attribute;
      inner_pre_call: in Attribute)
   is
      pragma Unreferenced (ref);

      -- REF: PRE_CALL closing_brackets
      s: Args_Stack.Stack;
      arg: Arg_Expression;
      d: Description;
   begin
      s:= inner_pre_call.cp_args;
      while not Args_Stack.Is_Empty(s) loop
         arg:= Args_Stack.Top(s); Args_Stack.Pop(s);

         if arg.shift = Null_Var_Id then
            Generate(instr => (it => IT_Params,
                               x1 => arg.var));
         else
            Generate(instr => (it => IT_Paramc,
                               x2 => arg.var, y2 => arg.shift));
         end if;
      end loop;

   end CG_Ref_Recursive_Call;

   -- Expressions -------------------------------------------------

   procedure CG_Expr_Logical
     (expr                 : out Attribute;
      left_expr, logical_op, M, right_expr: in Attribute)
   is
      --  EXPR:
      --       EXPR and_op M_EXPR EXPR
      --    |  EXPR or_op M_EXPR EXPR
   begin
      case logical_op.log_lot is
         when And_Op =>
            Put_Label(left_expr.e_ptrue, M.m_lab);
            expr.e_ptrue:= right_expr.e_ptrue;
            expr.e_pfalse:= Concatenate(left_expr.e_pfalse, right_expr.e_pfalse);
         when Or_Op =>
            Put_Label(left_expr.e_pfalse, M.m_lab);
            expr.e_pfalse:= right_expr.e_pfalse;
            expr.e_ptrue:= Concatenate(left_expr.e_ptrue, right_expr.e_ptrue);
         when others =>
            null;
      end case;
   end CG_Expr_Logical;


   procedure CG_Expr_Logical (expr: out Attribute; inner_expr: in Attribute)
   is
      --  EXPR: not_op EXPR
   begin
      expr.e_ptrue:= inner_expr.e_pfalse;
      expr.e_pfalse:= inner_expr.e_ptrue;
   end CG_Expr_Logical;


   procedure CG_Expr_Relational
     (expr                 : out Attribute;
      left_expr, relational_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR rel_op EXPR
      tl, tr: Var_Id;
      instr: Instruction;
   begin
      Dereference(left_expr.e_r, left_expr.e_shift, tl);
      Dereference(right_expr.e_r, right_expr.e_shift, tr);

      -- if tl op tr then goto [x]
      expr.e_ptrue:= Positive(Index(bin_3AC_file));
      case relational_op.rel_rot is
         when Equal_Op =>
            instr:= (it => IT_Eq,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
         when Unequal_Op =>
            instr:= (it => IT_Uneq,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
         when Less_Op =>
            instr:= (it => IT_L,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
         when Less_Equal_Op =>
            instr:= (it => IT_LE,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
         when Greater_Op =>
            instr:= (it => IT_G,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
         when Greater_Equal_Op =>
            instr:= (it => IT_GE,
                     e3 => Null_Label, pc3 => Null_PC,
                     xe => tl, ye => tr);
      end case;
      Generate(instr => instr);

      -- goto [x]
      expr.e_pfalse:= Positive(Index(bin_3AC_file));
      Generate(instr => (it => IT_Goto,
                         e1 => Null_Label, pc1 => Null_PC));


   end CG_Expr_Relational;


   procedure CG_Expr_Arithmetic
     (expr                 : out Attribute;
      left_expr, arith_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR addition_op EXPR
      --    |  EXPR substraction_op EXPR
      --    |  EXPR product_op EXPR
      --    |  EXPR division_op EXPR
      --    |  EXPR modulus_op EXPR
      tl, tr, vid: Var_Id;
      dt: Description;
      instr: Instruction;
   begin
      Dereference(left_expr.e_r, left_expr.e_shift, tl);
      Dereference(right_expr.e_r, right_expr.e_shift, tr);
      vid:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                          occup => INT_OCCUPANCY);
      case arith_op.arith_aot is
         when Add_Op  => instr:= (it => IT_Add,
                                  x3 => vid,
                                  y3 => tl,
                                  z3 => tr);

         when Sub_Op  => instr:= (it => IT_Sub,
                                  x3 => vid,
                                  y3 => tl,
                                  z3 => tr);

         when Mult_Op => instr:= (it => IT_Mult,
                                  x3 => vid,
                                  y3 => tl,
                                  z3 => tr);

         when Div_Op  => instr:= (it => IT_Div,
                                  x3 => vid,
                                  y3 => tl,
                                  z3 => tr);

         when Mod_Op  => instr:= (it => IT_Mod,
                                  x3 => vid,
                                  y3 => tl,
                                  z3 => tr);
      end case;
      Generate(instr);
      expr.e_r:= vid;
   end CG_Expr_Arithmetic;


   procedure CG_Expr_Neg (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: substraction_op EXPR %prec un_subs_op
      t, vid: Var_Id;
   begin
      Dereference(inner_expr.e_r, inner_expr.e_shift, t);
      vid:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                          occup => INT_OCCUPANCY);
      Generate(instr => (it => IT_Neg,
                         x2 => vid,
                         y2 => t));
      expr.e_r:= vid;
   end CG_Expr_Neg;


   procedure CG_Ref (expr: out Attribute; ref: in Attribute)
   is
      -- EXPR: REF
      t1, t2: Var_Id;
      refvar: Var;
      t, tm1: Var_Id;
      pc: Positive;
   begin
      expr.e_r:= ref.r_r;
      if ref.r_const_shift=0 and ref.r_relat_shift=Null_Var_Id then
         expr.e_shift:= Null_Var_Id;

      elsif ref.r_const_shift=0 and ref.r_relat_shift/=Null_Var_Id then
         expr.e_shift:= ref.r_relat_shift;

      elsif ref.r_const_shift/=0 and ref.r_relat_shift=Null_Var_Id then
         expr.e_shift:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                                     v => Value(ref.r_const_shift), ut => int_ut);

      elsif ref.r_const_shift/=0 and ref.r_relat_shift/=Null_Var_Id then
         t1:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                           v => Value(ref.r_const_shift), ut => int_ut);
         t2:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                            occup => INT_OCCUPANCY);
         Generate(instr => (it => IT_Add,
                            x3 => t2, y3 => ref.r_relat_shift, z3 => t1));
         expr.e_shift:= t2;
      end if;

      -- Constant true and false
      refvar:= vt(ref.r_r);
      if refvar.vt = VT_Const and then refvar.ut = bool_ut then

         if refvar.v = bool_false_value then
            expr.e_pfalse:= Positive(Index(File => bin_3AC_file));
         elsif refvar.v = bool_true_value then
            expr.e_ptrue:= Positive(Index(File => bin_3AC_file));
         end if;

         Generate(instr => (it => IT_Goto,
                            e1 => Null_Label, pc1 => Null_PC));

         -- Boolean vars
      elsif ref.r_ut = bool_ut then
         Dereference(expr.e_r, expr.e_shift, t);
         tm1:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                            v => bool_true_value, ut => bool_ut);

         pc:= Positive(Index(bin_3AC_file));
         Generate(instr => (it => IT_Eq,
                            e3 => Null_Label, pc3 => Null_PC, xe => t, ye => tm1));
         expr.e_ptrue:= pc;

         pc:= Positive(Index(bin_3AC_file));
         Generate(instr => (it => IT_Goto,
                            e1 => Null_Label, pc1 => Null_PC));
         expr.e_pfalse:= pc;
      end if;
   end CG_Ref;


   procedure CG_Literal (expr: out Attribute; literal: in Attribute)
   is
      -- EXPR: literal
   begin
      expr.e_r:= New_Constant(vt => vt, va => va, ct => ct, la => lac,
                              v => literal.lit_v, ut => literal.lit_ut);
   end CG_Literal;


   procedure CG_Enclosing (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: opening_brackets EXPR closing_brackets
   begin
      null;
   end CG_Enclosing;


end semantics.Code_Generation_Routines;
