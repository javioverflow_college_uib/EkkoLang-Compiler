with declarations;                use declarations;
with declarations.d_Description;  use declarations.d_Description;
with declarations.d_Symbol_Table; use declarations.d_Symbol_Table;
with declarations.d_3AC;          use declarations.d_3AC;
with semantics.Syntax_Routines;   use semantics.Syntax_Routines;
with semantics.Messages;          use semantics.Messages;

package body semantics.Type_Checking_Routines is

   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
   --|||||                                                     |||||
   --|||||                PREDECLARATIONS                      |||||
   --|||||                                                     |||||
   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

   procedure Register_Procedure(idp: in Attribute);
   procedure Register_Variable(id: in Attribute; idt: in Name_Id);
   procedure Check_If_Expression_Matches_Index_Type(it: in Index_Iterator;
                                                    expr: in Attribute);
   procedure Check_If_Expression_Matches_Param_Type(it: in Argument_Iterator;
                                                    expr: in Attribute);

   procedure TC_Ref_Recursive_Array(ref: out Attribute; inner_pre_array: in Attribute);
   procedure TC_Ref_Recursive_Call(ref: out Attribute; inner_pre_call: in Attribute);

   procedure TC_Pre_Call_Simple(pre_call: in out Attribute; ref, expr: in Attribute);
   procedure TC_Pre_Array_Simple(pre_array: out Attribute; ref, expr: in Attribute);

   procedure TC_Pre_Array_Recursive(pre_array : out Attribute;
                                    inner_pre_array, expr: in Attribute);
   procedure TC_Pre_Call_Recursive (pre_call : in out Attribute;
                                    inner_pre_call, expr: in Attribute);

   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
   --|||||                                                     |||||
   --|||||           TYPE CHECKING ROUTINES                    |||||
   --|||||                                                     |||||
   --|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

   -- S -------------------------------------------------------------------------------------------

   procedure TC_S (s: out Attribute; proc_decl: in Attribute)
   is
      -- S: PROC_DECL
   begin
      if proc_decl.p_arga /= 0 then
         successful_type_checking:= False;
         error_main_procedure_with_args(proc_decl.pos);
      end if;
      s:= (att => ok_at, pos => proc_decl.pos);

   end TC_S;

   -- Procedure -----------------------------------------------------------------------------------

   procedure TC_Proc_Decl
     (proc_decl                           : out Attribute;
      proc_preamble, decls, statements, id: in Attribute)
   is
      --  PROC_DECL:
      --       action_kw PROC_PREAMBLE is_kw
      --           DECLS
      --       begin_kw
      --           STATEMENTS
      --       end_kw id semicolon
   begin
      Exit_Block(st);
      if proc_preamble.pp_pid /= id.id_id then
         successful_type_checking:= False;
         error_end_name_does_not_match(id.pos, proc_preamble.pp_pid, id.id_id);
      end if;
      proc_decl:= (att => proc_at,
                   pos => proc_preamble.pos, p_arga => proc_preamble.pp_arga);
   end TC_Proc_Decl;


   procedure TC_Proc_Preamble_Simple
     (proc_preamble: out Attribute;
      id           : in Attribute)
   is
      -- PROC_PREAMBLE: id
   begin

      Register_Procedure(id);
      proc_preamble:= (att => proc_preamble_at,
                       pos => id.pos, pp_pid => id.id_id, pp_arga => 0);
      Enter_Block(st);

   end TC_Proc_Preamble_Simple;


   procedure Register_Procedure(idp: in Attribute) is
      d: Description;
      error: Boolean:= False;
   begin

      -- Add to symbol table a new procedure description
      d:= (dt => proc_d, pn => pa);
      Put(st, idp.id_id, d, error);

      -- Check if it was succesful
      if error then
         successful_type_checking:= False;
         error_identifier_already_seen(idp.pos, idp.id_id);
      end if;

   end Register_Procedure;


   procedure TC_Proc_Preamble_Complex
     (proc_preamble    : out Attribute;
      pre_proc_preamble: in Attribute)
   is
      -- PROC_PREAMBLE: PRE_PROC_PREAMBLE closing_brackets

      pid: Name_Id renames pre_proc_preamble.pp_pid;
      arga: Natural renames pre_proc_preamble.pp_arga;
      ad  : Description;
      d   : Description;
      error: Boolean:= False;
   begin

      proc_preamble:= pre_proc_preamble;
      Enter_Block(st);

   end TC_Proc_Preamble_Complex;


   procedure TC_Pre_Proc_Preamble_Simple
     (pre_proc_preamble: out Attribute;
      id, arg          : in Attribute)
   is
      -- PRE_PROC_PREAMBLE: id opening_brackets ARG

      ad, atd: Description;
      error: Boolean;
   begin

      Register_Procedure(id);

      -- Register Argument
      ad:= (dt => arg_d,
            argt => arg.a_idt, argmd => arg.a_md);
      Put_Argument(st, id.id_id, arg.a_ida, ad, error);

      -- Set output data
      pre_proc_preamble:= (att => proc_preamble_at,
                           pos => id.pos, pp_pid => id.id_id, pp_arga => 1);

   end TC_Pre_Proc_Preamble_Simple;


   procedure TC_Pre_Proc_Preamble_Recursive
     (pre_proc_preamble           : out Attribute;
      inner_pre_proc_preamble, arg: in Attribute)
   is
      -- PRE_PROC_PREAMBLE: PRE_PROC_PREAMBLE semicolon ARG

      pid: Name_Id renames inner_pre_proc_preamble.pp_pid;
      ad : Description; -- Argument description
      error: Boolean;
   begin

      -- Register argument description
      ad:= (dt => arg_d,
            argt => arg.a_idt, argmd => arg.a_md);
      Put_Argument(st, pid, arg.a_ida, ad, error);
      if error then
         successful_type_checking:= False;
         error_argument_name_already_registered(arg.pos, pid, arg.a_ida);
      end if;

      -- Register argument as variable or constant


      -- Set output data
      pre_proc_preamble:= inner_pre_proc_preamble;
      pre_proc_preamble.pp_arga:= pre_proc_preamble.pp_arga+1;

   end TC_Pre_Proc_Preamble_Recursive;


   procedure TC_Arg (arg: out Attribute; ida, mode, idt: in Attribute)
   is
      -- ARG: id colon MODE id

      atd: Description; -- Argument type description
   begin
      -- Check argument type
      atd:= Get(st, idt.id_id);
      if atd.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;

      arg:= (att => arg_at, pos => ida.pos,
             a_ida => ida.id_id, a_idt => idt.id_id, a_md => mode.m_md,
             a_av => Null_Var_Id);
   end TC_Arg;


   procedure TC_Mode_In (mode: out Attribute)
   is
      -- MODE: in_kw
   begin
      mode:= (att => mode_at,
              pos => (0, 0), m_md => in_md);
   end TC_Mode_In;


   procedure TC_Mode_Out (mode: out Attribute)
   is
      -- MODE: out_kw
   begin
      mode:= (att => mode_at,
              pos => (0, 0), m_md => out_md);
   end TC_Mode_Out;


   procedure TC_Mode_In_Out (mode: out Attribute)
   is
      -- MODE: in_kw out_kw
   begin
      mode:= (att => mode_at,
              pos => (0, 0), m_md => in_out_md);
   end TC_Mode_In_Out;

   -- Declarations -------------------------------------------------------------------------------

   procedure TC_Decls
     (decls            : out Attribute;
      inner_decls, decl: in Attribute)
   is
      -- DECLS: DECLS DECL
   begin
      decls:= (att => ok_at, pos => inner_decls.pos);
   end TC_Decls;


   procedure TC_Decls (decls: out Attribute)
   is
      -- DECLS: -- Lambda
   begin
      decls:= (att => ok_at, pos => (0,0));
   end TC_Decls;


   procedure TC_Decl (decl: out Attribute; inner_decl: in Attribute)
   is
      --  DECL:
      --       CONST_DECL
      --    |  VAR_DECL
      --    |  TYPE_DECL
      --    |  PROC_DECL
   begin
      decl:= (att => ok_at, pos => inner_decl.pos);
   end TC_Decl;

   -- Constant Declaration ------------------------------------------------------------------------

   procedure TC_Const_Decl
     (const_decl     : out Attribute;
      idc, idt, value: in Attribute)
   is
      -- CONST_DECL: id colon constant_kw id colonequal VALUE

      td, cd: Description;
      error : Boolean:= False;
   begin

      -- Check if idt is integer underlying type
      td:= Get(st, idt.id_id);
      if td.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;
      if td.td.ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_type(idt.pos, idt.id_id);
      end if;

      -- Check if constant value and its type is consistent
      case value.v_idt is
         when Null_Name_Id =>
            if value.v_ut /= td.td.ut then
               successful_type_checking:= False;
               error_incompatible_value(idt.pos, idt.id_id);
            end if;
            if value.v_v < td.td.lb then
               successful_type_checking:= False;
               error_value_out_of_range_lower(idt.pos, idt.id_id);
            end if;
            if value.v_v > td.td.ub then
               successful_type_checking:= False;
               error_value_out_of_range_upper(idt.pos, idt.id_id);
            end if;
         when others =>
            if value.v_idt /= idt.id_id then
               successful_type_checking:= False;
               error_incorrect_value_type(idt.pos, idt.id_id, value.v_idt);
            end if;
      end case;

      -- Add constant description to symbol table
      cd:= (dt => const_d,
            ct => idt.id_id, cn => value.v_vid);
      Put(st, idc.id_id, cd, error);
      if error then
         successful_type_checking:= False;
         error_identifier_already_seen(idc.pos, idc.id_id);
      end if;

      const_decl:= (att => ok_at, pos => idc.pos);

   end TC_Const_Decl;


   procedure TC_Value (value: out Attribute; elem_value: in Attribute)
   is
      -- VALUE: ELEM_VALUE
   begin

      value:= (att => value_at,
               pos => elem_value.pos, v_idt => elem_value.ev_idt,
               v_ut => elem_value.ev_ut, v_v => elem_value.ev_v,
               v_vid => Null_Var_Id
              );

   end TC_Value;


   procedure TC_Value_Negative
     (value     : out Attribute;
      elem_value: in Attribute)
   is
      -- VALUE: substraction_op ELEM_VALUE
   begin

      -- Check if it's an integer
      if elem_value.ev_ut /= int_ut then
         successful_type_checking:= False;
         error_neg_on_non_integer_value(elem_value.pos);
      end if;

      -- Set output data
      TC_Value(value, elem_value);
      value.v_v := -value.v_v;

   end TC_Value_Negative;


   procedure TC_Elem_Value_Id (elem_value: out Attribute; id: in Attribute)
   is
      -- ELEM_VALUE: id

      d, td: Description;
   begin

      -- Check if it's constant
      d:= Get(st, id.id_id);
      if d.dt /= const_d then
         successful_type_checking:= False;
         error_expected_constant(id.pos, id.id_id);
      end if;

      -- Get type info and set data
      td:= Get(st, d.ct);
      elem_value:= (att => elem_value_at,
                    pos => id.pos, ev_idt => d.ct, ev_ut => vt(d.cn).ut, ev_v => vt(d.cn).v);

   end TC_Elem_Value_Id;


   procedure TC_Elem_Value_Literal
     (elem_value: out Attribute;
      literal   : in Attribute)
   is
      -- ELEM_VALUE: literal
   begin

      elem_value:= (att => elem_value_at, pos => literal.pos,
                    ev_idt => Null_Name_Id, ev_ut => literal.lit_ut, ev_v => literal.lit_v);

   end TC_Elem_Value_Literal;

   -- Variable Declaration ------------------------------------------------------------------------

   procedure TC_Var_Decl
     (var_decl         : out Attribute;
      id, cont_var_decl: in Attribute)
   is
      -- VAR_DECL: id CONT_VAR_DECL
   begin

      -- Register id as variable
      Register_Variable(id, cont_var_decl.cvd_idt);

      var_decl:= (att => ok_at, pos => id.pos);

   end TC_Var_Decl;


   procedure Register_Variable
     (id: in Attribute; idt: in Name_Id)
   is
      d, dt: Description;
      vid: Var_Id;
      err: Boolean;
   begin

      err:= False;

      dt:= Get(st => st, id => idt);
      vid:= New_Local_Var(vt => vt, pt => pt, pid => pds(pd), va => va,
                          occup => dt.td.occu);
      d:= (dt => var_d,
           vt => idt, vn => vid);
      Put(st, id.id_id, d, err);

      if err then
         successful_type_checking:= False;
         error_identifier_already_seen(id.pos, id.id_id);
      end if;

   end Register_Variable;


   procedure TC_Cont_Var_Decl
     (cont_var_decl          : out Attribute;
      id, inner_cont_var_decl: in Attribute)
   is
      -- CONT_VAR_DECL: comma id CONT_VAR_DECL

   begin

      -- Register id as variable
      Register_Variable(id, inner_cont_var_decl.cvd_idt);

      -- Propagate type to upper levels
      cont_var_decl:= inner_cont_var_decl;

   end TC_Cont_Var_Decl;


   procedure TC_Cont_Var_Decl
     (cont_var_decl: out Attribute;
      id           : in Attribute)
   is
      -- CONT_VAR_DECL: colon id semicolon

      d: Description;
   begin

      -- Check if id is a type
      d:= Get(st, id.id_id);
      if d.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(id.pos, id.id_id);
      end if;

      -- Propagate type to upper levels
      cont_var_decl:= (att => cont_var_decl_at,
                       pos => id.pos, cvd_idt => id.id_id);

   end TC_Cont_Var_Decl;

   -- Type declaration ---------------------------------------------------------------------------

   procedure TC_Type_Decl (type_decl: out Attribute; decl: in Attribute)
   is
      --  TYPE_DECL:
      --       SUBRANGE_DECL
      --    |  ARRAY_DECL
      --    |  RECORD_DECL
   begin
      type_decl:= (att => ok_at, pos => decl.pos);
   end TC_Type_Decl;

   -- Type::Subrange Declaration -----------------------------------------------------------------

   procedure TC_Subrange_Decl
     (subrange_decl                    : out Attribute;
      id, idt, lower_value, upper_value: in Attribute)
   is
      --  SUBRANGE_DECL:
      --          type_kw id is_kw new_kw id range_kw VALUE point point VALUE

      tdd: Type_Description;
      td, d: Description;
      error: Boolean:= False;
   begin

      td:= Get(st, idt.id_id);

      if td.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;
      if td.td.ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_type(idt.pos, idt.id_id);
      end if;

      if lower_value.v_idt = Null_Name_Id and lower_value.v_ut /= td.td.ut then
         successful_type_checking:= False;
         error_value_type_for_range_incompatible(lower_value.pos);
      end if;
      if lower_value.v_idt /= Null_Name_Id and lower_value.v_idt /= idt.id_id then
         successful_type_checking:= False;
         error_value_type_for_range_incompatible(lower_value.pos);
      end if;

      if upper_value.v_idt = Null_Name_Id and upper_value.v_ut /= td.td.ut then
         successful_type_checking:= False;
         error_value_type_for_range_incompatible(upper_value.pos);
      end if;
      if upper_value.v_idt /= Null_Name_Id and upper_value.v_idt /= idt.id_id then
         successful_type_checking:= False;
         error_value_type_for_range_incompatible(upper_value.pos);
      end if;

      if lower_value.v_v < td.td.lb or upper_value.v_v > td.td.ub then
         successful_type_checking:= False;
         error_range_values_not_correct(lower_value.pos);
      end if;
      if upper_value.v_v < td.td.lb or lower_value.v_v > td.td.ub then
         successful_type_checking:= False;
         error_range_values_not_correct(lower_value.pos);
      end if;
      if lower_value.v_v > upper_value.v_v then
         successful_type_checking:= False;
         error_range_values_not_correct(lower_value.pos);
      end if;

      -- Register the new subrange type
      case lower_value.v_ut is
         when bool_ut =>
            tdd:= (ut => bool_ut,
                   occu => td.td.occu, lb => lower_value.v_v, ub => upper_value.v_v);
         when int_ut =>
            tdd:= (ut => int_ut,
                   occu => td.td.occu, lb => lower_value.v_v, ub => upper_value.v_v);
         when char_ut =>
            tdd:= (ut => char_ut,
                   occu => td.td.occu, lb => lower_value.v_v, ub => upper_value.v_v);
         when others =>
            null;
      end case;

      d:= (dt => type_d, td => tdd);
      Put(st, id.id_id, d, error);
      if error then
         successful_type_checking:= False;
         error_identifier_already_seen(id.pos, id.id_id);
      end if;

      subrange_decl:= (att => ok_at, pos => id.pos);

   end TC_Subrange_Decl;

   -- Type::Record Declaration -------------------------------------------------------------------

   procedure TC_Record_Decl
     (record_decl    : out Attribute;
      pre_record_decl: in Attribute)
   is
      --  RECORD_DECL: PRE_RECORD_DECL end_kw record_kw semicolon

      idr: Name_Id renames pre_record_decl.prd_idr;
      occup: Occupancy renames pre_record_decl.prd_occup;
      d: Description;
   begin

      -- Update record description with total occupation
      d:= Get(st, idr); d.td.occu:= occup; Update(st, idr, d);
      record_decl:= (att => ok_at, pos => pre_record_decl.pos);
   end TC_Record_Decl;


   procedure TC_Pre_Record_Decl
     (pre_record_decl               : out Attribute;
      inner_pre_record_decl, id, idt: in Attribute)
   is
      -- PRE_RECORD_DECL: PRE_RECORD_DECL id colon id semicolon

      td, fd: Description;
      error: Boolean;
   begin

      -- Check if idt is a type
      td:= Get(st, idt.id_id);
      if td.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;

      -- Register field
      fd:= (dt => field_d,
            ft => idt.id_id, fo => Offset(inner_pre_record_decl.prd_occup));
      Put_Field(st, inner_pre_record_decl.prd_idr, id.id_id, fd, error);
      if error then
         successful_type_checking:= False;
         error_field_in_record_already_exists(id.pos, id.id_id, inner_pre_record_decl.prd_idr);
      end if;

      -- Propagate record id and current occupation to upper levels
      pre_record_decl:= (att => pre_record_decl_at,
                         pos => inner_pre_record_decl.pos,
                         prd_idr => inner_pre_record_decl.prd_idr,
                         prd_occup => inner_pre_record_decl.prd_occup + td.td.occu);

   end TC_Pre_Record_Decl;


   procedure TC_Pre_Record_Decl
     (pre_record_decl: out Attribute;
      id             : in Attribute)
   is
      -- PRE_RECORD_DECL: type_kw id is_kw record_kw

      td: Type_Description;
      tr: Description;
      error: Boolean:= False;
   begin

      -- Register the new record type
      td:= (ut => rec_ut, occu => 0);
      tr:= (dt => type_d, td => td);
      Put(st, id.id_id, tr, error);
      if error then
         successful_type_checking:= False;
         error_identifier_already_seen(id.pos, id.id_id);
      end if;

      -- Propagate record id to upper levels
      pre_record_decl:= (att => pre_record_decl_at,
                         pos => id.pos, prd_idr => id.id_id, prd_occup => 0);

   end TC_Pre_Record_Decl;

   -- Type::Array Declaration --------------------------------------------------------------------


   procedure TC_Pre_Array_Decl_Simple
     (pre_array_decl: out Attribute;
      id, idt       : in Attribute)
   is
      -- PRE_ARRAY_DECL: type_kw id is_kw array_kw opening_brackets id
      td: Type_Description;
      da: Description;
      dti, di: Description;
      err: Boolean:= False;
   begin
      -- Register array type
      td:= (ut => arr_ut,
            occu => 0, ct => Null_Name_Id, b => 0); -- Set later
      da:= (dt => type_d,
            td => td);
      Put(st    => st,
          id    => id.id_id,
          d     => da,
          error => err);
      if err then
         successful_type_checking:= False;
         error_identifier_already_seen(id.pos, id.id_id);
      end if;

      -- Register first index
      dti:= Get(st => st, id => idt.id_id);
      if dti.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;
      if dti.td.ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_type(idt.pos, idt.id_id);
      end if;
      di:= (dt => index_d,
            it => idt.id_id,
            n => dti.td.ub - dti.td.lb + 1);
      Put_Index(st  => st,
                aid => id.id_id,
                d   => di);

      -- Set output
      pre_array_decl:= (att => pre_array_decl_at,
                        pos => id.pos,
                        pad_n => Offset(di.n),
                        pad_ida =>  id.id_id,
                        pad_b => 0);
   end TC_Pre_Array_Decl_Simple;


   procedure TC_Pre_Array_Decl_Recursive
     (pre_array_decl           : out Attribute;
      inner_pre_array_decl, idt: in Attribute)
   is
      -- PRE_ARRAY_DECL: PRE_ARRAY_DECL comma id
      dti, di: Description;
   begin
      -- Register next index
      dti:= Get(st => st, id => idt.id_id);
      if dti.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(idt.pos, idt.id_id);
      end if;
      if dti.td.ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_type(idt.pos, idt.id_id);
      end if;
      di:= (dt => index_d,
            it => idt.id_id,
            n => dti.td.ub - dti.td.lb + 1);
      Put_Index(st  => st,
                aid => inner_pre_array_decl.pad_ida,
                d   => di);

      -- Set output
      pre_array_decl:= inner_pre_array_decl;
      pre_array_decl.pad_n:= pre_array_decl.pad_n * Offset(di.n);
   end TC_Pre_Array_Decl_Recursive;


   procedure TC_Array_Decl
     (array_decl         : out Attribute;
      pre_array_decl, idt: in Attribute)
   is
      -- ARRAY_DECL: PRE_ARRAY_DECL closing_brackets of_kw id semicolon
      dc, da: Description;
   begin
      dc:= Get(st, idt.id_id);
      if dc.dt /= type_d then
         successful_type_checking:= False;
         error_expected_type(pos => idt.pos, id => idt.id_id);
      end if;

      da:= Get(st, pre_array_decl.pad_ida);
      da.td.occu:= Occupancy(pre_array_decl.pad_n) * dc.td.occu;
      da.td.ct:= idt.id_id;
      Update(st => st,
             id => pre_array_decl.pad_ida,
             d  => da);

      array_decl:= (att => ok_at, pos => pre_array_decl.pos);
   end TC_Array_Decl;


   -- Statements ---------------------------------------------------------------------------------

   procedure TC_Statements
     (statements                 : out Attribute;
      inner_statements, statement: in Attribute)
   is
      -- STATEMENTS: STATEMENTS STATEMENT
   begin
      statements:= (att => ok_at, pos => inner_statements.pos);
   end TC_Statements;


   procedure TC_Statements (statements: out Attribute)
   is
      -- STATEMENTS: null_kw semicolon
   begin
      statements:= (att => ok_at, pos => (0, 0));
   end TC_Statements;


   procedure TC_Statement
     (statement      : out Attribute;
      inner_statement: in Attribute)
   is
      --  STATEMENT:
      --       COND_STATEMENT
      --    |  LOOP_STATEMENT
      --    |  ASSIG_STATEMENT
      --    |  CALL_STATEMENT
   begin
      statement:= (att => ok_at, pos => inner_statement.pos);
   end TC_Statement;


   procedure TC_Cond_Statement
     (cond_statement  : out Attribute;
      expr, M, statements: in Attribute)
   is
      --  COND_STATEMENT:
      --       if_kw EXPR M_EXPR then_kw
      --           STATEMENTS
      --       end_kw if_kw semicolon
   begin

      if expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_boolean_expression(expr.pos);
      end if;

      cond_statement:= (att => ok_at, pos => expr.pos);

   end TC_Cond_Statement;


   procedure TC_Cond_Statement
     (cond_statement                          : out Attribute;
      expr, M, statements, ME, else_statements: in Attribute)
   is
      --  COND_STATEMENT:
      --    if_kw EXPR M_EXPR then_kw
      --       STATEMENTS
      --    else_kw M_ELSE
      --       STATEMENTS
      --    end_kw if_kw semicolon
   begin

      if expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_boolean_expression(expr.pos);
      end if;

      cond_statement:= (att => ok_at, pos => expr.pos);

   end TC_Cond_Statement;


   procedure TC_Loop_Statement
     (loop_statement          : out Attribute;
      M1, expr, M2, statements: in Attribute)
   is
      --  LOOP_STATEMENT:
      --       while_kw M_EXPR EXPR loop_kw M_EXPR
      --          STATEMENTS
      --       end_kw loop_kw semicolon
   begin

      if expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_boolean_expression(expr.pos);
      end if;

      loop_statement:= (att => ok_at, pos => expr.pos);

   end TC_Loop_Statement;


   procedure TC_Assig_Statement
     (assig_statement: out Attribute;
      ref, expr      : in Attribute)
   is
      --  ASSIG_STATEMENT:
      --       REF colonequal EXPR semicolon
   begin

      if not ref.r_is_var then
         successful_type_checking:= False;
         error_expected_variable_name(ref.pos);
      end if;

      case expr.e_idt is
         when Null_Name_Id =>
            if ref.r_ut /= expr.e_ut then
               successful_type_checking:= False;
               error_expresion_and_ref_different_type(ref.pos, ref.r_idt);
            end if;
         when others =>
            if ref.r_idt /= expr.e_idt then
               successful_type_checking:= False;
               error_expresion_and_ref_different_type(ref.pos, ref.r_idt);
            end if;
      end case;

      if ref.r_ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_reference(ref.pos);
      end if;

      assig_statement:= (att => ok_at, pos => ref.pos);


   end TC_Assig_Statement;


   procedure TC_Call_Statement
     (call_statement: out Attribute;
      ref           : in Attribute)
   is
      --  CALL_STATEMENT:
      --       REF semicolon
   begin
      if ref.att /= call_at then
         successful_type_checking:= False;
         error_expected_procedure_call(ref.pos);
      end if;
      call_statement:= (att => ok_at, pos => ref.pos);
   end TC_Call_Statement;

   -- References ---------------------------------------------------------------------------------

   procedure TC_Ref_Simple (ref: out Attribute; id: in Attribute)
   is
      -- REF: id

      td, d: Description;
   begin

      d:= Get(st, id.id_id);
      case d.dt is

         when var_d =>
            td:= Get(st, d.vt);
            ref:= (att => ref_at,
                   pos => id.pos, r_idt => d.vt, r_ut => td.td.ut, r_is_var => True,
                   r_r => d.vn, r_const_shift => 0, r_relat_shift => Null_Var_Id
                  );

         when const_d =>
            td:= Get(st, d.ct);
            ref:= (att => ref_at,
                   pos => id.pos, r_idt => d.ct, r_ut => td.td.ut, r_is_var => False,
                   r_r => d.cn, r_const_shift => 0, r_relat_shift => Null_Var_Id
                  );

         when argc_d =>
            td:= Get(st, d.argct);
            ref:= (att => ref_at,
                   pos => id.pos, r_idt => d.argct, r_ut => td.td.ut, r_is_var => False,
                   r_r => d.argcv, r_const_shift => 0, r_relat_shift => Null_Var_Id
                  );

         when proc_d =>
            ref:= (att => call_at,
                   pos => id.pos, c_nid => id.id_id);

         when others =>
            ref:= (att => null_ref_at, pos => id.pos, nr_id => id.id_id);

      end case;

   end TC_Ref_Simple;


   procedure TC_Ref (ref: out Attribute; inner_ref, id: in Attribute)
   is
      -- REF: REF point id

      ftd, fd: Description;
   begin
      -- Check if ref is a record ut
      if inner_ref.r_ut /= rec_ut then
         successful_type_checking:= False;
         error_expected_record_identifier(inner_ref.pos);
      end if;

      -- Check if field exists
      fd:= Get_Field(st, inner_ref.r_idt, id.id_id);
      if fd.dt = null_d then
         successful_type_checking:= False;
         error_record_field_does_not_exists(id.pos);
      end if;

      -- Set output ref data
      ftd:= Get(st, fd.ft);
      ref:= (att => ref_at,
             pos => inner_ref.pos, r_idt => fd.ft, r_ut => ftd.td.ut, r_is_var => inner_ref.r_is_var,

             -- Default values -- Reassigned in code generation
             r_r => Null_Var_Id, r_const_shift => 0, r_relat_shift => Null_Var_Id
            );
   end TC_Ref;

   --//--

   procedure TC_Pre_ArrayOrCall_Simple
     (pre_arrayOrCall: in out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR
   begin

      if ref.att = call_at then
         TC_Pre_Call_Simple(pre_call => pre_arrayOrCall,
                            ref      => ref,
                            expr     => expr);
      else
         TC_Pre_Array_Simple(pre_array => pre_arrayOrCall,
                             ref       => ref,
                             expr      => expr);
      end if;

   end TC_Pre_ArrayOrCall_Simple;

   procedure TC_Pre_ArrayOrCall_Recursive
     (pre_arrayOrCall            : in out Attribute;
      inner_pre_arrayOrCall, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR

   begin

      if inner_pre_arrayOrCall.att = pre_array_at then
         TC_Pre_Array_Recursive(pre_array       => pre_arrayOrCall,
                                inner_pre_array => inner_pre_arrayOrCall,
                                expr            => expr);
      else
         TC_Pre_Call_Recursive(pre_call       => pre_arrayOrCall,
                               inner_pre_call => inner_pre_arrayOrCall,
                               expr           => expr);
      end if;

   end TC_Pre_ArrayOrCall_Recursive;

   procedure TC_Ref_Recursive
     (ref            : out Attribute;
      inner_pre_arrayOrCall: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets
   begin

      if inner_pre_arrayOrCall.att = pre_array_at then
         TC_Ref_Recursive_Array(ref             => ref,
                                inner_pre_array => inner_pre_arrayOrCall);
      else
         TC_Ref_Recursive_Call(ref            => ref,
                               inner_pre_call => inner_pre_arrayOrCall);
      end if;

   end TC_Ref_Recursive;


   --///// ARRAY

   procedure TC_Pre_Array_Simple
     (pre_array: out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR

      it: Index_Iterator;
      it_cg: Index_Iterator;
   begin

      -- Check array and index types
      if ref.r_ut /= arr_ut then
         successful_type_checking:= False;
         error_expected_array_identifier(ref.pos);
      end if;
      First(st, ref.r_idt, it);
      Check_If_Expression_Matches_Index_Type(it, expr);

      -- Set output data
      First(st, ref.r_idt, it_cg);
      pre_array:= (att => pre_array_at,
                   pos => ref.pos, pa_adt => ref.r_idt, pa_it => it, pa_is_var => ref.r_is_var,

                   pa_r => Null_Var_Id, pa_const_shift => Offset'Last,
                   pa_relat_shift => Null_Var_Id, pa_shift => Null_Var_Id, pa_it_cg => it_cg
                  );

   end TC_Pre_Array_Simple;

   procedure TC_Pre_Array_Recursive
     (pre_array            : out Attribute;
      inner_pre_array, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR

      inner_it: Index_Iterator:= inner_pre_array.pa_it;
      inner_adt: Name_Id renames inner_pre_array.pa_adt;
      inner_is_var: Boolean renames inner_pre_array.pa_is_var;
   begin

      -- Check next index type
      Next(st, inner_it);
      Check_If_Expression_Matches_Index_Type(inner_it, expr);

      -- Set output data
      pre_array:= inner_pre_array;
      pre_array.pa_it:= inner_it;

   end TC_Pre_Array_Recursive;

   procedure TC_Ref_Recursive_Array
     (ref            : out Attribute;
      inner_pre_array: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets

      inner_it    : Index_Iterator:= inner_pre_array.pa_it;
      inner_adt   : Name_Id renames inner_pre_array.pa_adt;
      inner_is_var: Boolean renames inner_pre_array.pa_is_var;
      actd        : Description; -- Array component type description
      ad          : Description;
   begin

      -- Check if there are more formal indexes than given.
      Next(st, inner_it);
      if Is_Valid(inner_it) then
         successful_type_checking:= False;
         error_missing_value_for_indexes(inner_pre_array.pos);
      end if;

      -- Set output data
      ad:= Get(st, inner_adt);
      actd:= Get(st, ad.td.ct);
      ref:= (att => ref_at, pos => inner_pre_array.pos,
             r_idt => ad.td.ct, r_ut => actd.td.ut, r_is_var => inner_is_var,

             -- Default values -- Reassigned in code generation
             r_r => Null_Var_Id, r_const_shift => 0, r_relat_shift => Null_Var_Id
            );
   end TC_Ref_Recursive_Array;

   procedure Check_If_Expression_Matches_Index_Type
     (it: in Index_Iterator;
      expr: in Attribute)
   is
      id, itd: Description; -- Index description, index type description
   begin

      if not Is_Valid(it) then
         successful_type_checking:= False;
         error_more_values_than_indexes(expr.pos);
      end if;

      Get(st, it, id);
      case expr.e_idt is
         when Null_Name_Id =>
            itd:= Get(st, id.it);
            if itd.td.ut /= expr.e_ut then
               successful_type_checking:= False;
               error_expression_and_index_different_ut(expr.pos);
            end if;
         when others =>
            if id.it /= expr.e_idt then
               successful_type_checking:= False;
               error_expression_and_index_different_type(expr.pos, id.it, expr.e_idt);
            end if;
      end case;

   end Check_If_Expression_Matches_Index_Type;

   --///// CALL

   procedure TC_Pre_Call_Simple
     (pre_call: in out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR

      it: Argument_Iterator;
      args: Args_Stack.Stack;
   begin

      -- Check params types
      First(st, ref.c_nid, it);
      Check_If_Expression_Matches_Param_Type(it, expr);

      -- Set output data
      Args_Stack.Empty(args);
      pre_call:= (att => pre_call_at,
                  pos => ref.pos,
                  cp_nid => ref.c_nid, cp_it => it, cp_args => args);

   end TC_Pre_Call_Simple;

   procedure TC_Pre_Call_Recursive
     (pre_call            : in out Attribute;
      inner_pre_call, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR

      inner_it: Argument_Iterator:= inner_pre_call.cp_it;
   begin

      -- Check next index type
      Next(st, inner_it);
      Check_If_Expression_Matches_Param_Type(inner_it, expr);

      -- Set output data
      pre_call:= inner_pre_call;
      pre_call.cp_it:= inner_it;
   end TC_Pre_Call_Recursive;

   procedure TC_Ref_Recursive_Call
     (ref            : out Attribute;
      inner_pre_call: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets

      inner_it    : Argument_Iterator:= inner_pre_call.cp_it;
   begin

      -- Check if there are more formal indexes than given.
      Next(st, inner_it);
      if Is_Valid(inner_it) then
         successful_type_checking:= False;
         error_missing_value_for_params(inner_pre_call.pos);
      end if;

      -- Set output data
      ref:= (att => call_at,
             pos => inner_pre_call.pos, c_nid => inner_pre_call.cp_nid);
   end TC_Ref_Recursive_Call;

   procedure Check_If_Expression_Matches_Param_Type
     (it: in Argument_Iterator;
      expr: in Attribute)
   is
      pid: Name_Id; -- Param Name_Id
      pd, ptd: Description; -- Param type description
   begin

      if not Is_Valid(it) then
         successful_type_checking:= False;
         error_more_values_than_params(expr.pos);
         return;
      end if;

      Get(st, it, pid, pd);
      case expr.e_idt is
         when Null_Name_Id =>

            if pd.argt /= Null_Name_Id then

               ptd:= Get(st, pd.argt);
               if ptd.td.ut /= expr.e_ut then
                  successful_type_checking:= False;
                  error_expression_and_param_different_type(expr.pos);
               end if;

            end if;

         when others =>

            if pd.argt /= expr.e_idt then
               successful_type_checking:= False;
               error_expression_and_param_different_type(expr.pos);
            end if;
      end case;

      if (pd.argmd = out_md or pd.argmd = in_out_md) and not expr.e_is_var then
         successful_type_checking:= False; error_out_arg_is_not_var(expr.pos, pid);
      end if;

   end Check_If_Expression_Matches_Param_Type;


   -- Expressions --------------------------------------------------------------------------------

   procedure TC_Expr_Logical
     (expr                 : out Attribute;
      left_expr, logical_op, M, right_expr: in Attribute)
   is
      --  EXPR:
      --       EXPR and_op M_EXPR EXPR
      --    |  EXPR or_op M_EXPR EXPR

      eidt: Name_Id;
   begin

      if left_expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_bool_expression_on_logical_op(left_expr.pos);
      end if;
      if right_expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_bool_expression_on_logical_op(right_expr.pos);
      end if;
      if left_expr.e_idt /= Null_Name_Id and right_expr.e_idt /= Null_Name_Id
        and left_expr.e_idt /= right_expr.e_idt then
         successful_type_checking:= False;
         error_expressions_have_different_types(left_expr.pos);
      end if;

      -- Set output data
      if left_expr.e_idt = Null_Name_Id
      then eidt:= right_expr.e_idt;
      else eidt:= left_expr.e_idt;
      end if;

      -- Set output data
      expr:= (att => expression_at,
              pos => left_expr.pos, e_idt => eidt, e_ut => bool_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );

   end TC_Expr_Logical;


   procedure TC_Expr_Logical (expr: out Attribute; inner_expr: in Attribute)
   is
      --  EXPR: not_op EXPR

   begin
      -- Check if inner expr is boolean
      if inner_expr.e_ut /= bool_ut then
         successful_type_checking:= False;
         error_expected_bool_expression_on_not_op(inner_expr.pos);
      end if;

      expr:= (att => expression_at, pos => inner_expr.pos,
              e_idt => inner_expr.e_idt, e_ut => inner_expr.e_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );

   end TC_Expr_Logical;


   procedure TC_Expr_Relational
     (expr                 : out Attribute;
      left_expr, relational_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR rel_op EXPR
   begin

      if left_expr.e_ut /= right_expr.e_ut then
         successful_type_checking:= False;
         error_expressions_have_different_types(left_expr.pos);
      end if;
      if left_expr.e_idt /= Null_Name_Id and right_expr.e_idt /= Null_Name_Id
        and left_expr.e_idt /= right_expr.e_idt then
         successful_type_checking:= False;
         error_expressions_have_different_types(left_expr.pos);
      end if;

      if left_expr.e_ut > int_ut then
         successful_type_checking:= False;
         error_expected_scalar_expressions(left_expr.pos);
      end if;
      if left_expr.e_ut = bool_ut then
         successful_type_checking:= False;
         error_bool_with_rel_op(left_expr.pos);
      end if;

      -- Set output data
      expr:= (att => expression_at,
              pos => left_expr.pos, e_idt => Null_Name_Id, e_ut => bool_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );

   end TC_Expr_Relational;


   procedure TC_Expr_Arithmetic
     (expr                 : out Attribute;
      left_expr, arith_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR addition_op EXPR
      --    |  EXPR substraction_op EXPR
      --    |  EXPR product_op EXPR
      --    |  EXPR division_op EXPR
      --    |  EXPR modulus_op EXPR

      eidt: Name_Id;
   begin

      if left_expr.e_ut /= int_ut then
         successful_type_checking:= False;
         error_expected_integer_expression_on_arith_op(left_expr.pos);
      end if;
      if right_expr.e_ut /= int_ut then
         successful_type_checking:= False;
         error_expected_integer_expression_on_arith_op(right_expr.pos);
      end if;
      if left_expr.e_idt /= Null_Name_Id and right_expr.e_idt /= Null_Name_Id
        and left_expr.e_idt /= right_expr.e_idt then
         successful_type_checking:= False;
         error_expressions_have_different_types(left_expr.pos);
      end if;

      -- Set output data
      if left_expr.e_idt = Null_Name_Id
      then eidt:= right_expr.e_idt;
      else eidt:= left_expr.e_idt;
      end if;

      expr:= (att => expression_at,
              pos => left_expr.pos, e_idt => eidt, e_ut => int_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC -- Set on Code Generation
             );

   end TC_Expr_Arithmetic;


   procedure TC_Expr_Neg (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: substraction_op EXPR %prec un_subs_op
   begin

      -- Check if inner expr is integer
      if inner_expr.e_ut /= int_ut then
         successful_type_checking:= False;
         error_neg_on_non_integer_value(inner_expr.pos);
      end if;

      expr:= (att => expression_at, pos => inner_expr.pos,
              e_idt => inner_expr.e_idt, e_ut => inner_expr.e_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );
   end TC_Expr_Neg;


   procedure TC_Ref (expr: out Attribute; ref: in Attribute)
   is
      -- EXPR: REF
   begin
      expr:= (att => expression_at,
              pos => ref.pos, e_idt => ref.r_idt, e_ut => ref.r_ut, e_is_var => ref.r_is_var,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );
   end TC_Ref;


   procedure TC_Literal (expr: out Attribute; literal: in Attribute)
   is
      -- EXPR: literal
   begin
      expr:= (att => expression_at, pos => literal.pos,
              e_idt => Null_Name_Id, e_ut => literal.lit_ut, e_is_var => False,
              e_r => Null_Var_Id, e_shift => Null_Var_Id,
              e_ptrue => Null_PC, e_pfalse => Null_PC
             );
   end TC_Literal;


   procedure TC_Enclosing (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: opening_brackets EXPR closing_brackets
   begin
      expr:= inner_expr;
   end TC_Enclosing;

end semantics.Type_Checking_Routines;

