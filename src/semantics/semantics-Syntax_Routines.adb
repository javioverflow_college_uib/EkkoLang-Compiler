with semantics.Type_Checking_Routines;   use semantics.Type_Checking_Routines;
with semantics.Code_Generation_Routines; use semantics.Code_Generation_Routines;
with semantics.Messages;                 use semantics.Messages;

package body semantics.Syntax_Routines is

   procedure SR_S (s: out Attribute; proc_decl: in Attribute)
   is
      -- S: PROC_DECL
   begin
      if proc_decl.att = null_at then s:= proc_decl; return; end if;

      TC_S(s, proc_decl);
      if successful_type_checking then
         CG_S(s, proc_decl);
      end if;
   end SR_S;


   procedure SR_Proc_Decl
     (proc_decl                           : out Attribute;
      proc_preamble, decls, statements, id: in Attribute)
   is
      --  PROC_DECL:
      --       action_kw PROC_PREAMBLE is_kw
      --           DECLS
      --       begin_kw
      --           STATEMENTS
      --       end_kw action_kw
   begin
      if proc_preamble.att = null_at then proc_decl := proc_preamble;  return; end if;
      if decls.att         = null_at then proc_decl := decls;          return; end if;
      if statements.att    = null_at then proc_decl := statements;     return; end if;

      TC_Proc_Decl(proc_decl, proc_preamble, decls, statements, id);
      if successful_type_checking then
         CG_Proc_Decl(proc_decl, proc_preamble, decls, statements, id);
      end if;
   end SR_Proc_Decl;


   procedure SR_Proc_Preamble_Simple
     (proc_preamble: out Attribute;
      id           : in Attribute)
   is
      -- PROC_PREAMBLE: id
   begin
      TC_Proc_Preamble_Simple(proc_preamble, id);
      if successful_type_checking then
         CG_Proc_Preamble_Simple(proc_preamble, id);
      end if;
   end SR_Proc_Preamble_Simple;


   procedure SR_Proc_Preamble_Complex
     (proc_preamble    : out Attribute;
      pre_proc_preamble: in Attribute)
   is
      -- PROC_PREAMBLE: PRE_PROC_PREAMBLE closing_brackets
   begin
      if pre_proc_preamble.att = null_at then proc_preamble:= pre_proc_preamble; return; end if;

      TC_Proc_Preamble_Complex(proc_preamble, pre_proc_preamble);
      if successful_type_checking then
         CG_Proc_Preamble_Complex(proc_preamble, pre_proc_preamble);
      end if;
   end SR_Proc_Preamble_Complex;


   procedure SR_Pre_Proc_Preamble_Simple
     (pre_proc_preamble: out Attribute;
      id, arg          : in Attribute)
   is
      -- PRE_PROC_PREAMBLE: id opening_brackets ARG
   begin
      if arg.att = null_at then pre_proc_preamble:= arg; return; end if;

      TC_Pre_Proc_Preamble_Simple(pre_proc_preamble, id, arg);
      if successful_type_checking then
         CG_Pre_Proc_Preamble_Simple(pre_proc_preamble, id, arg);
      end if;
   end SR_Pre_Proc_Preamble_Simple;


   procedure SR_Pre_Proc_Preamble_Recursive
     (pre_proc_preamble           : out Attribute;
      inner_pre_proc_preamble, arg: in Attribute)
   is
      -- PRE_PROC_PREAMBLE: PRE_PROC_PREAMBLE semicolon ARG
   begin
      if inner_pre_proc_preamble.att = null_at then
         pre_proc_preamble:= inner_pre_proc_preamble;
         return;
      end if;

      TC_Pre_Proc_Preamble_Recursive(pre_proc_preamble, inner_pre_proc_preamble, arg);
      if successful_type_checking then
         CG_Pre_Proc_Preamble_Recursive(pre_proc_preamble, inner_pre_proc_preamble, arg);
      end if;
   end SR_Pre_Proc_Preamble_Recursive;


   procedure SR_Arg (arg: out Attribute; ida, mode, idt: in Attribute)
   is
      -- ARG: id colon MODE id
   begin
      TC_Arg(arg, ida, mode, idt);
      if successful_type_checking then
         CG_Arg(arg, ida, mode, idt);
      end if;
   end SR_Arg;


   procedure SR_Mode_In (mode: out Attribute)
   is
      -- MODE: in_kw
   begin
      TC_Mode_In(mode);
      if successful_type_checking then
         CG_Mode_In(mode);
      end if;
   end SR_Mode_In;


   procedure SR_Mode_Out (mode: out Attribute)
   is
      -- MODE: out_kw
   begin
      TC_Mode_Out(mode);
      if successful_type_checking then
         CG_Mode_Out(mode);
      end if;
   end SR_Mode_Out;


   procedure SR_Mode_In_Out (mode: out Attribute)
   is
      -- MODE: in_kw out_kw
   begin
      TC_Mode_In_Out(mode);
      if successful_type_checking then
         CG_Mode_In_Out(mode);
      end if;
   end SR_Mode_In_Out;


   procedure SR_Decls
     (decls            : out Attribute;
      inner_decls, decl: in Attribute)
   is
      -- DECLS: DECLS DECL
   begin
      if inner_decls.att = null_at then decls:= inner_decls; return; end if;
      if decl.att        = null_at then decls:= decl;        return; end if;

      TC_Decls(decls, inner_decls, decl);
      if successful_type_checking then
         CG_Decls(decls, inner_decls, decl);
      end if;
   end SR_Decls;


   procedure SR_Decls (decls: out Attribute)
   is
      -- DECLS: -- Lambda
   begin
      TC_Decls(decls);
      if successful_type_checking then
         CG_Decls(decls);
      end if;
   end SR_Decls;


   procedure SR_Decl (decl: out Attribute; inner_decl: in Attribute)
   is
      --  DECL:
      --       CONST_DECL
      --    |  VAR_DECL
      --    |  TYPE_DECL
      --    |  PROC_DECL
   begin
      if inner_decl.att = null_at then decl:= inner_decl; return; end if;

      TC_Decl(decl, inner_decl);
      if successful_type_checking then
         CG_Decl(decl, inner_decl);
      end if;
   end SR_Decl;


   procedure SR_Const_Decl
     (const_decl     : out Attribute;
      idc, idt, value: in Attribute)
   is
      -- CONST_DECL: id colon constant_kw id colonequal VALUE
   begin
      TC_Const_Decl(const_decl, idc, idt, value);
      if successful_type_checking then
         CG_Const_Decl(const_decl, idc, idt, value);
      end if;
   end SR_Const_Decl;


   procedure SR_Value (value: out Attribute; elem_value: in Attribute)
   is
      -- VALUE: ELEM_VALUE
   begin
      TC_Value(value, elem_value);
      if successful_type_checking then
         CG_Value(value, elem_value);
      end if;
   end SR_Value;


   procedure SR_Value_Negative
     (value     : out Attribute;
      elem_value: in Attribute)
   is
      -- VALUE: substraction_op ELEM_VALUE
   begin
      TC_Value_Negative(value, elem_value);
      if successful_type_checking then
         CG_Value_Negative(value, elem_value);
      end if;
   end SR_Value_Negative;


   procedure SR_Elem_Value_Id (elem_value: out Attribute; id: in Attribute)
   is
      -- ELEM_VALUE: id
   begin
      TC_Elem_Value_Id(elem_value, id);
      if successful_type_checking then
         CG_Elem_Value_Id(elem_value, id);
      end if;
   end SR_Elem_Value_Id;


   procedure SR_Elem_Value_Literal
     (elem_value: out Attribute;
      literal   : in Attribute)
   is
      -- ELEM_VALUE: literal
   begin
      TC_Elem_Value_Literal(elem_value, literal);
      if successful_type_checking then
         CG_Elem_Value_Literal(elem_value, literal);
      end if;
   end SR_Elem_Value_Literal;


   procedure SR_Var_Decl
     (var_decl         : out Attribute;
      id, cont_var_decl: in Attribute)
   is
      -- VAR_DECL: id CONT_VAR_DECL
   begin
      if cont_var_decl.att = null_at then var_decl:= cont_var_decl; return; end if;

      TC_Var_Decl(var_decl, id, cont_var_decl);
      if successful_type_checking then
         CG_Var_Decl(var_decl, id, cont_var_decl);
      end if;
   end SR_Var_Decl;


   procedure SR_Cont_Var_Decl
     (cont_var_decl          : out Attribute;
      id, inner_cont_var_decl: in Attribute)
   is
      -- CONT_VAR_DECL: comma id CONT_VAR_DECL
   begin
      if inner_cont_var_decl.att = null_at then
         cont_var_decl:= inner_cont_var_decl;
         return;
      end if;

      TC_Cont_Var_Decl(cont_var_decl, id, inner_cont_var_decl);
      if successful_type_checking then
         CG_Cont_Var_Decl(cont_var_decl, id, inner_cont_var_decl);
      end if;
   end SR_Cont_Var_Decl;


   procedure SR_Cont_Var_Decl
     (cont_var_decl: out Attribute;
      id           : in Attribute)
   is
      -- CONT_VAR_DECL: colon id semicolon
   begin
      TC_Cont_Var_Decl(cont_var_decl, id);
      if successful_type_checking then
         CG_Cont_Var_Decl(cont_var_decl, id);
      end if;
   end SR_Cont_Var_Decl;


   procedure SR_Type_Decl (type_decl: out Attribute; decl: in Attribute)
   is
      --  TYPE_DECL:
      --       SUBRANGE_DECL
      --    |  ARRAY_DECL
      --    |  RECORD_DECL
   begin
      if decl.att = null_at then type_decl:= decl; return; end if;

      TC_Type_Decl(type_decl, decl);
      if successful_type_checking then
         CG_Type_Decl(type_decl, decl);
      end if;
   end SR_Type_Decl;


   procedure SR_Subrange_Decl
     (subrange_decl                    : out Attribute;
      id, idt, lower_value, upper_value: in Attribute)
   is
      --  SUBRANGE_DECL:
      --          type_kw id is_kw new_kw id range_kw VALUE point point VALUE
   begin
      TC_Subrange_Decl(subrange_decl, id, idt, lower_value, upper_value);
      if successful_type_checking then
         CG_Subrange_Decl(subrange_decl, id, idt, lower_value, upper_value);
      end if;
   end SR_Subrange_Decl;


   procedure SR_Record_Decl
     (record_decl    : out Attribute;
      pre_record_decl: in Attribute)
   is
      --  RECORD_DECL: PRE_RECORD_DECL end_kw record_kw semicolon
   begin
      if pre_record_decl.att = null_at then record_decl:= pre_record_decl; return; end if;

      TC_Record_Decl(record_decl, pre_record_decl);
      if successful_type_checking then
         CG_Record_Decl(record_decl, pre_record_decl);
      end if;
   end SR_Record_Decl;


   procedure SR_Pre_Record_Decl
     (pre_record_decl               : out Attribute;
      inner_pre_record_decl, id, idt: in Attribute)
   is
      -- PRE_RECORD_DECL: PRE_RECORD_DECL id colon id semicolon
   begin
      if pre_record_decl.att = null_at then
         pre_record_decl:= inner_pre_record_decl;
         return;
      end if;

      TC_Pre_Record_Decl(pre_record_decl, inner_pre_record_decl, id, idt);
      if successful_type_checking then
         CG_Pre_Record_Decl(pre_record_decl, inner_pre_record_decl, id, idt);
      end if;
   end SR_Pre_Record_Decl;


   procedure SR_Pre_Record_Decl
     (pre_record_decl: out Attribute;
      id             : in Attribute)
   is
      -- PRE_RECORD_DECL: type_kw id is_kw record_kw
   begin
      TC_Pre_Record_Decl(pre_record_decl, id);
      if successful_type_checking then
         CG_Pre_Record_Decl(pre_record_decl, id);
      end if;
   end SR_Pre_Record_Decl;


   procedure SR_Array_Decl
     (array_decl         : out Attribute;
      pre_array_decl, idt: in Attribute)
   is
      -- ARRAY_DECL: PRE_ARRAY_DECL closing_brackets of_kw id semicolon
   begin
      if pre_array_decl.att = null_at then array_decl:= pre_array_decl; return; end if;

      TC_Array_Decl(array_decl, pre_array_decl, idt);
      if successful_type_checking then
         CG_Array_Decl(array_decl, pre_array_decl, idt);
      end if;
   end SR_Array_Decl;


   procedure SR_Pre_Array_Decl_Recursive
     (pre_array_decl           : out Attribute;
      inner_pre_array_decl, idt: in Attribute)
   is
      -- PRE_ARRAY_DECL: PRE_ARRAY_DECL comma id
   begin
      if inner_pre_array_decl.att = null_at then
         pre_array_decl:= inner_pre_array_decl;
         return;
      end if;

      TC_Pre_Array_Decl_Recursive(pre_array_decl, inner_pre_array_decl, idt);
      if successful_type_checking then
         CG_Pre_Array_Decl_Recursive(pre_array_decl, inner_pre_array_decl, idt);
      end if;
   end SR_Pre_Array_Decl_Recursive;


   procedure SR_Pre_Array_Decl_Simple
     (pre_array_decl: out Attribute;
      id, idt       : in Attribute)
   is
      -- PRE_ARRAY_DECL: type_kw id is_kw array_kw opening_brackets id
   begin
      TC_Pre_Array_Decl_Simple(pre_array_decl, id, idt);
      if successful_type_checking then
         CG_Pre_Array_Decl_Simple(pre_array_decl, id, idt);
      end if;
   end SR_Pre_Array_Decl_Simple;


   procedure SR_Statements
     (statements                 : out Attribute;
      inner_statements, statement: in Attribute)
   is
      -- STATEMENTS: STATEMENTS STATEMENT
   begin
      if inner_statements.att = null_at then statements:= inner_statements; return; end if;
      if statement.att = null_at then statements:= statement; return; end if;

      TC_Statements(statements, inner_statements, statement);
      if successful_type_checking then
         CG_Statements(statements, inner_statements, statement);
      end if;
   end SR_Statements;


   procedure SR_Statements (statements: out Attribute)
   is
      -- STATEMENTS: null_kw semicolon
   begin
      TC_Statements(statements);
      if successful_type_checking then
         CG_Statements(statements);
      end if;
   end SR_Statements;


   procedure SR_Statement
     (statement      : out Attribute;
      inner_statement: in Attribute)
   is
      --  STATEMENT:
      --       COND_STATEMENT
      --    |  LOOP_STATEMENT
      --    |  ASSIG_STATEMENT
      --    |  CALL_STATEMENT
   begin
      if inner_statement.att = null_at then statement:= inner_statement; return; end if;

      TC_Statement(statement, inner_statement);
      if successful_type_checking then
         CG_Statement(statement, inner_statement);
      end if;
   end SR_Statement;


   procedure SR_Cond_Statement
     (cond_statement     : out Attribute;
      expr, M, statements: in Attribute)
   is
      --  COND_STATEMENT:
      --       if_kw EXPR M_EXPR then_kw
      --           STATEMENTS
      --       end_kw if_kw semicolon
   begin
      if expr.att       = null_at then cond_statement:= expr;       return; end if;
      if statements.att = null_at then cond_statement:= statements; return; end if;

      TC_Cond_Statement(cond_statement, expr, M, statements);
      if successful_type_checking then
         CG_Cond_Statement(cond_statement, expr, M, statements);
      end if;
   end SR_Cond_Statement;


   procedure SR_Cond_Statement
     (cond_statement                          : out Attribute;
      expr, M, statements, ME, else_statements: in Attribute)
   is
      --  COND_STATEMENT:
      --    if_kw EXPR M_EXPR then_kw
      --       STATEMENTS
      --    else_kw M_ELSE
      --       STATEMENTS
      --    end_kw if_kw semicolon
   begin
      if expr.att            = null_at then cond_statement:= expr;            return; end if;
      if statements.att      = null_at then cond_statement:= statements;      return; end if;
      if else_statements.att = null_at then cond_statement:= else_statements; return; end if;

      TC_Cond_Statement(cond_statement, expr, M, statements, ME, else_statements);
      if successful_type_checking then
         CG_Cond_Statement(cond_statement, expr, M, statements, ME, else_statements);
      end if;
   end SR_Cond_Statement;


   procedure SR_Loop_Statement
     (loop_statement          : out Attribute;
      M1, expr, M2, statements: in Attribute)
   is
      --  LOOP_STATEMENT:
      --       while_kw M_EXPR EXPR loop_kw M_EXPR
      --          STATEMENTS
      --       end_kw loop_kw semicolon
   begin
      if expr.att       = null_at then loop_statement:= expr;       return; end if;
      if statements.att = null_at then loop_statement:= statements; return; end if;

      TC_Loop_Statement(loop_statement, M1, expr, M2, statements);
      if successful_type_checking then
         CG_Loop_Statement(loop_statement, M1, expr, M2, statements);
      end if;
   end SR_Loop_Statement;


   procedure SR_Assig_Statement
     (assig_statement: out Attribute;
      ref, expr      : in Attribute)
   is
      --  ASSIG_STATEMENT:
      --       REF colonequal EXPR semicolon
   begin
      if ref.att = null_ref_at then
         error_variable_not_declared(ref.pos, ref.nr_id);
         assig_statement:= (att => null_at, pos => ref.pos);
         return;
      end if;
      if ref.att = null_at  then assig_statement:= ref;  return; end if;
      if expr.att = null_at then assig_statement:= expr; return; end if;

      TC_Assig_Statement(assig_statement, ref, expr);
      if successful_type_checking then
         CG_Assig_Statement(assig_statement, ref, expr);
      end if;
   end SR_Assig_Statement;


   procedure SR_Call_Statement
     (call_statement: out Attribute;
      ref           : in Attribute)
   is
      --  CALL_STATEMENT:
      --       REF semicolon
   begin
      if ref.att = null_ref_at then
         error_proc_not_declared(ref.pos, ref.nr_id);
         call_statement:= (att => null_at, pos => ref.pos);
         return;
      end if;

      TC_Call_Statement(call_statement, ref);
      if successful_type_checking then
         CG_Call_Statement(call_statement, ref);
      end if;
   end SR_Call_Statement;


   procedure SR_Ref_Simple (ref: out Attribute; id: in Attribute)
   is
      -- REF: id
   begin
      TC_Ref_Simple(ref, id);
      if successful_type_checking then
         CG_Ref_Simple(ref, id);
      end if;
   end SR_Ref_Simple;


   procedure SR_Ref (ref: out Attribute; inner_ref, id: in Attribute)
   is
      -- REF: REF point id
   begin
      if inner_ref.att = null_ref_at then
         error_record_var_not_declared(inner_ref.pos, inner_ref.nr_id);
         ref:= (att => null_at, pos => inner_ref.pos);
         return;
      end if;
      if inner_ref.att = null_at then ref:= inner_ref; return; end if;

      TC_Ref(ref, inner_ref, id);
      if successful_type_checking then
         CG_Ref(ref, inner_ref, id);
      end if;
   end SR_Ref;


   procedure SR_Ref_Recursive
     (ref            : out Attribute;
      inner_pre_arrayOrCall: in Attribute)
   is
      -- REF: PRE_ARRAY closing_brackets
   begin
      if inner_pre_arrayOrCall.att = null_at then ref:= inner_pre_arrayOrCall; return; end if;

      TC_Ref_Recursive(ref, inner_pre_arrayOrCall);
      if successful_type_checking then
         CG_Ref_Recursive(ref, inner_pre_arrayOrCall);
      end if;
   end SR_Ref_Recursive;


   procedure SR_Pre_ArrayOrCall_Simple
     (pre_arrayOrCall: in out Attribute;
      ref, expr: in Attribute)
   is
      -- PRE_ARRAY: REF opening_brackets EXPR
   begin

      if ref.att = null_ref_at then
         error_arrayOrCall_not_declared(ref.pos, ref.nr_id);
         pre_arrayOrCall:= (att => null_at, pos => ref.pos);
         return;
      end if;
      if ref.att = null_at  then pre_arrayOrCall:= ref;  return; end if;
      if expr.att = null_at then pre_arrayOrCall:= expr; return; end if;

      TC_Pre_ArrayOrCall_Simple(pre_arrayOrCall, ref, expr);
      if successful_type_checking then
         CG_Pre_ArrayOrCall_Simple(pre_arrayOrCall, ref, expr);
      end if;
   end SR_Pre_ArrayOrCall_Simple;


   procedure SR_Pre_ArrayOrCall_Recursive
     (pre_arrayOrCall            : out Attribute;
      inner_pre_arrayOrCall, expr: in Attribute)
   is
      --  PRE_ARRAY: PRE_ARRAY comma EXPR
   begin
      if expr.att = null_at then pre_arrayOrCall:= expr; return; end if;
      if inner_pre_arrayOrCall.att = null_at then pre_arrayOrCall:= expr; return; end if;

      TC_Pre_ArrayOrCall_Recursive(pre_arrayOrCall, inner_pre_arrayOrCall, expr);
      if successful_type_checking then
         CG_Pre_ArrayOrCall_Recursive(pre_arrayOrCall, inner_pre_arrayOrCall, expr);
      end if;
   end SR_Pre_ArrayOrCall_Recursive;


   procedure SR_Expr_Logical
     (expr                 : out Attribute;
      left_expr, logical_op, M, right_expr: in Attribute)
   is
      --  EXPR:
      --       EXPR and_op M_EXPR EXPR
      --    |  EXPR or_op M_EXPR EXPR
   begin
      if left_expr.att  = null_at then expr:= left_expr;  return; end if;
      if right_expr.att = null_at then expr:= right_expr; return; end if;

      TC_Expr_Logical(expr, left_expr, logical_op, M, right_expr);
      if successful_type_checking then
         CG_Expr_Logical(expr, left_expr, logical_op, M, right_expr);
      end if;
   end SR_Expr_Logical;


   procedure SR_Expr_Logical (expr: out Attribute; inner_expr: in Attribute)
   is
      --  EXPR: not_op EXPR
   begin
      if inner_expr.att = null_at then expr:= inner_expr; return; end if;

      TC_Expr_Logical(expr, inner_expr);
      if successful_type_checking then
         CG_Expr_Logical(expr, inner_expr);
      end if;
   end SR_Expr_Logical;


   procedure SR_Expr_Relational
     (expr                 : out Attribute;
      left_expr, relational_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR rel_op EXPR
   begin
      if left_expr.att  = null_at then expr:= left_expr;  return; end if;
      if right_expr.att = null_at then expr:= right_expr; return; end if;

      TC_Expr_Relational(expr, left_expr, relational_op, right_expr);
      if successful_type_checking then
         CG_Expr_Relational(expr, left_expr, relational_op, right_expr);
      end if;
   end SR_Expr_Relational;


   procedure SR_Expr_Arithmetic
     (expr                 : out Attribute;
      left_expr, arith_op, right_expr: in Attribute)
   is
      --  EXPR:
      --    |  EXPR addition_op EXPR
      --    |  EXPR substraction_op EXPR
      --    |  EXPR product_op EXPR
      --    |  EXPR division_op EXPR
      --    |  EXPR modulus_op EXPR
   begin
      if left_expr.att  = null_at then expr:= left_expr;  return; end if;
      if right_expr.att = null_at then expr:= right_expr; return; end if;

      TC_Expr_Arithmetic(expr, left_expr, arith_op, right_expr);
      if successful_type_checking then
         CG_Expr_Arithmetic(expr, left_expr, arith_op, right_expr);
      end if;
   end SR_Expr_Arithmetic;


   procedure SR_Expr_Neg (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: substraction_op EXPR %prec un_subs_op
   begin
      if inner_expr.att = null_at then expr:= inner_expr; return; end if;

      TC_Expr_Neg(expr, inner_expr);
      if successful_type_checking then
         CG_Expr_Neg(expr, inner_expr);
      end if;
   end SR_Expr_Neg;


   procedure SR_Ref (expr: out Attribute; ref: in Attribute)
   is
      -- EXPR: REF
   begin

      if ref.att = null_ref_at then
         error_name_not_declared(ref.pos, ref.nr_id);
         expr:= (att => null_at, pos => ref.pos);
         return;
      end if;
      if ref.att = null_at then expr:= ref; return; end if;

      TC_Ref(expr, ref);
      if successful_type_checking then
         CG_Ref(expr, ref);
      end if;
   end SR_Ref;


   procedure SR_Literal (expr: out Attribute; literal: in Attribute)
   is
      -- EXPR: literal
   begin
      TC_Literal(expr, literal);
      if successful_type_checking then
         CG_Literal(expr, literal);
      end if;
   end SR_Literal;


   procedure SR_Enclosing (expr: out Attribute; inner_expr: in Attribute)
   is
      -- EXPR: opening_brackets EXPR closing_brackets
   begin
      if inner_expr.att = null_at then expr:= inner_expr; return; end if;

      TC_Enclosing(expr, inner_expr);
      if successful_type_checking then
         CG_Enclosing(expr, inner_expr);
      end if;
   end SR_Enclosing;


end semantics.Syntax_Routines;
