generic
  type Item is private;
  Max: Natural:= 100;
package d_Stack is

  type Stack is private;

  procedure Empty(s: in out Stack);
  procedure Push(s: in out Stack; x: in Item);
  procedure Pop(s: in out Stack);
  function  Top(s: in Stack) return Item;
  function  Is_Empty(s: in Stack) return Boolean;

  Space_Overflow, Bad_Use: exception;

private

  Min: constant Natural:= 0;

  subtype Index is Natural range Min..Max;
  type Mem_Space is array (Index range 1..Index'Last) of Item;

  type Stack is
    record
      a: Mem_Space;
      n: Index;
    end record;

end d_Stack;
