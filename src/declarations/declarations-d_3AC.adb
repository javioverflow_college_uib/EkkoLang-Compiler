with Ada.Strings; use Ada.Strings;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;

package body declarations.d_3AC is

   function New_Constant
     (vt: in out Var_Table;
      va: in out Var_Id;
      ct: in out Constants_Vector_Package.Vector;
      la: in out Label;
      v: in Value;
      ut: in Underlying_Type)
      return Var_Id is

      cid: Var_Id;
      lab: Label;
      ci: Constant_Index;
   begin
      -- Check if constant already appeared
      ci:= (v => v, ut => ut);
      cid:= Get(ct, ci);

      -- If not, create constant and register it
      if cid = Null_Var_Id then
         lab:= New_Label(la);
         va:= va+1; cid:= va;
         vt(va):= (vt => VT_Const, v => v, ut => ut, lab => lab);

         Put(ct, ci, va);
      end if;

      return cid;
   end New_Constant;


   function New_Local_Var
     (vt: in out Var_Table;
      pt: in out Proc_Table;
      pid: in Proc_Id;
      va: in out Var_Id;
      occup: in Occupancy)
      return Var_Id is

      p: Proc renames pt(pid);
   begin
      p.occupvl:= p.occupvl + occup;
      va:= va+1;
      vt(va):= (vt => VT_Var, pid => pid, shift => Offset(-p.occupvl), occup => occup);
      return va;
   end New_Local_Var;

   function New_Arg_Var
     (vt: in out Var_Table;
      pt: in out Proc_Table;
      pid: in Proc_Id;
      va: in out Var_Id)
      return Var_Id is

      p: Proc renames pt(pid);
   begin
      p.pa:= p.pa+1;
      va:= va+1;
      vt(va):= (vt => VT_Var,
                pid => pid,
                shift => Offset(8 + p.pa * Natural(INT_OCCUPANCY)),
                occup => INT_OCCUPANCY);
      return va;
   end New_Arg_Var;

   function New_Ext_Proc
     (pt: in out Proc_Table;
      pa: in out Proc_Id;
      nid: in Name_Id;
      pam: in Natural)
      return Proc_Id is
   begin
      pa:= pa+1;
      pt(pa):= (pt => PT_External,
                pa => pam, nid => nid);
      return pa;
   end New_Ext_Proc;


   function New_Int_Proc
     (pt: in out Proc_Table;
      pa: in out Proc_Id;
      la: in out Label;
      pd: in Proc_Depth)
      return Proc_Id is

      lab: Label;
   begin
      pa:= pa+1;
      lab:= New_Label(la);
      pt(pa):= (pt => PT_Internal,
                pa => 0, depth => pd, occupvl => 0, lab => lab);
      return pa;
   end New_Int_Proc;

   function New_Label(la: in out Label) return Label is
   begin
      la:= la+1; return la;
   end New_Label;


   function Img (v: in Var_Id; vt: in Var_Table) return String is
   begin
      case vt(v).vt is
         when VT_Const =>
            return Trim(Integer(vt(v).v)'Img, Left);
         when VT_Var =>
            return "t" & Trim(Natural(v)'Img, Left);
      end case;
   end Img;

   function Img (p: in Proc_Id; pt: in Proc_Table; nt: in Name_Table) return String is
   begin
      case pt(p).pt is
         when PT_Internal =>
            return "p" & Trim(Natural(p)'Img, Left);
         when PT_External =>
            return Get(nt, pt(p).nid);
      end case;
   end Img;

   function Img (lab: in Label) return String is
   begin
      return "e" & trim(lab'Img, Left);
   end Img;

   function Img
     (i: in Instruction;
      vt: in Var_Table;
      pt: in Proc_Table;
      nt: in Name_Table)
      return String is
   begin
      case i.it is
         when IT_Copy =>
            return "    " & Img(i.x2, vt) & ":= " & Img(i.y2, vt);
         when IT_Query_Index =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & "[" & Img(i.z3, vt) & "]";
         when IT_Assign_Index =>
            return "    " & Img(i.x3, vt) & "[" & Img(i.y3, vt) & "]:= " & Img(i.z3, vt);
         when IT_Add =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " + " & Img(i.z3, vt);
         when IT_Sub =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " - " & Img(i.z3, vt);
         when IT_Mult =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " * " & Img(i.z3, vt);
         when IT_Div =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " / " & Img(i.z3, vt);
         when IT_Mod =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " mod " & Img(i.z3, vt);
         when IT_Neg =>
            return "    " & Img(i.x2, vt) & ":= -" & Img(i.y2, vt);
         when IT_And =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " and " & Img(i.z3, vt);
         when IT_Or =>
            return "    " & Img(i.x3, vt) & ":= " & Img(i.y3, vt) & " or " & Img(i.z3, vt);
         when IT_Not =>
            return "    " & Img(i.x2, vt) & ":= not " & Img(i.y2, vt);
         when IT_Skip =>
            return Img(i.e1) & ": skip";
         when IT_Goto =>
            return "    goto " & Img(i.e1);
         when IT_Eq =>
            return "    if " & Img(i.xe, vt) & "=" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_Uneq =>
            return "    if " & Img(i.xe, vt) & "/=" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_G =>
            return "    if " & Img(i.xe, vt) & ">" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_GE =>
            return "    if " & Img(i.xe, vt) & ">=" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_L =>
            return "    if " & Img(i.xe, vt) & "<" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_LE =>
            return "    if " & Img(i.xe, vt) & "<=" & Img(i.ye, vt) & " goto " & Img(i.e3);
         when IT_Call =>
            return "    call " & Img(i.pid, pt, nt);
         when IT_Pmb =>
            return "    pmb " & Img(i.pid, pt, nt);
         when IT_Rtn =>
            return "    rtn " & Img(i.pid, pt, nt);
         when IT_Params =>
            return "    params " & Img(i.x1, vt);
         when IT_Paramc =>
            return "    paramc " & Img(i.x2, vt) & "[" & Img(i.y2, vt) & "]";
      end case;
   end Img;

end declarations.d_3AC;
