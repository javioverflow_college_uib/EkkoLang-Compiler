with declarations.d_Description; use declarations.d_Description;
with declarations.d_Symbol_Table; use declarations.d_Symbol_Table;
with declarations.d_3AC; use declarations.d_3AC;
with semantics; use semantics;
with d_Stack;
with Ada.Direct_IO; use semantics.IO_3AC;

package declarations.d_Attribute is

   type Relational_Operator_Type is (Equal_Op, Unequal_Op, Less_Op, Less_Equal_Op,
                                     Greater_Op, Greater_Equal_Op);

   type Arithmetic_Operator_Type is (Add_Op, Sub_Op, Mult_Op, Div_Op, Mod_Op);

   type Logical_Operator_Type is (And_Op, Or_Op, Not_Op);

   type Position is
      record
         line  : Natural;
         column: Natural;
      end record;

   type Attribute_Type is
     (
      -- Lexic
      null_at, ok_at,
      id_at, lit_at, rel_op_at, arith_op_at, logical_op_at,

      -- Subprogram
      proc_at, proc_preamble_at, arg_at, mode_at,

      -- Constant
      const_decl_at, value_at, elem_value_at,
      -- Variable
      cont_var_decl_at, var_decl_at,

      -- Subrange declaration
      subrange_decl_at,

      -- Record declaration
      record_decl_at, pre_record_decl_at,

      -- Array declaration
      pre_array_decl_at,

      -- Reference
      ref_at, null_ref_at, pre_array_at, call_at, pre_call_at,

      -- Expression
      expression_at, mark_expr, mark_else
     );

   type Arg_Expression is
      record
         var: Var_Id;
         shift: Var_Id;
      end record;

   package Args_Stack is new d_Stack(Item => Arg_Expression);

   type Attribute(att: Attribute_Type:= null_at) is
      record
         pos: Position;
         case att is

            when null_at =>
               null;

            when ok_at =>
               null;

               -- Lexic Attributes
            when id_at =>
               id_id: Name_Id;

            when lit_at =>
               lit_ut: Underlying_Type;
               lit_v: Value;

            when rel_op_at =>
               rel_rot: Relational_Operator_Type;

            when arith_op_at =>
               arith_aot: Arithmetic_Operator_Type;

            when logical_op_at =>
               log_lot: Logical_Operator_Type;

               -- Subprogram
            when proc_at =>
               p_arga: Natural;

            when proc_preamble_at =>
               pp_pid: Name_Id;
               pp_arga: Natural;

            when arg_at =>
               a_ida: Name_Id;
               a_idt: Name_Id;
               a_md: Arg_Mode;
               a_av: Var_Id;

            when mode_at =>
               m_md: Arg_Mode;

               -- Constant Declaration
            when const_decl_at =>
               null;

            when value_at =>
               v_idt: Name_Id;
               v_ut: Underlying_Type;
               v_v: Value;
               v_vid: Var_Id;

            when elem_value_at =>
               ev_idt: Name_Id;
               ev_ut: Underlying_Type;
               ev_v: Value;

               -- Variable Declaration
            when var_decl_at =>
               null;

            when cont_var_decl_at =>
               cvd_idt: Name_Id;

               -- Subrange Declaration
            when subrange_decl_at =>
               null;

               -- Record Declaration
            when record_decl_at =>
               null;

            when pre_record_decl_at =>
               prd_idr: Name_Id;
               prd_occup: Occupancy;

               -- Array Declaration
            when pre_array_decl_at =>
               pad_n: Offset;
               pad_ida: Name_Id;
               pad_b: Offset;

               -- Reference
            when ref_at =>
               r_ut: Underlying_Type;
               r_is_var: Boolean;

               r_idt: Name_Id;

               r_r: Var_Id;
               r_const_shift: Offset;
               r_relat_shift: Var_Id;

            when null_ref_at =>
               nr_id: Name_Id;

            when pre_array_at =>
               pa_adt: Name_Id;
               pa_it: Index_Iterator;
               pa_is_var: Boolean;

               pa_r: Var_Id;
               pa_const_shift: Offset;
               pa_relat_shift: Var_Id;
               pa_shift: Var_Id;
               pa_it_cg: Index_Iterator;

            when call_at =>
               c_nid: Name_Id;

            when pre_call_at =>
               cp_nid: Name_Id;
               cp_it: Argument_Iterator;
               cp_args: Args_Stack.Stack;

               -- Expression Declaration
            when expression_at =>
               e_idt: Name_Id;
               e_ut: Underlying_Type;
               e_is_var: Boolean;

               e_r: Var_Id;
               e_shift: Var_Id;

               e_ptrue: Positive;
               e_pfalse: Positive;

            when mark_expr =>
               m_lab: Label;

            when mark_else =>
               me_lab: Label;
               me_labfi: Label;

         end case;
      end record;


end declarations.d_Attribute;
