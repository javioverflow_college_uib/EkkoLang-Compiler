with declarations.d_Description; use declarations.d_Description;
with declarations.d_Names_Table; use declarations.d_Names_Table;
with d_vector;

with Direct_IO;

package declarations.d_3AC is

   type Procedure_Type is (PT_External, PT_Internal);
   type Proc (pt: Procedure_Type:= PT_Internal) is
      record
         pa: Natural;               -- Params Amount
         case pt is
            when PT_Internal =>
               depth: Proc_Depth;
               occupvl: Occupancy;
               lab: Label;
            when PT_External =>
               nid: Name_Id;
         end case;
      end record;

   type Proc_Table is array(Proc_Id) of Proc;

   type Proc_Depth_Stack is array(Proc_Depth) of Proc_Id;

   type Var_Type is (VT_Var, VT_Const);
   type Var (vt: Var_Type:= VT_Var) is
      record
         case vt is
            when VT_Var =>
               pid: Proc_Id;
               shift: Offset;
               occup: Occupancy;
            when VT_Const =>
               v: Value;
               ut: Underlying_Type;
               lab: Label;
         end case;
      end record;

   type Var_Table is array(Var_Id) of Var;

   type Constant_Index is
      record
         v: Value;
         ut: Underlying_Type;
      end record;

   package Constants_Vector_Package is new d_Vector(key       => Constant_Index,
                                                   item      => Var_Id,
                                                   Max       => 1000,
                                                    Null_Item => Null_Var_Id);
   use Constants_Vector_Package;

   type Instruction_Type is (
                             IT_Copy, IT_Query_Index, IT_Assign_Index,
                             IT_Add, IT_Sub, IT_Mult, IT_Div, IT_Mod, IT_Neg,
                             IT_And, IT_Or, IT_Not,
                             IT_Skip, IT_Goto,
                             IT_Eq, IT_Uneq, IT_G, IT_GE, IT_L, IT_LE,
                             IT_Call, IT_Pmb, IT_Rtn, IT_Params, IT_Paramc
                            );

   type Instruction(it: Instruction_Type:= IT_Skip) is
      record
         case it is
               --    -----------------
               --   | it | x1 | - | - |
               --    -----------------
            when IT_Params =>
               x1: Var_Id;

               --    ------------------
               --   | it | x2 | y2 | - |
               --    ------------------
            when IT_Copy | IT_Neg | IT_Not | IT_Paramc =>
               x2: Var_Id;
               y2: Var_Id;

               --    -------------------
               --   | it | x3 | y3 | z3 |
               --    -------------------
            when IT_Query_Index | IT_Assign_Index |
                 IT_Add | IT_Sub | IT_Mult | IT_Div | IT_Mod |
                 IT_And | IT_Or =>
               x3: Var_Id;
               y3: Var_Id;
               z3: Var_Id;

               --    -----------------
               --   | it | e1 | - | - |
               --    -----------------
            when IT_Skip | IT_Goto =>
               e1: Label;
               pc1: Positive;

               --    ------------------
               --   | it | pid | - | - |
               --    ------------------
            when IT_Call | IT_Pmb | IT_Rtn =>
               pid: Proc_Id;

               --    -------------------
               --   | it | e3 | xe | ye |
               --    -------------------
            when IT_Eq | IT_Uneq | IT_G | IT_GE | IT_L | IT_LE =>
               e3: Label;
               pc3: Positive;
               xe: Var_Id;
               ye: Var_Id;
         end case;
      end record;

   function New_Constant(vt: in out Var_Table; va: in out Var_Id;
                         ct: in out Constants_Vector_Package.Vector;
                         la: in out Label; v: in Value; ut: in Underlying_Type) return Var_Id;

   function New_Local_Var(vt: in out Var_Table; pt: in out Proc_Table; pid: in Proc_Id;
                          va: in out Var_Id; occup: in Occupancy) return Var_Id;

   function New_Arg_Var(vt: in out Var_Table; pt: in out Proc_Table; pid: in Proc_Id;
                        va: in out Var_Id) return Var_Id;

   function New_Ext_Proc(pt: in out Proc_Table; pa: in out Proc_Id; nid: in Name_Id;
                         pam: in Natural) return Proc_Id;

   function New_Int_Proc(pt: in out Proc_Table; pa: in out Proc_Id; la: in out Label;
                         pd: in Proc_Depth) return Proc_Id;

   function New_Label(la: in out Label) return Label;

   function Img(i: in Instruction; vt: in Var_Table; pt: in Proc_Table;
               nt: Name_Table) return String;

end declarations.d_3AC;
