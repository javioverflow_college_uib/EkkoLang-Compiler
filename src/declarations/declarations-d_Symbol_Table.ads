with declarations.d_Description; use declarations.d_Description;

package declarations.d_Symbol_Table is

    type Symbol_Table is limited private;

    Not_A_Type, Not_A_Record, Not_An_Array, Not_A_Procedure, Bad_Use: exception;

    -- Common
    procedure Empty(st: out Symbol_Table);
    procedure Put(st   : in out Symbol_Table; id: in Name_Id; d: in Description;
                  error: in out Boolean);
    function Get(st: in Symbol_Table; id: in Name_Id) return Description;
    procedure Update(st: in out Symbol_Table; id: in Name_Id; d: in Description);

    procedure Enter_Block(st: in out Symbol_Table);
    procedure Exit_Block(st: in out Symbol_Table);

    -- Fields
    procedure Put_Field(st   : in out Symbol_Table; rid, fid: in Name_Id; d: in Description;
                        error: out Boolean);
    function Get_Field(st: in Symbol_Table; rid, fid: in Name_Id) return Description;

    -- Arrays
    procedure Put_Index(st: in out Symbol_Table; aid: in Name_Id; d: in Description);

    type Index_Iterator is private;

    procedure First(st: in Symbol_Table; aid: in Name_Id; it: out Index_Iterator);
    function Is_Valid(it: in Index_Iterator) return Boolean;
    procedure Get(st: in Symbol_Table; it: in Index_Iterator; d: out Description);
    procedure Next(st: in Symbol_Table; it: in out Index_Iterator);

    -- Arguments
    procedure Put_Argument(st   : in out Symbol_Table; pid, aid: in Name_Id; ad: in Description;
                           error: out Boolean);

    type Argument_Iterator is private;

    procedure First(st: in Symbol_Table; pid: in Name_Id; it: out Argument_Iterator);
    function Is_Valid(it: in Argument_Iterator) return Boolean;
    procedure Get(st: in Symbol_Table; it: in Argument_Iterator; aid: out Name_Id;
                  ad: out Description);
    procedure Next(st: in Symbol_Table; it: in out Argument_Iterator);

private

    Max_Depth: constant Integer:= 20;
    Max_Expansion_Cells: constant Natural:= Max_Names_Amount;

    type Expansion_Index is new Natural range 0..Max_Expansion_Cells;

    type Depth is new Integer range -1..Max_Depth;

    type Description_Cell is
        record
            dp: Depth;
            d: Description;
            e: Expansion_Index; -- First of args, fields or indexes list
        end record;

    type Expansion_Cell is
        record
            id: Name_Id;   -- Record / Array / Procedure parent or highest depth item
            dp: Depth;
            d: Description;
            n: Expansion_Index;
        end record;

    type Description_Table is array(Name_Id) of Description_Cell;
    type Expansion_Table is array(Expansion_Index) of Expansion_Cell;
    type Depth_Table is array(Depth) of Expansion_Index;

    type Symbol_Table is
        record
            dp: Depth;
            dt: Description_Table;
            et: Expansion_Table;
            dpt: Depth_Table;
        end record;

    type Index_Iterator is
        record
            ei: Expansion_Index;
        end record;

    type Argument_Iterator is
        record
            ei: Expansion_Index;
        end record;

end declarations.d_Symbol_Table;
