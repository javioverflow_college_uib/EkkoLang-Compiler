package declarations.d_Description is

   type Value is new Long_Integer;
   type Offset is new Integer;
   type Occupancy is new Natural;

   INT_OCCUPANCY: constant Occupancy:= 4;
   BOOL_OCCUPANCY: constant Occupancy:= 4;
   CHAR_OCCUPANCY: constant Occupancy:= 4;

   type Underlying_Type is (bool_ut, char_ut, int_ut, arr_ut, rec_ut, null_ut);

   type Type_Description (ut: Underlying_Type:= null_ut) is
      record
         occu: Occupancy; -- occupation
         case ut is
            when bool_ut | char_ut | int_ut =>
               ub, lb: Value; -- upper / lower bounds
            when arr_ut =>
               ct: Name_Id;   -- component type
               b: Offset;     -- static calculation at CG for indexes
            when rec_ut | null_ut =>
               null;
         end case;
      end record;


   type Description_Type is (null_d, var_d, const_d, proc_d, type_d, field_d,
                             index_d, arg_d, argc_d);

   type Arg_Mode is (in_md, out_md, in_out_md);

   type Description (dt: Description_Type:= null_d) is
      record
         case dt is
            when null_d =>
               null;

            when type_d =>
               td: Type_Description;

            when var_d =>
               vt: Name_Id; -- variable type
               vn: Var_Id; -- variable(VT_Var) number
            when const_d =>
               ct: Name_Id; -- constant type
               cn: Var_Id;  -- variable(VT_Const) number
            when argc_d =>
               argct: Name_Id;   -- argument type
               argcv: Var_Id;    -- argument variable number

            when proc_d =>
               pn: Proc_Id; -- procedure number

            when field_d =>
               ft: Name_Id; -- field type
               fo: Offset;  -- field offset
            when index_d =>
               it: Name_Id; -- index type
               n: Value;    -- upper_bound - lower_bound + 1
            when arg_d =>
               argt: Name_Id;   -- argument type
               argmd: Arg_Mode; -- argument mode

         end case;
      end record;


end declarations.d_Description;
