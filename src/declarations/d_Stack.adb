package body d_stack is

  procedure Empty(s: in out stack) is
  begin
    s.n:= Min;
  end Empty;


  procedure Push(s: in out stack; x: in Item) is
  begin
    if s.n = Max then raise Space_Overflow; end if;
    s.n:= s.n+1; s.a(s.n):= x;
  end Push;


  procedure Pop(s: in out stack) is
  begin
    if s.n = Min then raise Bad_Use; end if;
    s.n:= s.n-1;
  end Pop;


  function Top(s: in stack) return Item is
  begin
    if s.n = Min then raise Bad_Use; end if;
    return s.a(s.n);
  end Top;


  function Is_Empty(s: in stack) return Boolean is
  begin
    return s.n = Min;
  end Is_Empty;

end d_stack;
