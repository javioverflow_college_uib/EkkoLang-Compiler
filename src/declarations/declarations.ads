package declarations is

   Max_Names_Amount: constant Natural:= 9999;
   type Name_Id is new Natural range 0..Max_Names_Amount;
   Null_Name_Id: constant Name_Id:= 0;

   Max_Strings_Amount: constant Natural:= 9999;
   type String_Id is new Natural range 0..Max_Strings_Amount;
   Null_String_Id: constant String_Id:= 0;

   Average_Name_Length: constant Natural:= 16;
   Average_String_Length: constant Natural:= 64;

   -- Labels definition
   Max_Labels_Amount: Natural:= 2000;
   type Label is new Natural range 0..Max_Labels_Amount;
   Null_Label: constant Label:= 0;

   Max_Proc_Depth: Natural:= 100;
   type Proc_Depth is new Natural range 0..Max_Proc_Depth;
   Null_Proc_Depth: constant Proc_Depth:= 0;

   -- Procedure definition
   Max_Procs_Amount: Natural:= 2000;
   type Proc_Id is new Natural range 0..Max_Procs_Amount;
   Null_Proc_Id: constant Proc_Id:= 0;

   -- Variable definition
   Max_Vars_Amount: Natural:= 2000;
   type Var_Id is new Natural range 0..Max_Vars_Amount;
   Null_Var_Id: constant Var_Id:= 0;

end declarations;
