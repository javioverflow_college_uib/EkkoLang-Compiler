generic
   type key is private;
   type item is private;
   Max: Natural:= 1000;
   Null_Item: item;
package d_vector is

   type Vector is limited private;

   Space_Overflow: Exception;

   procedure Empty(v: out Vector);
   procedure Put  (v: in out Vector; k: in key; x: in item);
   function  Get  (v: in Vector; k: in key) return item;

   type Iterator is private;

   procedure First   (v: in Vector; it: out Iterator);
   function  Is_Valid(v: in Vector; it: in Iterator) return Boolean;
   procedure Next    (v: in Vector; it: in out Iterator);
   procedure Get     (v: in Vector; it: in Iterator; k: out key; x: out item);

private

   type Index is new integer range 0..Max;

   type Cell is
      record
         k: key;
         x: item;
      end record;

   type Mem_Space is array(Index range 1..Index'Last) of Cell;

   type Vector is
      record
         m: Mem_Space;
         n: Index;
      end record;

   type Iterator is
      record
         i: Index;
      end record;

end d_vector;
