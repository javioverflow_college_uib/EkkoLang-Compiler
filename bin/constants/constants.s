.section .bss
.comm DISP, 100
.section .text
.global _e1

.section .data
  ec1: .long  -1
  ec2: .long   0
  ec3: .long   90
  ec4: .ascii  "j"
  ec5: .asciz  "HAHA"

.section .text
_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $8, %esp

  movl $90, %eax
  movl %eax, -4(%ebp)

  movl $106, %eax
  movl %eax, -8(%ebp)

  movl $ec5, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  leal -4(%ebp), %eax
  pushl %eax

  call _geti
  addl $4, %esp

  leal -4(%ebp), %eax
  pushl %eax

  call _puti
  addl $4, %esp

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

