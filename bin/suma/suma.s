.section .bss
.comm DISP, 100
.section .text
.global _e1

_e2:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $36, %esp

  movl $0, %eax
  movl 16(%ebp), %edi
  movl %eax, (%edi)

  movl $3, %eax
  movl %eax, -8(%ebp)

  movl $5, %eax
  movl %eax, -4(%ebp)

_e3:

  movl -8(%ebp), %eax
  movl -4(%ebp), %ebx
  cmpl %ebx, %eax
  jg _e14
  jmp _e4
_e14:

  jmp _e5

_e4:

  movl -8(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -12(%ebp)

  movl -12(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -16(%ebp)

  movl -16(%ebp), %eax
  movl 12(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -20(%ebp)

  movl 16(%ebp), %esi
  movl (%esi), %eax
  movl -20(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -24(%ebp)

  movl -24(%ebp), %eax
  movl 16(%ebp), %edi
  movl %eax, (%edi)

  movl -8(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -28(%ebp)

  movl -28(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -32(%ebp)

  movl 12(%ebp), %eax
  movl -32(%ebp), %ebx
  addl %ebx, %eax
  pushl %eax

  call _puti
  addl $4, %esp

  call _new_line
  addl $0, %esp

  movl -8(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -36(%ebp)

  movl -36(%ebp), %eax
  movl %eax, -8(%ebp)

  jmp _e3

_e5:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e6:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $20, %esp

  movl $1, %eax
  movl %eax, -4(%ebp)

  movl $3, %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -8(%ebp)

  movl -8(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -12(%ebp)

  movl -12(%ebp), %eax
  movl 12(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -16(%ebp)

  movl -4(%ebp), %eax
  movl -16(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -20(%ebp)

  movl -20(%ebp), %eax
  movl %eax, -4(%ebp)

  leal -4(%ebp), %eax
  pushl %eax

  call _puti
  addl $4, %esp

  call _new_line
  addl $0, %esp

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e7:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $8, %esp

  movl $0, %eax
  movl %eax, -4(%ebp)

_e8:

  movl -4(%ebp), %eax
  movl $3, %ebx
  cmpl %ebx, %eax
  jge _e15
  jmp _e9
_e15:

  jmp _e10

_e9:

  leal -4(%ebp), %eax
  pushl %eax

  call _puti
  addl $4, %esp

  movl -4(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -8(%ebp)

  movl -8(%ebp), %eax
  movl %eax, -4(%ebp)

  jmp _e8

_e10:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e11:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $8, %esp

  movl 16(%ebp), %esi
  movl (%esi), %eax
  movl $0, %ebx
  cmpl %ebx, %eax
  jle _e16
  jmp _e12
_e16:

  jmp _e13

_e12:

  movl 12(%ebp), %esi
  movl (%esi), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -4(%ebp)

  movl -4(%ebp), %eax
  movl 12(%ebp), %edi
  movl %eax, (%edi)

  movl 16(%ebp), %esi
  movl (%esi), %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -8(%ebp)

  movl -8(%ebp), %eax
  movl 16(%ebp), %edi
  movl %eax, (%edi)

  movl 16(%ebp), %eax
  pushl %eax

  movl 12(%ebp), %eax
  pushl %eax

  call _e11
  addl $8, %esp

_e13:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $64, %esp

  movl $12, %eax
  movl $3, %ebx
  leal -40(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $16, %eax
  movl $5, %ebx
  leal -40(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $3, %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -44(%ebp)

  movl -44(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -48(%ebp)

  movl -48(%ebp), %eax
  movl $2, %ebx
  leal -40(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $4, %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -52(%ebp)

  movl -52(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -56(%ebp)

  movl -56(%ebp), %eax
  movl $4, %ebx
  leal -40(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $5, %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -60(%ebp)

  movl -60(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -64(%ebp)

  movl -64(%ebp), %eax
  movl $5, %ebx
  leal -40(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

