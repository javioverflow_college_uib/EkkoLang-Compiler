
rem ====================================
rem generacio del fitxer objecte stdio.o
rem   -c indica que nom�s compili
rem ====================================

rem gcc -c U:\stdio.s -o U:\stdio.o

rem =========================================
rem generacio del fitxer objecte test.o
rem   -c indica que nom�s compili
rem =========================================

gcc -c U:\%1\%1.s -o U:\%1\%1.o


rem ==================================================================
rem muntatge de test.o
rem    -o xxx especifica el nom que desitgem per al fitxer executable.
rem    -e xxx especifica l'etiqueta on es vol que comenci l'execuci�
rem ==================================================================

ld -o U:\%1\%1.exe -e _e1 U:\%1\%1.o  U:\stdio.o C:\GNAT\2016\lib\gcc\i686-pc-mingw32\4.9.4\*.a

call U:\%1\%1.exe
