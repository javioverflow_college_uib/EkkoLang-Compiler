.section .bss
.comm DISP, 100
.section .text
.global _e1

_e2:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $4, %esp

  movl 12(%ebp), %esi
  movl (%esi), %eax  movl 16(%ebp), %esi
  movl (%esi), %ebx  addl  %ebx, %eax
  movl %eax, -4(%ebp)
  movl -4(%ebp), %eax  movl 20(%ebp), %edi
  movl %eax, (%edi)
  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $8, %esp

  movl $8, %eax  movl %eax, -4(%ebp)
  leal -8(%ebp), %eax  pushl %eax

  leal -4(%ebp), %eax  pushl %eax

.section .data
  ec_e3: .long   3
.section .text
  movl $ec_e3, %eax  pushl %eax

  call _e2
  addl $12, %esp

  leal -8(%ebp), %eax  pushl %eax

  call _puti
  addl $4, %esp

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

