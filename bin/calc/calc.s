.section .bss
.comm DISP, 100
.section .text
.global _e1

.section .data
  ec1: .long  -1
  ec2: .long   0
  ec3: .asciz  "Parametre in tipus bool llegit correctament"
  ec4: .long   1
  ec5: .long   2
  ec6: .long   8
  ec7: .long  -3
  ec8: .long   4
  ec9: .long   288
  ec10: .asciz  "Paremetre out modificat correctament"

.section .text
_e2:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $4, %esp

  movl 12(%ebp), %esi
  movl (%esi), %eax
  movl $-1, %ebx
  cmpl %ebx, %eax
  jne _e32
  jmp _e3
_e32:

  jmp _e4

_e3:

  movl $ec3, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  call _new_line
  addl $0, %esp

_e4:

  jmp _e7

_e5:

  movl $-1, %eax
  movl %eax, -4(%ebp)

  jmp _e6

_e7:

  movl $0, %eax
  movl %eax, -4(%ebp)

_e6:

  movl -4(%ebp), %eax
  movl 16(%ebp), %edi
  movl %eax, (%edi)

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $136, %esp

  movl $8, %eax
  movl $8, %ebx
  cmpl %ebx, %eax
  jne _e33
  jmp _e8
_e33:

  jmp _e10

_e8:

  movl $-1, %eax
  movl %eax, -28(%ebp)

  jmp _e9

_e10:

  movl $0, %eax
  movl %eax, -28(%ebp)

_e9:

  movl $8, %eax
  movl $8, %ebx
  cmpl %ebx, %eax
  jne _e34
  jmp _e11
_e34:

  jmp _e13

_e11:

  movl $-1, %eax
  movl %eax, -32(%ebp)

  jmp _e12

_e13:

  movl $0, %eax
  movl %eax, -32(%ebp)

_e12:

  movl -28(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -36(%ebp)

  movl -36(%ebp), %eax
  movl -32(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -40(%ebp)

  movl -40(%ebp), %eax
  movl $-3, %ebx
  subl %ebx, %eax
  movl %eax, -44(%ebp)

  movl -44(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -48(%ebp)

  movl -48(%ebp), %eax
  movl $288, %ebx
  leal -16(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  jmp _e14

_e14:

  movl $-1, %eax
  movl %eax, -52(%ebp)

  jmp _e15

_e16:

  movl $0, %eax
  movl %eax, -52(%ebp)

_e15:

  jmp _e17

_e17:

  movl $-1, %eax
  movl %eax, -56(%ebp)

  jmp _e18

_e19:

  movl $0, %eax
  movl %eax, -56(%ebp)

_e18:

  movl -52(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -60(%ebp)

  movl -60(%ebp), %eax
  movl -56(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -64(%ebp)

  movl -64(%ebp), %eax
  movl $-3, %ebx
  subl %ebx, %eax
  movl %eax, -68(%ebp)

  movl -68(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -72(%ebp)

  leal -16(%ebp), %eax
  movl -72(%ebp), %ebx
  addl %ebx, %eax
  pushl %eax

  call _puti
  addl $4, %esp

  call _new_line
  addl $0, %esp

  movl $1, %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -76(%ebp)

  movl -76(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -80(%ebp)

  jmp _e20

_e20:

  movl $-1, %eax
  movl %eax, -84(%ebp)

  jmp _e21

_e22:

  movl $0, %eax
  movl %eax, -84(%ebp)

_e21:

  movl -80(%ebp), %eax
  movl -84(%ebp), %ebx
  leal -24(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $2, %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -88(%ebp)

  movl -88(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -92(%ebp)

  jmp _e23

_e23:

  movl $-1, %eax
  movl %eax, -96(%ebp)

  jmp _e24

_e25:

  movl $0, %eax
  movl %eax, -96(%ebp)

_e24:

  movl -92(%ebp), %eax
  movl -96(%ebp), %ebx
  leal -24(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $1, %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -100(%ebp)

  movl -100(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -104(%ebp)

  movl -104(%ebp), %eax
  leal -24(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -108(%ebp)

  movl -108(%ebp), %eax
  movl $-1, %ebx
  cmpl %ebx, %eax
  jne _e35
  jmp _e26
_e35:

  jmp _e28

_e26:

  movl $-1, %eax
  movl %eax, -112(%ebp)

  jmp _e27

_e28:

  movl $0, %eax
  movl %eax, -112(%ebp)

_e27:

  movl $2, %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -116(%ebp)

  movl -116(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -120(%ebp)

  movl -120(%ebp), %eax
  leal -24(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -124(%ebp)

  movl -124(%ebp), %eax
  movl $-1, %ebx
  cmpl %ebx, %eax
  jne _e36
  jmp _e29
_e36:

  jmp _e29

_e29:

  leal -24(%ebp), %eax
  movl -120(%ebp), %ebx
  addl %ebx, %eax
  pushl %eax

  leal -112(%ebp), %eax
  pushl %eax

  call _e2
  addl $8, %esp

  movl $2, %eax
  movl $1, %ebx
  subl %ebx, %eax
  movl %eax, -128(%ebp)

  movl -128(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -132(%ebp)

  movl -132(%ebp), %eax
  leal -24(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -136(%ebp)

  movl -136(%ebp), %eax
  movl $-1, %ebx
  cmpl %ebx, %eax
  jne _e37
  jmp _e31
_e37:

  jmp _e30

_e30:

  movl $ec10, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  call _new_line
  addl $0, %esp

_e31:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

