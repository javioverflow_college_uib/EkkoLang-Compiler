.section .bss
.comm DISP, 100
.section .text
.global _e1

_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $0, %esp

.section .data
  ec_e2: .long   9
.section .text
  movl $ec_e2, %eax
  pushl %eax

  call _puti
  addl $4, %esp

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

