.section .bss
   .comm DISP, 40
.section .text
   .global _e1
_e2:
   movl  $DISP, %eax
   addl  $8, %eax
   pushl (%eax)
   pushl %ebp
   movl  %esp, %ebp
   movl  %ebp, (%eax)
   subl  $220, %esp
   movl   12(%ebp), %eax
   movl  $100, %ebx
   addl  %ebx, %eax
   movl  $0, %ebx
   movl  %ebx, (%eax)
   movl  $DISP, %eax
   movl   4(%eax), %eax
   leal  -4(%eax), %eax
   pushl %eax
   call  _getc
   addl  $4, %esp
_e5:
   movl  $DISP, %esi
   movl   4(%esi), %esi
   movl  -4(%esi), %eax
   movl  $32, %ebx
   cmpl  %ebx, %eax
   je   _e31
   jmp  _e8
_e31:
   jmp  _e7
_e8:
   movl  $DISP, %esi
   movl   4(%esi), %esi
   movl  -4(%esi), %eax
   movl  $59, %ebx
   cmpl  %ebx, %eax
   je   _e32
   jmp  _e6
_e32:
   jmp  _e7
_e6:
   movl   12(%ebp), %eax
   movl  $100, %ebx
   addl  %ebx, %eax
   movl  (%eax), %eax
   movl  %eax, -104(%ebp)
   movl  -104(%ebp), %eax
   movl  $0, %ebx
   subl  %ebx, %eax
   movl  %eax, -108(%ebp)
   movl  -108(%ebp), %eax
   movl  $4, %ebx
   imul  %ebx, %eax
   movl  %eax, -112(%ebp)
   movl   12(%ebp), %eax
   movl  -112(%ebp), %ebx
   addl  %ebx, %eax
   movl  $DISP, %esi
   movl   4(%esi), %esi
   movl  -4(%esi), %ebx
   movl  %ebx, (%eax)
   movl   12(%ebp), %eax
   movl  $100, %ebx
   addl  %ebx, %eax
   movl  (%eax), %eax
   movl  %eax, -216(%ebp)
   movl  -216(%ebp), %eax
   movl  $1, %ebx
   addl  %ebx, %eax
   movl  %eax, -220(%ebp)
   movl   12(%ebp), %eax
   movl  $100, %ebx
   addl  %ebx, %eax
   movl  -220(%ebp), %ebx
   movl  %ebx, (%eax)
   movl  $DISP, %eax
   movl   4(%eax), %eax
   leal  -4(%eax), %eax
   pushl %eax
   call  _getc
   addl  $4, %esp
   jmp  _e5
_e7:
   movl  %ebp, %esp
   popl  %ebp
   movl  $DISP, %eax
   addl  $8, %eax
   popl  (%eax)
   ret
_e1:
   movl  $DISP, %eax
   addl  $4, %eax
   pushl (%eax)
   pushl %ebp
   movl  %esp, %ebp
   movl  %ebp, (%eax)
   subl  $340, %esp
   movl  $0, %eax
   movl  %eax, -112(%ebp)
   movl  $32, %eax
   movl  %eax, -4(%ebp)
.section .data
   _c20: .asciz "Introduiu una frase acabada en ';': "
.section .text
   movl  $_c20, %eax
   pushl %eax
   call  _puts
   addl  $4, %esp
_e17:
   movl  -4(%ebp), %eax
   movl  $59, %ebx
   cmpl  %ebx, %eax
   je   _e33
   jmp  _e18
_e33:
   jmp  _e19
_e18:
   leal  -108(%ebp), %eax
   pushl %eax
   call  _e2
   addl  $4, %esp
   movl  $0, %eax
   movl  %eax, -116(%ebp)
_e22:
   leal  -108(%ebp), %eax
   movl  $100, %ebx
   addl  %ebx, %eax
   movl  (%eax), %eax
   movl  %eax, -220(%ebp)
   movl  -116(%ebp), %eax
   movl  -220(%ebp), %ebx
   cmpl  %ebx, %eax
   jge  _e34
   jmp  _e23
_e34:
   jmp  _e24
_e23:
   movl  -116(%ebp), %eax
   movl  $0, %ebx
   subl  %ebx, %eax
   movl  %eax, -224(%ebp)
   movl  -224(%ebp), %eax
   movl  $4, %ebx
   imul  %ebx, %eax
   movl  %eax, -228(%ebp)
   leal  -108(%ebp), %eax
   movl  -228(%ebp), %ebx
   addl  %ebx, %eax
   movl  (%eax), %eax
   movl  %eax, -332(%ebp)
   movl  -332(%ebp), %eax
   movl  $97, %ebx
   cmpl  %ebx, %eax
   jne  _e35
   jmp  _e25
_e35:
   jmp  _e26
_e25:
   movl  -112(%ebp), %eax
   movl  $1, %ebx
   addl  %ebx, %eax
   movl  %eax, -336(%ebp)
   movl  -336(%ebp), %eax
   movl  %eax, -112(%ebp)
_e26:
   movl  -116(%ebp), %eax
   movl  $1, %ebx
   addl  %ebx, %eax
   movl  %eax, -340(%ebp)
   movl  -340(%ebp), %eax
   movl  %eax, -116(%ebp)
   jmp  _e22
_e24:
   jmp  _e17
_e19:
.section .data
   _c28: .asciz "El nombre de As a la frase es: "
.section .text
   movl  $_c28, %eax
   pushl %eax
   call  _puts
   addl  $4, %esp
   leal  -112(%ebp), %eax
   pushl %eax
   call  _puti
   addl  $4, %esp
   call  _new_line
   addl  $0, %esp
.section .data
   _c29: .asciz "Final del programa."
.section .text
   movl  $_c29, %eax
   pushl %eax
   call  _puts
   addl  $4, %esp
   call  _new_line
   addl  $0, %esp
   movl  %ebp, %esp
   popl  %ebp
   movl  $DISP, %eax
   addl  $4, %eax
   popl  (%eax)
   ret
