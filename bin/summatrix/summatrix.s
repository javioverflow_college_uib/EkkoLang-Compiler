.section .bss
.comm DISP, 100
.section .text
.global _e1

.section .data
  ec1: .long  -1
  ec2: .long   0
  ec3: .long   1
  ec4: .long   2
  ec5: .asciz  "Valor resultat matriu: "
  ec6: .long   3
  ec7: .long   4
  ec8: .ascii  " "
  ec9: .asciz  "Valor primera matriu: "
  ec10: .asciz  "Valor de la segona matriu: "

.section .text
_e2:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $40, %esp

  movl $1, %eax
  movl %eax, -8(%ebp)

  movl $ec5, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  call _new_line
  addl $0, %esp

_e3:

  movl -8(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e22
  jmp _e4
_e22:

  jmp _e8

_e4:

  movl $1, %eax
  movl %eax, -4(%ebp)

_e5:

  movl -4(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e23
  jmp _e6
_e23:

  jmp _e7

_e6:

  movl -8(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -16(%ebp)

  movl -16(%ebp), %eax
  movl -4(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -20(%ebp)

  movl -20(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -24(%ebp)

  movl -24(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -28(%ebp)

  movl -28(%ebp), %eax
  movl 12(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -32(%ebp)

  movl -32(%ebp), %eax
  movl %eax, -12(%ebp)

  leal -12(%ebp), %eax
  pushl %eax

  call _puti
  addl $4, %esp

  movl $ec8, %eax
  pushl %eax

  call _putc
  addl $4, %esp

  movl -4(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -36(%ebp)

  movl -36(%ebp), %eax
  movl %eax, -4(%ebp)

  jmp _e5

_e7:

  call _new_line
  addl $0, %esp

  movl -8(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -40(%ebp)

  movl -40(%ebp), %eax
  movl %eax, -8(%ebp)

  jmp _e3

_e8:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e9:

  movl $DISP, %eax
  addl $8, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $76, %esp

  movl $1, %eax
  movl %eax, -8(%ebp)

_e10:

  movl -8(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e24
  jmp _e11
_e24:

  jmp _e15

_e11:

  movl $1, %eax
  movl %eax, -4(%ebp)

_e12:

  movl -4(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e25
  jmp _e13
_e25:

  jmp _e14

_e13:

  movl -8(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -12(%ebp)

  movl -12(%ebp), %eax
  movl -4(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -16(%ebp)

  movl -16(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -20(%ebp)

  movl -20(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -24(%ebp)

  movl -8(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -28(%ebp)

  movl -28(%ebp), %eax
  movl -4(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -32(%ebp)

  movl -32(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -36(%ebp)

  movl -36(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -40(%ebp)

  movl -8(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -44(%ebp)

  movl -44(%ebp), %eax
  movl -4(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -48(%ebp)

  movl -48(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -52(%ebp)

  movl -52(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -56(%ebp)

  movl -40(%ebp), %eax
  movl 12(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -60(%ebp)

  movl -56(%ebp), %eax
  movl 16(%ebp), %esi
  addl %eax, %esi
  movl (%esi), %eax
  movl %eax, -64(%ebp)

  movl -60(%ebp), %eax
  movl -64(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -68(%ebp)

  movl -24(%ebp), %eax
  movl -68(%ebp), %ebx
  movl 12(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl -4(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -72(%ebp)

  movl -72(%ebp), %eax
  movl %eax, -4(%ebp)

  jmp _e12

_e14:

  movl -8(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -76(%ebp)

  movl -76(%ebp), %eax
  movl %eax, -8(%ebp)

  jmp _e10

_e15:

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $8, %eax
  popl %eax
  ret

_e1:

  movl $DISP, %eax
  addl $4, %eax
  pushl (%eax)
  pushl %ebp
  movl %esp, %ebp
  movl %ebp, (%eax)
  subl $84, %esp

  movl $1, %eax
  movl %eax, -40(%ebp)

_e16:

  movl -40(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e26
  jmp _e17
_e26:

  jmp _e21

_e17:

  movl $1, %eax
  movl %eax, -36(%ebp)

_e18:

  movl -36(%ebp), %eax
  movl $2, %ebx
  cmpl %ebx, %eax
  jg _e27
  jmp _e19
_e27:

  jmp _e20

_e19:

  movl $ec9, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  leal -44(%ebp), %eax
  pushl %eax

  call _geti
  addl $4, %esp

  movl -40(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -48(%ebp)

  movl -48(%ebp), %eax
  movl -36(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -52(%ebp)

  movl -52(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -56(%ebp)

  movl -56(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -60(%ebp)

  movl -60(%ebp), %eax
  movl -44(%ebp), %ebx
  leal -32(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl $ec10, %eax
  pushl %eax

  call _puts
  addl $4, %esp

  leal -44(%ebp), %eax
  pushl %eax

  call _geti
  addl $4, %esp

  movl -40(%ebp), %eax
  movl $2, %ebx
  imull %ebx, %eax
  movl %eax, -64(%ebp)

  movl -64(%ebp), %eax
  movl -36(%ebp), %ebx
  addl  %ebx, %eax
  movl %eax, -68(%ebp)

  movl -68(%ebp), %eax
  movl $3, %ebx
  subl %ebx, %eax
  movl %eax, -72(%ebp)

  movl -72(%ebp), %eax
  movl $4, %ebx
  imull %ebx, %eax
  movl %eax, -76(%ebp)

  movl -76(%ebp), %eax
  movl -44(%ebp), %ebx
  leal -16(%ebp), %edi
  addl %eax, %edi
  movl %ebx, (%edi)

  movl -36(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -80(%ebp)

  movl -80(%ebp), %eax
  movl %eax, -36(%ebp)

  jmp _e18

_e20:

  movl -40(%ebp), %eax
  movl $1, %ebx
  addl  %ebx, %eax
  movl %eax, -84(%ebp)

  movl -84(%ebp), %eax
  movl %eax, -40(%ebp)

  jmp _e16

_e21:

  leal -16(%ebp), %eax
  pushl %eax

  leal -32(%ebp), %eax
  pushl %eax

  call _e9
  addl $8, %esp

  leal -32(%ebp), %eax
  pushl %eax

  call _e2
  addl $4, %esp

  movl %ebp, %esp
  popl %ebp
  movl $DISP, %eax
  addl $4, %eax
  popl %eax
  ret

