package body d_vector is



   procedure Empty (v: out Vector) is
      n: Index renames v.n;
   begin
      n:= 0;
   end Empty;


   procedure Put (v: in out Vector; k: in key; x: in item) is
      m: Mem_Space renames v.m;
      n: Index renames v.n;
   begin
      if n=Index'Last then raise Space_Overflow; end if;
      n:=n+1; m(n):=(k => k, x => x);
   end Put;


   function Get (v: in Vector; k: in key) return item is
      m: Mem_Space renames v.m;
      n: Index renames v.n;
      i: Index;
   begin
      i:= Index(1);
      while i<=n loop
         if m(i).k=k then return m(i).x; end if;
         i:= i+1;
      end loop;

      return Null_Item;
   end Get;

   procedure First (v: in Vector; it: out Iterator) is
      i: Index renames it.i;
   begin
      i:=1;
   end First;



   function Is_Valid (v: in Vector; it: in Iterator) return Boolean is
      i: Index renames it.i;
      n: Index renames v.n;
   begin
      return i<=n;
   end Is_Valid;


   procedure Next (v: in Vector; it: in out Iterator) is
      i: Index renames it.i;
   begin
      i:=i+1;
   end Next;

   procedure Get
     (v: in Vector;
      it: in Iterator;
      k: out key;
      x: out item)
   is
      i: Index renames it.i;
      m: Mem_Space renames v.m;
   begin
      k:= m(i).k;
      x:= m(i).x;
   end Get;

end d_vector;
