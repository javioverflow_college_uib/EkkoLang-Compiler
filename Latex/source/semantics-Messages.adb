with Text_IO; use Text_IO;
with System;  use System;

package body semantics.Messages is

    function Img(pos: in Position) return String is
    begin
        return "line" & pos.line'Img & " and column" & pos.column'Img;
    end Img;

    procedure error_main_procedure_with_args(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Main procedure mustn't have arguments");
    end error_main_procedure_with_args;

    procedure error_identifier_already_seen(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Name " & Get(nt, id)
               & " has already been declared");
    end error_identifier_already_seen;

    procedure error_expected_type(pos: in Position; id: in Name_Id) is
    begin
        put_line("Error at " & Img(pos) & ": Expected type. Found '" & Get(nt, id) & "' name");
    end error_expected_type;

    procedure error_expected_scalar_type(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Expected a scalar type. Found '" & Get(nt, id)
               & "' name");
    end error_expected_scalar_type;

    procedure error_incompatible_value(pos: in Position; id: in Name_Id) is
    begin
        put_line("Error at " & Img(pos) & ": Value '" & Get(nt, id) & "' is not compatible");
    end error_incompatible_value;

    procedure error_value_out_of_range_lower(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Value '" & Get(nt, id)
               & "' is out of range. It's too low");
    end error_value_out_of_range_lower;

    procedure error_value_out_of_range_upper(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Value '" & Get(nt, id)
               & "' is out of range. It's too high");
    end error_value_out_of_range_upper;

    procedure error_incorrect_value_type(pos: in Position; idt: in Name_Id; idvt: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Value is of type '" & Get(nt, idvt)
               & "'. Expected type '" & Get(nt, idt) & "'");
    end error_incorrect_value_type;

    procedure error_expected_integer_value(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected integer value");
    end error_expected_integer_value;

    procedure error_expected_constant(pos: in Position; id: in Name_Id) is
    begin
        put_line("Error at " & Img(pos) & ": '" & Get(nt, id) & "' must be constant");
    end error_expected_constant;

   procedure error_field_in_record_already_exists(pos: in Position; idf: in Name_Id;
                                                  idr: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Field '" & Get(nt, idf) & "' in record '"
               & Get(nt, idf) & "' declaration appears more than once.");
    end error_field_in_record_already_exists;

    procedure error_expected_boolean_expression(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected boolean expression");
    end error_expected_boolean_expression;

    procedure error_expected_variable_name(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected variable name");
    end error_expected_variable_name;

    procedure error_expresion_and_ref_different_type(pos: in Position; idt: in Name_Id) is
    begin
      put_line("Error at " & Img(pos)
               & ": Expression can't be assigned to variable. Expected type '"
               & Get(nt, idt) & "'");
    end error_expresion_and_ref_different_type;

    procedure error_expected_scalar_reference(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected a scalar type");
    end error_expected_scalar_reference;

    procedure error_expected_record_identifier(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected record type");
    end error_expected_record_identifier;

    procedure error_record_field_does_not_exists(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Field does not exists");
    end error_record_field_does_not_exists;

    procedure error_expected_array_identifier(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected array type");
    end error_expected_array_identifier;

    procedure error_missing_value_for_indexes(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Missing value for index/es");
    end error_missing_value_for_indexes;

    procedure error_missing_value_for_params(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Missing value for parameters");
    end error_missing_value_for_params;

    procedure error_expression_and_index_different_ut(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Expression underlying type does not match with array index type");
    end error_expression_and_index_different_ut;

   procedure error_expression_and_index_different_type(pos: in Position; iidt: in Name_Id;
                                                       eidt: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Found expression type '" & Get(nt, eidt)
               & "'. Expected type: '" & Get(nt, iidt) & "'");
    end error_expression_and_index_different_type;

    procedure error_more_values_than_indexes(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": More indexes values were given than expected");
    end error_more_values_than_indexes;

    procedure error_expression_and_param_different_type(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Expression type does not match with parameter type");
    end error_expression_and_param_different_type;

    procedure error_more_values_than_params(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": More parameter values were given than expected");
    end error_more_values_than_params;

    procedure error_expected_integer_expression(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected integer expression");
    end error_expected_integer_expression;

    procedure error_expected_integer_expression_on_arith_op(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Cannot apply arithmetic operator over non integer expressions");
    end error_expected_integer_expression_on_arith_op;

    procedure error_expected_bool_expression_on_logical_op(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Cannot apply logical operator over non boolean expressions");
    end error_expected_bool_expression_on_logical_op;

    procedure error_expected_bool_expression_on_not_op(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Cannot apply NOT operator over non boolean expressions");
    end error_expected_bool_expression_on_not_op;

    procedure error_neg_on_non_integer_value(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Cannot apply unary minus operator over non integer expression."
               & " Try using integer expression");
    end error_neg_on_non_integer_value;

    procedure error_expressions_have_different_types(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expressions have different types");
    end error_expressions_have_different_types;

    procedure error_expected_scalar_expressions(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected scalar expressions");
    end error_expected_scalar_expressions;

    procedure error_value_type_for_range_incompatible(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Value type for range incompatible");
    end error_value_type_for_range_incompatible;

    procedure error_range_values_not_correct(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Range values are not correct");
    end error_range_values_not_correct;

   procedure error_end_name_does_not_match(pos: in Position; pid: in Name_Id;
                                           eid: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Procedure ending name '" & Get(nt, eid)
               & "' does not match with '" & Get(nt, pid) & "' declared at the preamble");
    end error_end_name_does_not_match;

   procedure error_argument_name_already_registered(pos: in Position; pid: in Name_Id;
                                                    aid: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": At procedure '" & Get(nt, pid)
               & "' there is more than one argument called '" & Get(nt, aid) & "'.");
    end error_argument_name_already_registered;

    procedure error_expected_procedure_call(pos: in Position) is
    begin
        put_line("Error at " & Img(pos) & ": Expected procedure call");
    end error_expected_procedure_call;

    procedure error_bool_with_rel_op(pos: in Position) is
    begin
      put_line("Error at " & Img(pos)
               & ": Relational comparison with bool types is not supported.");
    end error_bool_with_rel_op;

    procedure error_out_arg_is_not_var(pos: in Position; pid: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Argument '" & Get(nt, pid)
               & "' has out/in-out mode so you cannot pass constant values. Try using variable.");
    end error_out_arg_is_not_var;

    procedure error_variable_not_declared(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Variable '" & Get(nt, id)
               & "' has not been declared");
    end error_variable_not_declared;

    procedure error_proc_not_declared(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Procedure '" & Get(nt, id)
               & "' has not been declared");
    end error_proc_not_declared;

    procedure error_record_var_not_declared(pos: in Position; id: in Name_Id) is
    begin
      put_line("Error at " & Img(pos) & ": Record variable '" & Get(nt, id)
               & "' has not been declared");
    end error_record_var_not_declared;

    procedure error_arrayOrCall_not_declared(pos: in Position; id: in Name_Id) is
    begin
        put_line("Error at " & Img(pos) & ": Name '" & Get(nt, id) & "' has not been declared");
    end error_arrayOrCall_not_declared;

    procedure error_name_not_declared(pos: in Position; id: in Name_Id) is
    begin
        put_line("Error at " & Img(pos) & ": Name '" & Get(nt, id) & "' has not been declared");
    end error_name_not_declared;

end semantics.Messages;
