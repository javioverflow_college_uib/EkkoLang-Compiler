with Ada.Strings.Hash; use Ada.Strings;

package body declarations.d_Names_Table is

    function Is_Characters_Table_Overflow_If_Add(nt: in Name_Table; s: in String)
        return Boolean is
        st: String_Ids_Table renames nt.st;
        sa: Natural renames nt.sa;
        idt: Name_Ids_Table renames nt.idt;
        ida: Natural renames nt.ida;
        last_name_cti, last_string_cti: Characters_Table_Index;
    begin
        last_name_cti:= idt(Name_Id(ida)).lci;
        last_string_cti:= st(String_Id(sa));
        return last_name_cti + s'Length >= last_string_cti;
    end Is_Characters_Table_Overflow_If_Add;

    procedure Empty(nt: out Name_Table) is
        dt: Dispersion_Table renames nt.dt;
        idt: Name_Ids_Table renames nt.idt;
        ida: Natural renames nt.ida;
        st: String_Ids_Table renames nt.st;
        sa: Natural renames nt.sa;
    begin
        for i in Dispersion_Table_Index loop dt(i) := Null_Name_Id; end loop;
        ida := 0; idt(0) := (Null_Name_Id, 1);
        sa := 0; st(0) := Max_Characters_Amount;
    end Empty;


    procedure Put(nt: in out Name_Table; name_text: in String; id: out Name_Id) is
        dt_index: Dispersion_Table_Index;
        dt: Dispersion_Table renames nt.dt;
        idt: Name_Ids_Table renames nt.idt;
        ida: Natural renames nt.ida;
        ct: Characters_Table renames nt.ct;

        last_cti: Characters_Table_Index;

        function Same_Name_Text return boolean is
            first_cti, last_cti, current_cti: Characters_Table_Index;
            name_id_index: Name_Id;
        begin
            name_id_index:= Name_Id(id);
            first_cti:= idt(name_id_index - 1).lci + 1;
            last_cti:= idt(name_id_index).lci;

            if (last_cti + 1 - first_cti) /= name_text'Length then return false; end if;

            current_cti:= first_cti;
            for name_index in name_text'Range loop
                if name_text(name_index) /= ct(current_cti) then return false; end if;
                current_cti := current_cti + 1;
            end loop;

            return true;
        end Same_Name_Text;

    begin
        dt_index:= Ada.Strings.Hash(name_text) mod Dispersion_Table_Length;
        id:= dt(dt_index);
        while id /= Null_Name_Id and then not Same_Name_Text loop
            id:= idt(id).next;
        end loop;

        if id=Null_Name_Id then
            if Is_Characters_Table_Overflow_If_Add(nt, name_text) 
              or (ida+1) > Max_Strings_Amount then
                raise Space_Overflow;
            end if;

            last_cti:= idt(Name_Id(ida)).lci;
            for name_index in name_text'range loop
                last_cti := last_cti + 1;
                ct(last_cti):= name_text(name_index);
            end loop;

            ida:= ida+1; id:= Name_Id(ida);
            idt(id):= (dt(dt_index), last_cti); dt(dt_index):= id;
        end if;
    end Put;


    function Get(nt: in Name_Table; id: in Name_Id) return String is
        idt: Name_Ids_Table renames nt.idt;
        ida: Natural renames nt.ida;
        ct: Characters_Table renames nt.ct;

        first_cti, last_cti: Characters_Table_Index;
    begin
        if id < Name_Id(1) or id > Name_Id(ida) then raise Bad_Use; end if;
        first_cti:= idt(id-1).lci + 1;
        last_cti:= idt(id).lci;
        return ct(first_cti..last_cti);
    end Get;


    procedure Put(nt: in out Name_Table; string_text: in String; id: out String_Id) is
        st: String_Ids_Table renames nt.st;
        sa: Natural renames nt.sa;
        ct: Characters_Table renames nt.ct;

        last_cti: Characters_Table_Index;
    begin
        if Is_Characters_Table_Overflow_If_Add(nt, string_text) 
          or (sa+1) > Max_Strings_Amount then
            raise Space_Overflow;
        end if;

        last_cti:= st(String_Id(sa));
        for string_index in reverse string_text'Range loop
            last_cti := last_cti - 1;
            ct(last_cti):= string_text(string_index);
        end loop;

        sa:= sa+1; 
        id:= String_Id(sa); st(id):= last_cti;
    end Put;


    function Get(nt: in Name_Table; id: in String_Id) return String is
        st: String_Ids_Table renames nt.st;
        sa: Natural renames nt.sa;
        ct: Characters_Table renames nt.ct;

        first_cti, last_cti: Characters_Table_Index;
    begin
        if id < String_Id(1) or id > String_Id(sa) then raise Bad_Use; end if;
        first_cti:= st(id);
        last_cti:= st(id-1) - 1;
        return ct(first_cti..last_cti);
    end Get;

end declarations.d_Names_Table;
