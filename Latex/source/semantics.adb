with Ekko_Lexical_Analyzer; use Ekko_Lexical_Analyzer;
with Interfaces; use Interfaces;

package body semantics is

   procedure Put_Standard_Environment;
   function Without_Extension(filepath: in String) return Unbounded_String is
      i: Natural;
   begin
      i:= filepath'Last;
      while filepath(i) /= '.' and i >= filepath'First loop
         i:= i-1;
      end loop;
      if i/=0 then
         return To_Unbounded_String(filepath(filepath'First..i-1));
      else
         return To_Unbounded_String(filepath);
      end if;
   end Without_Extension;

   procedure Get_Analysis_Ready (filepath: in String) is
   begin

      Set_Input_File(filepath);

      Empty(nt);
      Empty(st);
      Empty(ct);

      va:= Null_Var_Id;
      pa:= Null_Proc_Id;
      la:= Null_Label;
      lac:= Null_Label;
      pd:= Null_Proc_Depth;

      filepath_no_extension:= Without_Extension(filepath);
      Create(File => bin_3AC_file,
             Mode => Inout_File,
             Name => To_String(filepath_no_extension) & ".3AC");

      successful_type_checking:= True;

      Put_Standard_Environment;
   end Get_Analysis_Ready;


   procedure Get_Analysis_Done is
   begin
      Close(File => bin_3AC_file);
   end Get_Analysis_Done;

   procedure Generate_Symbolic_3AC_File is
      instr: Instruction;
   begin
      Set_Index(File => bin_3AC_file,
                To   => IO_3AC.Positive_Count'First);

      Create(File => sym_3AC_file,
             Mode => Out_File,
             Name => To_String(filepath_no_extension) & ".3ACS");
      while not End_Of_File(bin_3AC_file) loop
         Read(File => bin_3AC_file,
              Item => instr);
         Put_Line(File => sym_3AC_file,
                  Item => Img(instr, vt, pt, nt));
      end loop;
      Close(File => sym_3AC_file);

   end Generate_Symbolic_3AC_File;

   procedure Put_Standard_Environment is
      intid, charid, boolid, pnid: Name_Id;
      idt, nid: Name_Id;
      td: Type_Description;
      d, dp: Description;
      err: Boolean:= False;
      vid: Var_Id;
      pid: Proc_Id;
   begin
      -- Integer type
      Put(nt        => nt,
          name_text => "int",
          id        => idt);
      td:= (ut => int_ut,
            occu => INT_OCCUPANCY, lb => Value(Integer_16'First+1), ub => Value(Integer_16'Last));
      d := (dt => type_d, td => td);
      Put(st    => st,
          id    => idt,
          d     => d,
          error => err);
      intid:= idt;

      -- Character type
      Put(nt        => nt,
          name_text => "char",
          id        => idt);
      td:= (ut => char_ut,
            occu => CHAR_OCCUPANCY, lb => 0, ub => 255);
      d := (dt => type_d, td => td);
      Put(st    => st,
          id    => idt,
          d     => d,
          error => err);
      charid:= idt;

      -- Boolean type
      Put(nt        => nt,
          name_text => "bool",
          id        => idt);
      td:= (ut => bool_ut,
            occu => BOOL_OCCUPANCY, lb => -1, ub => 0);
      d := (dt => type_d, td => td);
      Put(st    => st,
          id    => idt,
          d     => d,
          error => err);
      boolid:= idt;

      -- -- true
      Put(nt        => nt,
          name_text => "true",
          id        => nid);
      vid:= New_Constant(vt => vt,
                         va => va,
                         ct => ct,
                         la => lac,
                         v  => bool_true_value,
                         ut => bool_ut);
      d:= (dt => const_d,
           ct => idt, cn => vid);
      Put(st    => st,
          id    => nid,
          d     => d,
          error => err);

      -- -- false
      Put(nt        => nt,
          name_text => "false",
          id        => nid);
      vid:= New_Constant(vt => vt,
                         va => va,
                         ct => ct,
                         la => lac,
                         v  => bool_false_value,
                         ut => bool_ut);
      d:= (dt => const_d,
           ct => idt, cn => vid);
      Put(st    => st,
          id    => nid,
          d     => d,
          error => err);

      -- Getc
      Put(nt        => nt,
          name_text => "getc",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 1);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);

      Put(nt        => nt,
          name_text => "c",
          id        => nid);
      d:= (dt => arg_d,
           argt => charid, argmd => in_out_md);
      Put_Argument(st    => st,
                   pid   => pnid,
                   aid   => nid,
                   ad    => d,
                   error => err);


      -- Putc
      Put(nt        => nt,
          name_text => "putc",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 1);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);

      Put(nt        => nt,
          name_text => "c",
          id        => nid);
      d:= (dt => arg_d,
           argt => charid, argmd => in_md);
      Put_Argument(st    => st,
                   pid   => pnid,
                   aid   => nid,
                   ad    => d,
                   error => err);

      -- Geti
      Put(nt        => nt,
          name_text => "geti",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 1);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);

      Put(nt        => nt,
          name_text => "i",
          id        => nid);
      d:= (dt => arg_d,
           argt => intid, argmd => in_out_md);
      Put_Argument(st    => st,
                   pid   => pnid,
                   aid   => nid,
                   ad    => d,
                   error => err);

      -- Puti
      Put(nt        => nt,
          name_text => "puti",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 1);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);

      Put(nt        => nt,
          name_text => "i",
          id        => nid);
      d:= (dt => arg_d,
           argt => intid, argmd => in_md);
      Put_Argument(st    => st,
                   pid   => pnid,
                   aid   => nid,
                   ad    => d,
                   error => err);


      -- Puts
      Put(nt        => nt,
          name_text => "puts",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 1);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);

      Put(nt        => nt,
          name_text => "s",
          id        => nid);
      d:= (dt => arg_d,
           argt => Null_Name_Id, argmd => in_md);
      Put_Argument(st    => st,
                   pid   => pnid,
                   aid   => nid,
                   ad    => d,
                   error => err);

      -- New_Line
      Put(nt        => nt,
          name_text => "new_line",
          id        => pnid);
      pid:= New_Ext_Proc(pt  => pt,
                         pa  => pa,
                         nid => pnid,
                         pam => 0);
      dp:= (dt => proc_d, pn => pid);
      Put(st    => st,
          id    => pnid,
          d     => dp,
          error => err);


   end Put_Standard_Environment;
end semantics;























