with declarations.d_Attribute; use declarations.d_Attribute;

package semantics.Code_Generation_Routines is

   -- S
   procedure CG_S(s: out Attribute; proc_decl: in Attribute);

   -- Procedure
   procedure CG_MProc_Preamble;
   procedure CG_MProc_Begin;
   procedure CG_Proc_Decl(proc_decl                           : out Attribute;
                          proc_preamble, decls, statements, id: in Attribute);

   procedure CG_Proc_Preamble_Simple(proc_preamble: out Attribute;
                                     id           : in Attribute);

   procedure CG_Proc_Preamble_Complex(proc_preamble    : out Attribute;
                                      pre_proc_preamble: in Attribute);

   procedure CG_Pre_Proc_Preamble_Simple(pre_proc_preamble: out Attribute;
                                         id, arg          : in Attribute);

   procedure CG_Pre_Proc_Preamble_Recursive(pre_proc_preamble           : out Attribute;
                                            inner_pre_proc_preamble, arg: in Attribute);

   procedure CG_Arg(arg           : out Attribute;
                    ida, mode, idt: in Attribute);

   procedure CG_Mode_In    (mode: out Attribute);
   procedure CG_Mode_Out   (mode: out Attribute);
   procedure CG_Mode_In_Out(mode: out Attribute);

   -- Declarations
   procedure CG_Decls(decls            : out Attribute;
                      inner_decls, decl: in Attribute);

   procedure CG_Decls(decls: out Attribute);

   procedure CG_Decl(decl      : out Attribute;
                     inner_decl: in Attribute);

   -- Constant Declaration
   procedure CG_Const_Decl(const_decl     : out Attribute;
                           idc, idt, value: in Attribute);

   procedure CG_Value(value     : out Attribute;
                      elem_value: in Attribute);

   procedure CG_Value_Negative(value     : out Attribute;
                               elem_value: in Attribute);

   procedure CG_Elem_Value_Id(elem_value: out Attribute;
                              id        : in Attribute);

   procedure CG_Elem_Value_Literal(elem_value: out Attribute;
                                   literal   : in Attribute);

   -- Variable Declaration
   procedure CG_Var_Decl(var_decl         : out Attribute;
                         id, cont_var_decl: in Attribute);

   procedure CG_Cont_Var_Decl(cont_var_decl          : out Attribute;
                              id, inner_cont_var_decl: in Attribute);

   procedure CG_Cont_Var_Decl(cont_var_decl: out Attribute;
                              id           : in Attribute);

   -- Type declaration
   procedure CG_Type_Decl(type_decl: out Attribute;
                          decl     : in Attribute);

   -- Type::Subrange Declaration
   procedure CG_Subrange_Decl(subrange_decl                    : out Attribute;
                              id, idt, lower_value, upper_value: in Attribute);

   -- Type::Record Declaration
   procedure CG_Record_Decl(record_decl    : out Attribute;
                            pre_record_decl: in Attribute);

   procedure CG_Pre_Record_Decl(pre_record_decl               : out Attribute;
                                inner_pre_record_decl, id, idt: in Attribute);

   procedure CG_Pre_Record_Decl(pre_record_decl: out Attribute;
                                id             : in Attribute);

   -- Type::Array Declaration
   procedure CG_Array_Decl(array_decl         : out Attribute;
                           pre_array_decl, idt: in Attribute);

   procedure CG_Pre_Array_Decl_Recursive(pre_array_decl           : out Attribute;
                                         inner_pre_array_decl, idt: in Attribute);

   procedure CG_Pre_Array_Decl_Simple(pre_array_decl: out Attribute;
                                      id, idt       : in Attribute);

   -- Statements
   procedure CG_Statements(statements                 : out Attribute;
                           inner_statements, statement: in Attribute);

   procedure CG_Statements(statements: out Attribute);

   procedure CG_Statement(statement      : out Attribute;
                          inner_statement: in Attribute);

   procedure CG_M_Expr(M : out Attribute);
   procedure CG_M_Else(ME: out Attribute);

   procedure CG_Cond_Statement(cond_statement  : out Attribute;
                               expr, M, statements: in Attribute);

   procedure CG_Cond_Statement(cond_statement                          : out Attribute;
                               expr, M, statements, ME, else_statements: in Attribute);

   procedure CG_Loop_Statement(loop_statement          : out Attribute;
                               M1, expr, M2, statements: in Attribute);

   procedure CG_Assig_Statement(assig_statement: out Attribute;
                                ref, expr      : in Attribute);

   procedure CG_Call_Statement(call_statement: out Attribute;
                               ref           : in Attribute);

   -- References
   procedure CG_Ref_Simple(ref: out Attribute;
                           id : in Attribute);

   procedure CG_Ref(ref          : out Attribute;
                    inner_ref, id: in Attribute);

   procedure CG_Pre_ArrayOrCall_Simple(pre_arrayOrCall: in out Attribute;
                                       ref, expr: in Attribute);

   procedure CG_Pre_ArrayOrCall_Recursive(pre_arrayOrCall            : out Attribute;
                                          inner_pre_arrayOrCall, expr: in Attribute);

   procedure CG_Ref_Recursive(ref            : out Attribute;
                              inner_pre_arrayOrCall: in Attribute);

   -- Expressions
   procedure CG_Expr_Logical(expr                 : out Attribute;
                             left_expr, logical_op, M, right_expr: in Attribute);

   procedure CG_Expr_Logical(expr      : out Attribute;
                             inner_expr: in Attribute);

   procedure CG_Expr_Relational(expr                 : out Attribute;
                                left_expr, relational_op, right_expr: in Attribute);

   procedure CG_Expr_Arithmetic(expr                 : out Attribute;
                                left_expr, arith_op, right_expr: in Attribute);

   procedure CG_Expr_Neg(expr      : out Attribute;
                         inner_expr: in Attribute);

   procedure CG_Ref(expr: out Attribute;
                    ref : in Attribute);

   procedure CG_Literal(expr   : out Attribute;
                        literal: in Attribute);

   procedure CG_Enclosing(expr      : out Attribute;
                          inner_expr: in Attribute);

end semantics.Code_Generation_Routines;

