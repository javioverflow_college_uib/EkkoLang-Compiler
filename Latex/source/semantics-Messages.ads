with declarations;             use declarations;
with declarations.d_Attribute; use declarations.d_Attribute;

package semantics.Messages is

    procedure error_main_procedure_with_args(pos: in Position);

    procedure error_identifier_already_seen(pos: in Position; id: in Name_Id);

    procedure error_expected_type(pos: in Position; id: in Name_Id);

    procedure error_expected_scalar_type(pos: in Position; id: in Name_Id);

    procedure error_incompatible_value(pos: in Position; id: in Name_Id);

    procedure error_value_out_of_range_lower(pos: in Position; id: in Name_Id);

    procedure error_value_out_of_range_upper(pos: in Position; id: in Name_Id);

    procedure error_incorrect_value_type(pos: in Position; idt: in Name_Id; idvt: in Name_Id);

    procedure error_expected_integer_value(pos: in Position);

    procedure error_expected_constant(pos: in Position; id: in Name_Id);

    procedure error_field_in_record_already_exists(pos: in Position; idf: in Name_Id; idr: in Name_Id);

    procedure error_expected_boolean_expression(pos: in Position);

    procedure error_expected_variable_name(pos: in Position);

    procedure error_expresion_and_ref_different_type(pos: in Position; idt: in Name_Id);

    procedure error_expected_scalar_reference(pos: in Position);

    procedure error_expected_record_identifier(pos: in Position);

    procedure error_record_field_does_not_exists(pos: in Position);

    procedure error_expected_array_identifier(pos: in Position);

    procedure error_missing_value_for_indexes(pos: in Position);

    procedure error_missing_value_for_params(pos: in Position);

    procedure error_expression_and_index_different_ut(pos: in Position);

    procedure error_expression_and_index_different_type(pos: in Position; iidt: in Name_Id; eidt: in Name_Id);

    procedure error_more_values_than_indexes(pos: in Position);

    procedure error_expression_and_param_different_type(pos: in Position);

    procedure error_more_values_than_params(pos: in Position);

    procedure error_expected_integer_expression(pos: in Position);

    procedure error_expected_integer_expression_on_arith_op(pos: in Position);

    procedure error_expected_bool_expression_on_logical_op(pos: in Position);

    procedure error_expected_bool_expression_on_not_op(pos: in Position);

    procedure error_neg_on_non_integer_value(pos: in Position);

    procedure error_expressions_have_different_types(pos: in Position);

    procedure error_expected_scalar_expressions(pos: in Position);

    procedure error_value_type_for_range_incompatible(pos: in Position);

    procedure error_range_values_not_correct(pos: in Position);

    procedure error_end_name_does_not_match(pos: in Position; pid: in Name_Id; eid: in Name_Id);

    procedure error_argument_name_already_registered(pos: in Position; pid: in Name_Id; aid: in Name_Id);

    procedure error_expected_procedure_call(pos: in Position);

    procedure error_bool_with_rel_op(pos: in Position);

    procedure error_out_arg_is_not_var(pos: in Position; pid: in Name_Id);

    procedure error_variable_not_declared(pos: in Position; id: in Name_Id);

    procedure error_proc_not_declared(pos: in Position; id: in Name_Id);

    procedure error_record_var_not_declared(pos: in Position; id: in Name_Id);

    procedure error_arrayOrCall_not_declared(pos: in Position; id: in Name_Id);

    procedure error_name_not_declared(pos: in Position; id: in Name_Id);

end semantics.Messages;
