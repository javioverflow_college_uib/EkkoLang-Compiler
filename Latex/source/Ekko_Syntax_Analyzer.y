---------------------------------
-- Tokens 
---------------------------------

-- Procedure
%token action_kw
%token is_kw
%token begin_kw
%token end_kw
%token opening_brackets
%token in_kw
%token out_kw
%token semicolon
%token closing_brackets

-- Type declarations
%token type_kw
%token new_kw
%token range_kw
%token point
%token record_kw
%token array_kw
%token of_kw

-- Variable declarations
%token constant_kw
%token colon

-- Statements
%token if_kw
%token then_kw
%token else_kw

%token while_kw
%token loop_kw

-- Variable initialization
%token colonequal
%token null_kw

-- Logical operators
%token not_op
%token and_op
%token or_op

-- Arithmetic operators
%token addition_op
%token substraction_op
%token product_op
%token division_op
%token modulus_op

-- Indexes
%token comma

-- User input content
%token id
%token literal

---------------------------------
--  Precedences
---------------------------------
%left and_op
%left or_op
%left not_op

%nonassoc rel_op

%left addition_op substraction_op
%left un_subs_op
%left product_op division_op modulus_op


%with declarations.d_Attribute;
{ subtype YYSType is declarations.d_Attribute.Attribute; }


%%

S:
     PROC_DECL {SR_S($$,$1);}
  ;

PROC_DECL:
     action_kw MPROC_PREAMBLE PROC_PREAMBLE is_kw
         DECLS
     begin_kw MPROC_BEGIN
         STATEMENTS
     end_kw id semicolon {SR_Proc_Decl($$,$3,$5,$8,$10);}
  ;

MPROC_PREAMBLE: 
  -- Lambda
        {CG_MProc_Preamble;}
  ;

MPROC_BEGIN: 
  -- Lambda
        {CG_MProc_Begin;}
  ;

-- Procedure preamble --------------------------------

PROC_PREAMBLE:
     id
        {SR_Proc_Preamble_Simple($$,$1);}
  |  PRE_PROC_PREAMBLE closing_brackets 
        {SR_Proc_Preamble_Complex($$,$1);}
  ;

PRE_PROC_PREAMBLE:
     PRE_PROC_PREAMBLE semicolon ARG 
        {SR_Pre_Proc_Preamble_Recursive($$,$1,$3);}
  |  id opening_brackets ARG 
        {SR_Pre_Proc_Preamble_Simple($$,$1,$3);}
  ;

ARG:
     id colon MODE id 
        {SR_Arg($$,$1,$3,$4);}
  ;

MODE:
     in_kw 
        {SR_Mode_In($$);}
  |  out_kw 
        {SR_Mode_Out($$);}
  |  in_kw out_kw 
        {SR_Mode_In_Out($$);}
  ;

-- Declarations --------------------------------------

DECLS:
     DECLS DECL
        {SR_Decls($$,$1,$2);}
  |  -- lambda 
        {SR_Decls($$);}
  ;

DECL:
     CONST_DECL 
        {SR_Decl($$,$1);}
  |  VAR_DECL 
        {SR_Decl($$,$1);}
  |  TYPE_DECL 
        {SR_Decl($$,$1);}
  |  PROC_DECL 
        {SR_Decl($$,$1);}
  ;

-- Constant Declaration ------------------------------

CONST_DECL:
     id colon constant_kw id colonequal VALUE semicolon
        {SR_Const_Decl($$,$1,$4,$6);}
  ;

VALUE:
     ELEM_VALUE 
        {SR_Value($$,$1);}
  |  substraction_op ELEM_VALUE 
        {SR_Value_Negative($$,$2);}
  ;

ELEM_VALUE:
     id 
        {SR_Elem_Value_Id($$,$1);}
  |  literal 
        {SR_Elem_Value_Literal($$,$1);}
  ;

-- Variable Declaration ------------------------------

VAR_DECL:
     id CONT_VAR_DECL 
        {SR_Var_Decl($$,$1,$2);}
  ;

CONT_VAR_DECL:
     comma id CONT_VAR_DECL 
        {SR_Cont_Var_Decl($$,$2,$3);}
  |  colon id semicolon 
        {SR_Cont_Var_Decl($$,$2);}
  ;

-- Type Declaration ----------------------------------

TYPE_DECL:
     SUBRANGE_DECL 
        {SR_Type_Decl($$,$1);}
  |  ARRAY_DECL 
        {SR_Type_Decl($$,$1);}
  |  RECORD_DECL 
        {SR_Type_Decl($$,$1);}
  ;

-- Type::Subrange Declaration ------------------------

SUBRANGE_DECL:
     type_kw id is_kw new_kw id range_kw VALUE point point VALUE semicolon
        {SR_Subrange_Decl($$,$2,$5,$7,$10);}
  ;

-- Type::Record Declaration --------------------------

RECORD_DECL:
     PRE_RECORD_DECL end_kw record_kw semicolon 
        {SR_Record_Decl($$,$1);}
  ;

PRE_RECORD_DECL:
     PRE_RECORD_DECL id colon id semicolon 
        {SR_Pre_Record_Decl($$,$1,$2,$4);}
  |  type_kw id is_kw record_kw 
        {SR_Pre_Record_Decl($$,$2);}
  ;

-- Type::Array Declaration ---------------------------

ARRAY_DECL:
     PRE_ARRAY_DECL closing_brackets of_kw id semicolon 
        {SR_Array_Decl($$,$1,$4);}
  ;

PRE_ARRAY_DECL:
     PRE_ARRAY_DECL comma id 
        {SR_Pre_Array_Decl_Recursive($$,$1,$3);}
  |  type_kw id is_kw array_kw opening_brackets id 
        {SR_Pre_Array_Decl_Simple($$,$2,$6);}
  ;

-- Statements ----------------------------------------

STATEMENTS:
  |  STATEMENTS STATEMENT 
        {SR_Statements($$,$1,$2);}
  |  null_kw semicolon 
        {SR_Statements($$);}
  ;

STATEMENT:
     COND_STATEMENT 
        {SR_Statement($$,$1);}
  |  LOOP_STATEMENT 
        {SR_Statement($$,$1);}
  |  ASSIG_STATEMENT 
        {SR_Statement($$,$1);}
  |  CALL_STATEMENT 
        {SR_Statement($$,$1);}
  ;

COND_STATEMENT:
     if_kw EXPR M_EXPR then_kw 
         STATEMENTS 
     end_kw if_kw semicolon 
        {SR_Cond_Statement($$,$2,$3,$5);}
  |  if_kw EXPR M_EXPR then_kw
         STATEMENTS 
     else_kw M_ELSE
         STATEMENTS 
     end_kw if_kw semicolon 
        {SR_Cond_Statement($$,$2,$3,$5,$7,$8);}
  ;

M_EXPR:
     -- Lambda
        {CG_M_Expr($$);}     
  ;

M_ELSE:
     -- Lambda
         {CG_M_Else($$);}
  ;

LOOP_STATEMENT:
     while_kw M_EXPR EXPR loop_kw M_EXPR STATEMENTS end_kw loop_kw semicolon 
        {SR_Loop_Statement($$,$2,$3,$5,$6);}
  ;

ASSIG_STATEMENT:
     REF colonequal EXPR semicolon 
        {SR_Assig_Statement($$,$1,$3);}
  ;

CALL_STATEMENT:
     REF semicolon 
        {SR_Call_Statement($$,$1);}
  ;

-- References -----------------------------------

REF:
     id 
        {SR_Ref_Simple($$,$1);}
  |  REF point id 
        {SR_Ref($$,$1,$3);}
  |  PRE_ARRAY closing_brackets 
        {SR_Ref_Recursive($$,$1);}
  ;

PRE_ARRAY:
     PRE_ARRAY comma EXPR 
        {SR_Pre_ArrayOrCall_Recursive($$,$1,$3);}
  |  REF opening_brackets EXPR 
        {SR_Pre_ArrayOrCall_Simple($$,$1,$3);}
  ;

-- Expressions ----------------------------------

EXPR:
     EXPR and_op M_EXPR EXPR 
        {SR_Expr_Logical($$,$1,$2,$3,$4);}
  |  EXPR or_op M_EXPR EXPR 
        {SR_Expr_Logical($$,$1,$2,$3,$4);}
  |  not_op EXPR 
        {SR_Expr_Logical($$,$2);}
  |  EXPR rel_op EXPR 
        {SR_Expr_Relational($$,$1,$2,$3);}
  |  EXPR addition_op EXPR 
        {SR_Expr_Arithmetic($$,$1,$2,$3);}
  |  EXPR substraction_op EXPR 
        {SR_Expr_Arithmetic($$,$1,$2,$3);}
  |  EXPR product_op EXPR 
        {SR_Expr_Arithmetic($$,$1,$2,$3);}
  |  EXPR division_op EXPR 
        {SR_Expr_Arithmetic($$,$1,$2,$3);}
  |  EXPR modulus_op EXPR 
        {SR_Expr_Arithmetic($$,$1,$2,$3);}
  |  substraction_op EXPR %prec un_subs_op 
        {SR_Expr_Neg($$,$2);}
  |  REF 
        {SR_Ref($$,$1);}
  |  literal 
        {SR_Literal($$,$1);}
  |  opening_brackets EXPR closing_brackets 
        {SR_Enclosing($$,$2);}
  ;

%%
with Ekko_Lexical_Analyzer;     use Ekko_Lexical_Analyzer;
with Ekko_Lexical_Analyzer_dfa; use Ekko_Lexical_Analyzer_dfa;
with Ekko_Lexical_Analyzer_io;  use Ekko_Lexical_Analyzer_io;

with Ekko_Syntax_Analyzer_tokens; use Ekko_Syntax_Analyzer_tokens;
with Ekko_Syntax_Analyzer_goto;   use Ekko_Syntax_Analyzer_goto;
with Ekko_Syntax_Analyzer_shift_reduce; use Ekko_Syntax_Analyzer_shift_reduce;
with Ekko_Syntax_Analyzer_error_report; use Ekko_Syntax_Analyzer_error_report;

with semantics.Syntax_Routines; use semantics.Syntax_Routines;
with semantics.Code_Generation_Routines; use semantics.Code_Generation_Routines;

with Text_IO;

package Ekko_Syntax_Analyzer is
    procedure YYParse;
end Ekko_Syntax_Analyzer;


package body Ekko_Syntax_Analyzer is

##

end Ekko_Syntax_Analyzer; 

%%

%report_error
begin
    Text_IO.Put_Line("Syntax error at line" & Natural'Image(Line_Number)
        & " between columns" & Natural'Image(Offset) & " and" & Natural'Image(Finish) 
        & ". Hint: " & Message);
end;
