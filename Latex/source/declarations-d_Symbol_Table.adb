package body declarations.d_Symbol_Table is


    procedure Empty (st: out Symbol_Table) is
        dt : Description_Table renames st.dt;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
    begin
        -- Description table
        for id in Name_Id loop
            dt(id):= (dp => 0, d => (dt => null_d), e => 0);
        end loop;

        -- Expansion table gets populated from description table

        -- Depth table
        dp:= 0; dpt(dp):= 0;
        dp:= dp+1; dpt(dp):= dpt(dp-1);

    end Empty;


    procedure Put
        (st   : in out Symbol_Table;
         id   : in Name_Id;
         d    : in Description;
         error: in out Boolean)
    is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
        ei : Expansion_Index;
    begin

        -- Check if there is another with same id and depth
        if dt(id).dp = dp then error:= True; return; end if;

        -- Get expansion table cell
        ei:= dpt(dp)+1; dpt(dp):= ei;

        -- Copy data
        et(ei):= (id => id, dp => dt(id).dp, d => dt(id).d, n => dt(id).e);
        dt(id):= (dp => dp, d => d, e => 0);

    end Put;


    function Get (st: in Symbol_Table; id: in Name_Id) return Description is
        dt : Description_Table renames st.dt;
    begin
        return dt(id).d;
    end Get;


    procedure Update
        (st: in out Symbol_Table;
         id: in Name_Id;
         d : in Description)
    is
        dt : Description_Table renames st.dt;
    begin
        dt(id).d:= d;
    end Update;


    procedure Enter_Block (st: in out Symbol_Table) is
        dp : Depth renames st.dp;
        dpt: Depth_Table renames st.dpt;
    begin
        dp:= dp+1; dpt(dp):= dpt(dp-1);
    end Enter_Block;


    procedure Exit_Block (st: in out Symbol_Table) is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
        lei, uei : Expansion_Index; -- lower and upper expansion index
        id: Name_Id;
    begin

        -- Decrease depth
        uei:= dpt(dp); dp:= dp-1; lei:= dpt(dp);

        -- Copy et cells of highest depth to td
        while uei > lei loop
            if et(uei).dp /= -1 then  -- Indexes, fields and args must be skipped
                id:= et(uei).id;
                dt(id):= (dp => et(uei).dp, d => et(uei).d, e => et(uei).n);
            end if;
            uei:= uei-1;
        end loop;

    end Exit_Block;


    procedure Put_Field
        (st      : in out Symbol_Table;
         rid, fid: in Name_Id;
         d      : in Description;
         error   : out Boolean)
    is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
        ei : Expansion_Index;
        Already_Exists: exception;
    begin

        -- Check if rid is a record type
        if dt(rid).d.dt /= type_d then raise Not_A_Type; end if;
        if dt(rid).d.td.ut /= rec_ut then raise Not_A_Record; end if;

        -- Check if field id is already registered in this record
        ei:= dt(rid).e;
        while ei /= 0 and then et(ei).id /= fid loop
            ei:= et(ei).n;
        end loop;
        if ei /= 0 then raise Already_Exists; end if;

        -- Get expansion table free cell
        ei:= dpt(dp)+1; dpt(dp):= ei;

        -- Copy data
        et(ei):= (id => fid, dp => -1, d => d, n => dt(rid).e);
        dt(rid).e:= ei;

    exception
        when Already_Exists => error:= True;
        when others => raise;
    end Put_Field;


    function Get_Field
        (st      : in Symbol_Table;
         rid, fid: in Name_Id)
         return Description
    is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        ei : Expansion_Index;
        d  : Description;
    begin
        -- Check if rid is a record type
        if dt(rid).d.dt /= type_d then raise Not_A_Type; end if;
        if dt(rid).d.td.ut /= rec_ut then raise Not_A_Record; end if;

        -- Check if field id exists in this record
        ei:= dt(rid).e;
        while ei /= 0 and then et(ei).id /= fid loop
            ei:= et(ei).n;
        end loop;

        -- Get description
        if ei = 0 then d:= (dt => null_d); else d:= et(ei).d; end if;
        return d;

    end Get_Field;


    procedure Put_Index
        (st : in out Symbol_Table;
         aid: in Name_Id;
         d : in Description)
    is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
        ei, nei : Expansion_Index;  -- expansion index / next expansion index
    begin

        -- Check if rid is an array type
        if dt(aid).d.dt /= type_d then raise Not_A_Type; end if;
        if dt(aid).d.td.ut /= arr_ut then raise Not_An_Array; end if;

        -- Look for last registered index
        ei:= 0; nei:= dt(aid).e;
        while nei /= 0 loop
            ei:= nei; nei:= et(nei).n;
        end loop;

        -- Get expansion table free cell
        nei:= dpt(dp)+1; dpt(dp):= nei;

        -- Copy data
        et(nei):= (id => Null_Name_Id, dp => -1, d => d, n => 0);

        -- Point to the new index
        if ei = 0 then dt(aid).e:= nei; else et(ei).n:= nei; end if;

    end Put_Index;


    procedure First
        (st : in Symbol_Table;
         aid: in Name_Id;
         it : out Index_Iterator)
    is
        dt : Description_Table renames st.dt;
    begin

        -- Check if rid is an array type
        if dt(aid).d.dt /= type_d then raise Not_A_Type; end if;
        if dt(aid).d.td.ut /= arr_ut then raise Not_An_Array; end if;

        -- Get first index
        it.ei:= dt(aid).e;

    end First;


    function Is_Valid (it: in Index_Iterator) return Boolean is
    begin
        return it.ei /= 0;
    end Is_Valid;

    procedure Get
        (st: in Symbol_Table;
         it: in Index_Iterator;
         d: out Description)
    is
        et : Expansion_Table renames st.et;
    begin

        -- Check if it's valid
        if it.ei = 0 then raise Bad_Use; end if;

        -- Set index description
        d:= et(it.ei).d;

    end Get;


    procedure Next (st: in Symbol_Table; it: in out Index_Iterator) is
        et : Expansion_Table renames st.et;
    begin

        -- Check if it's valid
        if it.ei = 0 then raise Bad_Use; end if;

        -- Update iterator
        it.ei:= et(it.ei).n;

    end Next;


    procedure Put_Argument
        (st      : in out Symbol_Table;
         pid, aid: in Name_Id;
         ad      : in Description;
         error   : out Boolean)
    is
        dt : Description_Table renames st.dt;
        et : Expansion_Table renames st.et;
        dpt: Depth_Table renames st.dpt;
        dp : Depth renames st.dp;
        ei, nei : Expansion_Index;  -- expansion index / next expansion index
        Already_Exists: exception;
    begin

        -- Check if pid is a procedure type
        if dt(pid).d.dt /= proc_d then raise Not_A_Procedure; end if;

        -- Look for last registered argument
        ei:= 0; nei:= dt(pid).e;
        while nei /= 0 and then et(nei).id /= aid loop
            ei:= nei; nei:= et(nei).n;
        end loop;

        -- Check if it doesn't exist
        if nei /= 0 then raise Already_Exists; end if;

        -- Get expansion table free cell
        nei:= dpt(dp)+1; dpt(dp):= nei;

        -- Copy data
        et(nei):= (id => aid, dp => -1, d => ad, n => 0);

        -- Point to the new argument
        if ei = 0 then dt(pid).e:= nei; else et(ei).n:= nei; end if;

    exception
        when Already_Exists => error:= True;
        when others => raise;
    end Put_Argument;


    procedure First
        (st : in Symbol_Table;
         pid: in Name_Id;
         it : out Argument_Iterator)
    is
        dt : Description_Table renames st.dt;
    begin

        -- Check if pid is a procedure type
        if dt(pid).d.dt /= proc_d then raise Not_A_Procedure; end if;

        -- Get first index
        it.ei:= dt(pid).e;

    end First;


    function Is_Valid (it: in Argument_Iterator) return Boolean is
    begin
        return it.ei /= 0;
    end Is_Valid;

    procedure Get
        (st : in Symbol_Table;
         it : in Argument_Iterator;
         aid: out Name_Id;
         ad : out Description)
    is
        et : Expansion_Table renames st.et;
    begin

        -- Check if it's valid
        if it.ei = 0 then raise Bad_Use; end if;

        -- Set output data
        aid:= et(it.ei).id; ad:= et(it.ei).d;

    end Get;


    procedure Next (st: in Symbol_Table; it: in out Argument_Iterator) is
        et : Expansion_Table renames st.et;
    begin

        -- Check if it's valid
        if it.ei = 0 then raise Bad_Use; end if;

        -- Update iterator
        it.ei:= et(it.ei).n;

    end Next;

end declarations.d_Symbol_Table;
