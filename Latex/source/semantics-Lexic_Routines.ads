with declarations.d_Attribute; use declarations.d_Attribute;

package semantics.Lexic_Routines is

    type Literal_Type is (Char_Lit, Integer_Lit, String_Lit);

    procedure LR_Identifier (id: out Attribute; pos: in Position; text: in String);

    procedure LR_Literal_Integer (lit: out Attribute; pos: in Position; text: in String);
    procedure LR_Literal_Char    (lit: out Attribute; pos: in Position; text: in String);
    procedure LR_Literal_String  (lit: out Attribute; pos: in Position; text: in String);

    procedure LR_Relational_Less_Op          (rel_op: out Attribute; pos: in Position);
    procedure LR_Relational_Greater_Op       (rel_op: out Attribute; pos: in Position);
    procedure LR_Relational_Less_Equal_Op    (rel_op: out Attribute; pos: in Position);
    procedure LR_Relational_Greater_Equal_Op (rel_op: out Attribute; pos: in Position);
    procedure LR_Relational_Equal_Op         (rel_op: out Attribute; pos: in Position);
    procedure LR_Relational_Unequal_Op       (rel_op: out Attribute; pos: in Position);

   procedure LR_Arithmetic_Add_Op          (arith_op: out Attribute; pos: in Position);
   procedure LR_Arithmetic_Sub_Op          (arith_op: out Attribute; pos: in Position);
   procedure LR_Arithmetic_Mult_Op         (arith_op: out Attribute; pos: in Position);
   procedure LR_Arithmetic_Div_Op          (arith_op: out Attribute; pos: in Position);
   procedure LR_Arithmetic_Mod_Op          (arith_op: out Attribute; pos: in Position);

   procedure LR_Logical_And_Op  (logical_op: out Attribute; pos: in Position);
   procedure LR_Logical_Or_Op   (logical_op: out Attribute; pos: in Position);

end semantics.Lexic_Routines;
