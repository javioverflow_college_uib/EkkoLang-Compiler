with Ada.Containers; use Ada.Containers; -- Hash_Type

package declarations.d_Names_Table is

    type Name_Table is limited private;

    procedure Empty(nt: out Name_Table);
    procedure Put(nt: in out Name_Table; name_text: in String; id: out Name_Id);
    function Get(nt: in Name_Table; id: in Name_Id) return String;
    procedure Put(nt: in out Name_Table; string_text: in String; id: out String_Id);
    function Get(nt: in Name_Table; id: in String_Id) return String;

    Space_Overflow, Bad_Use: exception;

private

    Max_Characters_Amount: constant Natural:= Max_Names_Amount*Average_Name_Length +
        Max_Strings_Amount*Average_String_Length;

    Dispersion_Table_Length: constant Hash_Type:= Hash_Type(Max_Names_Amount);
    subtype Dispersion_Table_Index is Hash_Type range 0..Dispersion_Table_Length-1;
    type Dispersion_Table is array(Dispersion_Table_Index) of Name_Id;

    subtype Characters_Table_Index is Natural range 1..Max_Characters_Amount;
    subtype Characters_Table is String(Characters_Table_Index);

    type Hash_Node_Type is
        record
            next: Name_Id;
            lci: Characters_Table_Index;
        end record;

    type Name_Ids_Table is array(Name_Id) of Hash_Node_Type;

    type String_Ids_Table is array(String_Id) of Characters_Table_Index;

    type Name_Table is
        record
            dt: Dispersion_Table;

            idt: Name_Ids_Table;
            ida: Natural;

            st: String_Ids_Table;
            sa: Natural;

            ct: Characters_Table;
        end record;

end declarations.d_Names_Table;
