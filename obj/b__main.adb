pragma Ada_95;
pragma Source_File_Name (ada_main, Spec_File_Name => "b__main.ads");
pragma Source_File_Name (ada_main, Body_File_Name => "b__main.adb");
pragma Suppress (Overflow_Check);
with Ada.Exceptions;

package body ada_main is
   pragma Warnings (Off);

   E074 : Short_Integer; pragma Import (Ada, E074, "system__os_lib_E");
   E013 : Short_Integer; pragma Import (Ada, E013, "system__soft_links_E");
   E019 : Short_Integer; pragma Import (Ada, E019, "system__exception_table_E");
   E150 : Short_Integer; pragma Import (Ada, E150, "ada__containers_E");
   E044 : Short_Integer; pragma Import (Ada, E044, "ada__io_exceptions_E");
   E099 : Short_Integer; pragma Import (Ada, E099, "ada__strings_E");
   E105 : Short_Integer; pragma Import (Ada, E105, "ada__strings__maps_E");
   E177 : Short_Integer; pragma Import (Ada, E177, "ada__strings__maps__constants_E");
   E050 : Short_Integer; pragma Import (Ada, E050, "ada__tags_E");
   E048 : Short_Integer; pragma Import (Ada, E048, "ada__streams_E");
   E072 : Short_Integer; pragma Import (Ada, E072, "interfaces__c_E");
   E021 : Short_Integer; pragma Import (Ada, E021, "system__exceptions_E");
   E077 : Short_Integer; pragma Import (Ada, E077, "system__file_control_block_E");
   E066 : Short_Integer; pragma Import (Ada, E066, "system__file_io_E");
   E070 : Short_Integer; pragma Import (Ada, E070, "system__finalization_root_E");
   E068 : Short_Integer; pragma Import (Ada, E068, "ada__finalization_E");
   E125 : Short_Integer; pragma Import (Ada, E125, "system__storage_pools_E");
   E119 : Short_Integer; pragma Import (Ada, E119, "system__finalization_masters_E");
   E115 : Short_Integer; pragma Import (Ada, E115, "system__storage_pools__subpools_E");
   E154 : Short_Integer; pragma Import (Ada, E154, "system__direct_io_E");
   E009 : Short_Integer; pragma Import (Ada, E009, "system__secondary_stack_E");
   E101 : Short_Integer; pragma Import (Ada, E101, "ada__strings__unbounded_E");
   E046 : Short_Integer; pragma Import (Ada, E046, "ada__text_io_E");
   E133 : Short_Integer; pragma Import (Ada, E133, "d_stack_E");
   E144 : Short_Integer; pragma Import (Ada, E144, "d_vector_E");
   E147 : Short_Integer; pragma Import (Ada, E147, "declarations__d_names_table_E");
   E136 : Short_Integer; pragma Import (Ada, E136, "declarations__d_3ac_E");
   E152 : Short_Integer; pragma Import (Ada, E152, "declarations__d_symbol_table_E");
   E091 : Short_Integer; pragma Import (Ada, E091, "ekko_lexical_analyzer_dfa_E");
   E093 : Short_Integer; pragma Import (Ada, E093, "ekko_lexical_analyzer_io_E");
   E162 : Short_Integer; pragma Import (Ada, E162, "ekko_syntax_analyzer_error_report_E");
   E098 : Short_Integer; pragma Import (Ada, E098, "semantics_E");
   E159 : Short_Integer; pragma Import (Ada, E159, "declarations__d_attribute_E");
   E160 : Short_Integer; pragma Import (Ada, E160, "ekko_syntax_analyzer_tokens_E");
   E089 : Short_Integer; pragma Import (Ada, E089, "ekko_lexical_analyzer_E");
   E176 : Short_Integer; pragma Import (Ada, E176, "semantics__assembler_E");
   E168 : Short_Integer; pragma Import (Ada, E168, "semantics__code_generation_routines_E");
   E156 : Short_Integer; pragma Import (Ada, E156, "semantics__lexic_routines_E");
   E172 : Short_Integer; pragma Import (Ada, E172, "semantics__messages_E");
   E170 : Short_Integer; pragma Import (Ada, E170, "semantics__syntax_routines_E");
   E079 : Short_Integer; pragma Import (Ada, E079, "ekko_syntax_analyzer_E");
   E174 : Short_Integer; pragma Import (Ada, E174, "semantics__type_checking_routines_E");

   Local_Priority_Specific_Dispatching : constant String := "";
   Local_Interrupt_States : constant String := "";

   Is_Elaborated : Boolean := False;

   procedure finalize_library is
   begin
      E046 := E046 - 1;
      declare
         procedure F1;
         pragma Import (Ada, F1, "ada__text_io__finalize_spec");
      begin
         F1;
      end;
      E101 := E101 - 1;
      declare
         procedure F2;
         pragma Import (Ada, F2, "ada__strings__unbounded__finalize_spec");
      begin
         F2;
      end;
      declare
         procedure F3;
         pragma Import (Ada, F3, "system__file_io__finalize_body");
      begin
         E066 := E066 - 1;
         F3;
      end;
      E119 := E119 - 1;
      E115 := E115 - 1;
      E154 := E154 - 1;
      declare
         procedure F4;
         pragma Import (Ada, F4, "system__direct_io__finalize_spec");
      begin
         F4;
      end;
      declare
         procedure F5;
         pragma Import (Ada, F5, "system__storage_pools__subpools__finalize_spec");
      begin
         F5;
      end;
      declare
         procedure F6;
         pragma Import (Ada, F6, "system__finalization_masters__finalize_spec");
      begin
         F6;
      end;
      declare
         procedure Reraise_Library_Exception_If_Any;
            pragma Import (Ada, Reraise_Library_Exception_If_Any, "__gnat_reraise_library_exception_if_any");
      begin
         Reraise_Library_Exception_If_Any;
      end;
   end finalize_library;

   procedure adafinal is
      procedure s_stalib_adafinal;
      pragma Import (C, s_stalib_adafinal, "system__standard_library__adafinal");

      procedure Runtime_Finalize;
      pragma Import (C, Runtime_Finalize, "__gnat_runtime_finalize");

   begin
      if not Is_Elaborated then
         return;
      end if;
      Is_Elaborated := False;
      Runtime_Finalize;
      s_stalib_adafinal;
   end adafinal;

   type No_Param_Proc is access procedure;

   procedure adainit is
      Main_Priority : Integer;
      pragma Import (C, Main_Priority, "__gl_main_priority");
      Time_Slice_Value : Integer;
      pragma Import (C, Time_Slice_Value, "__gl_time_slice_val");
      WC_Encoding : Character;
      pragma Import (C, WC_Encoding, "__gl_wc_encoding");
      Locking_Policy : Character;
      pragma Import (C, Locking_Policy, "__gl_locking_policy");
      Queuing_Policy : Character;
      pragma Import (C, Queuing_Policy, "__gl_queuing_policy");
      Task_Dispatching_Policy : Character;
      pragma Import (C, Task_Dispatching_Policy, "__gl_task_dispatching_policy");
      Priority_Specific_Dispatching : System.Address;
      pragma Import (C, Priority_Specific_Dispatching, "__gl_priority_specific_dispatching");
      Num_Specific_Dispatching : Integer;
      pragma Import (C, Num_Specific_Dispatching, "__gl_num_specific_dispatching");
      Main_CPU : Integer;
      pragma Import (C, Main_CPU, "__gl_main_cpu");
      Interrupt_States : System.Address;
      pragma Import (C, Interrupt_States, "__gl_interrupt_states");
      Num_Interrupt_States : Integer;
      pragma Import (C, Num_Interrupt_States, "__gl_num_interrupt_states");
      Unreserve_All_Interrupts : Integer;
      pragma Import (C, Unreserve_All_Interrupts, "__gl_unreserve_all_interrupts");
      Detect_Blocking : Integer;
      pragma Import (C, Detect_Blocking, "__gl_detect_blocking");
      Default_Stack_Size : Integer;
      pragma Import (C, Default_Stack_Size, "__gl_default_stack_size");
      Leap_Seconds_Support : Integer;
      pragma Import (C, Leap_Seconds_Support, "__gl_leap_seconds_support");

      procedure Runtime_Initialize (Install_Handler : Integer);
      pragma Import (C, Runtime_Initialize, "__gnat_runtime_initialize");

      Finalize_Library_Objects : No_Param_Proc;
      pragma Import (C, Finalize_Library_Objects, "__gnat_finalize_library_objects");
   begin
      if Is_Elaborated then
         return;
      end if;
      Is_Elaborated := True;
      Main_Priority := -1;
      Time_Slice_Value := -1;
      WC_Encoding := 'b';
      Locking_Policy := ' ';
      Queuing_Policy := ' ';
      Task_Dispatching_Policy := ' ';
      Priority_Specific_Dispatching :=
        Local_Priority_Specific_Dispatching'Address;
      Num_Specific_Dispatching := 0;
      Main_CPU := -1;
      Interrupt_States := Local_Interrupt_States'Address;
      Num_Interrupt_States := 0;
      Unreserve_All_Interrupts := 0;
      Detect_Blocking := 0;
      Default_Stack_Size := -1;
      Leap_Seconds_Support := 0;

      Runtime_Initialize (1);

      Finalize_Library_Objects := finalize_library'access;

      System.Soft_Links'Elab_Spec;
      System.Exception_Table'Elab_Body;
      E019 := E019 + 1;
      Ada.Containers'Elab_Spec;
      E150 := E150 + 1;
      Ada.Io_Exceptions'Elab_Spec;
      E044 := E044 + 1;
      Ada.Strings'Elab_Spec;
      E099 := E099 + 1;
      Ada.Strings.Maps'Elab_Spec;
      Ada.Strings.Maps.Constants'Elab_Spec;
      E177 := E177 + 1;
      Ada.Tags'Elab_Spec;
      Ada.Streams'Elab_Spec;
      E048 := E048 + 1;
      Interfaces.C'Elab_Spec;
      System.Exceptions'Elab_Spec;
      E021 := E021 + 1;
      System.File_Control_Block'Elab_Spec;
      E077 := E077 + 1;
      System.Finalization_Root'Elab_Spec;
      E070 := E070 + 1;
      Ada.Finalization'Elab_Spec;
      E068 := E068 + 1;
      System.Storage_Pools'Elab_Spec;
      E125 := E125 + 1;
      System.Finalization_Masters'Elab_Spec;
      System.Storage_Pools.Subpools'Elab_Spec;
      System.Direct_Io'Elab_Spec;
      E154 := E154 + 1;
      E115 := E115 + 1;
      System.Finalization_Masters'Elab_Body;
      E119 := E119 + 1;
      System.File_Io'Elab_Body;
      E066 := E066 + 1;
      E072 := E072 + 1;
      Ada.Tags'Elab_Body;
      E050 := E050 + 1;
      E105 := E105 + 1;
      System.Soft_Links'Elab_Body;
      E013 := E013 + 1;
      System.Os_Lib'Elab_Body;
      E074 := E074 + 1;
      System.Secondary_Stack'Elab_Body;
      E009 := E009 + 1;
      Ada.Strings.Unbounded'Elab_Spec;
      E101 := E101 + 1;
      Ada.Text_Io'Elab_Spec;
      Ada.Text_Io'Elab_Body;
      E046 := E046 + 1;
      E133 := E133 + 1;
      E144 := E144 + 1;
      declarations.d_names_table'elab_spec;
      E147 := E147 + 1;
      declarations.d_3ac'elab_spec;
      E136 := E136 + 1;
      declarations.d_symbol_table'elab_spec;
      E152 := E152 + 1;
      E091 := E091 + 1;
      EKKO_LEXICAL_ANALYZER_IO'ELAB_SPEC;
      E093 := E093 + 1;
      Ekko_Syntax_Analyzer_Error_Report'Elab_Spec;
      E162 := E162 + 1;
      semantics'elab_spec;
      declarations.d_attribute'elab_spec;
      E159 := E159 + 1;
      Ekko_Syntax_Analyzer_Tokens'Elab_Spec;
      E160 := E160 + 1;
      E098 := E098 + 1;
      semantics.assembler'elab_body;
      E176 := E176 + 1;
      E168 := E168 + 1;
      E156 := E156 + 1;
      E089 := E089 + 1;
      E172 := E172 + 1;
      E079 := E079 + 1;
      E174 := E174 + 1;
      E170 := E170 + 1;
   end adainit;

   procedure Ada_Main_Program;
   pragma Import (Ada, Ada_Main_Program, "_ada_main");

   function main
     (argc : Integer;
      argv : System.Address;
      envp : System.Address)
      return Integer
   is
      procedure Initialize (Addr : System.Address);
      pragma Import (C, Initialize, "__gnat_initialize");

      procedure Finalize;
      pragma Import (C, Finalize, "__gnat_finalize");
      SEH : aliased array (1 .. 2) of Integer;

      Ensure_Reference : aliased System.Address := Ada_Main_Program_Name'Address;
      pragma Volatile (Ensure_Reference);

   begin
      gnat_argc := argc;
      gnat_argv := argv;
      gnat_envp := envp;

      Initialize (SEH'Address);
      adainit;
      Ada_Main_Program;
      adafinal;
      Finalize;
      return (gnat_exit_status);
   end;

--  BEGIN Object file/option list
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/d_Stack.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/d_Vector.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations-d_Description.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations-d_Names_Table.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations-d_3AC.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations-d_Symbol_Table.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/ekko_lexical_analyzer_dfa.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/ekko_lexical_analyzer_io.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/ekko_syntax_analyzer_error_report.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/Ekko_Syntax_Analyzer_goto.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/Ekko_Syntax_Analyzer_shift_reduce.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/declarations-d_Attribute.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/Ekko_Syntax_Analyzer_tokens.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Assembler.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Code_Generation_Routines.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Lexic_Routines.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/ekko_lexical_analyzer.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Messages.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/ekko_syntax_analyzer.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/main.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Type_Checking_Routines.o
   --   /Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/semantics-Syntax_Routines.o
   --   -L/Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/
   --   -L/Users/Mac/MY_DOCUMENTS/GEIN/4.1/Compiladores/EkkoLang-Compiler/obj/
   --   -L/applications/gps/lib/gcc/x86_64-apple-darwin13.4.0/4.9.3/adalib/
   --   -static
   --   -lgnat
--  END Object file/option list   

end ada_main;
