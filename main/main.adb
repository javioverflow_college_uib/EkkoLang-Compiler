with Ada.Text_IO; use Ada.Text_IO;
with Ada.IO_Exceptions; use Ada.IO_Exceptions;
with Ada.Command_Line; use Ada.Command_Line;

with Ekko_Syntax_Analyzer;  use Ekko_Syntax_Analyzer;
with Ekko_Syntax_Analyzer_Error_Report;

with semantics;            use semantics;
with semantics.Assembler;  use semantics.Assembler;

procedure Main is
begin
   
   if Argument_Count = 0 then 
      Put_Line("You must specify the path to the source code");
      return;
   end if;
   
   for i in 1..Argument_Count loop
      
      Get_Analysis_Ready(Argument(i));
      YYParse;
      Generate_Symbolic_3AC_File;
      if successful_type_checking then
         Generate_Assembler;
      end if;
      Get_Analysis_Done;
      
   end loop;
   
exception
   when Ekko_Syntax_Analyzer_Error_Report.Syntax_Error =>
      Get_Analysis_Done;
   when Ada.IO_Exceptions.Name_Error =>
      Put_Line("Ekko source file not found");
   when others => 
      null;
end Main;
