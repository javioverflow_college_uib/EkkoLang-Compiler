import re
import sys 

FILENAME_ARG = 1

if not sys.argv[FILENAME_ARG]:
    print "Falta pasar la ruta del fichero"
    sys.exit()

filename = sys.argv[FILENAME_ARG]

with open(filename, 'r') as f:

    file_text = f.read()

    procedures = re.findall(r'procedure\s([A-Za-z_]+)', file_text)
    functions  = re.findall(r'function\s([A-Za-z_]+)', file_text)

    for action in procedures + functions:
        print 'b ' + action
