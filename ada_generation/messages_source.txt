error_main_procedure_with_args
"ERROR: Main procedure mustn't have arguments"

error_identifier_already_seen
"ERROR: " & p.line'Img & ":" & p.column'Img & ": There is another declaration with same name"
id
Name_Id
p
Position

error_expected_type
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Expected type"
id
Name_Id
p
Position

error_expected_scalar_type
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Expected a scalar type"
id
Name_Id
p
Position

error_incompatible_value
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Incompatible value"
id
Name_Id
p
Position

error_value_out_of_range_lower
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Value is too low"
id
Name_Id
p
Position

error_value_out_of_range_upper
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Value is too high"
id
Name_Id
p
Position

error_incorrect_value_type
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Incorrect value type"
idt
Name_Id
idv
Name_Id
p
Position

error_expected_integer_value
"ERROR: Expected integer value"

error_expected_constant
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Expected constant"
id
Name_Id
p
Position

error_field_in_record_already_exists
"ERROR: " & p.line'Img & ":" & p.column'Img & ": There is another field with same name"
id
Name_Id
p
Position

error_expected_boolean_expression
"ERROR: Expected boolean expression"

error_expected_variable_name
"ERROR: Expected variable name"

error_expresion_and_ref_different_type
"ERROR: Expression can't be assigned to variable. Not same type"

error_expected_scalar_reference
"ERROR: Expected a scalar type"

error_expected_record_identifier
"ERROR: Expected record type"

error_record_field_does_not_exists
"ERROR: record field does not exists"

error_expected_array_identifier
"ERROR: Expected array type"

error_missing_value_for_indexes
"ERROR: Missing value for index/es"

error_expression_and_index_different_type
"ERROR: Expression type does not match with array index type"

error_more_values_than_indexes
"ERROR: More indexes values were given than expected"

error_expected_integer_expression
"ERROR: Expected integer expression"

error_expressions_have_different_types
"ERROR: Expressions have different types"

error_expected_scalar_expressions
"ERROR: Expected scalar expressions"

error_value_type_for_range_incompatible
"ERROR: Value type for range incompatible"

error_range_values_not_correct
"ERROR: Range values are not correct"

error_end_name_does_not_match
"ERROR: " & p.line'Img & ":" & p.column'Img & ": Name does not match with the one at preamble"
id
Name_Id
p
Position

error_argument_name_already_registered
"ERROR: Argument name must be unique"
